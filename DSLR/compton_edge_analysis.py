import numpy as np
import matplotlib.pyplot as plt
import scipy as sp
import scipy.special
import matplotlib.patches as patches
from sympy import DiracDelta
n=100000
x_min= -250.0
x_max= 1000.0
E = np.linspace(x_min,x_max,n)
E_c = 500.0
np.set_printoptions(precision=2)
def H(E):
    h = np.zeros([n])
    for i in range(0,n):
        if E[i] < E_c:
            h[i] = 1
        elif E[i] == E_c:
            h[i] = 1
        else:
            h[i] = 0
    return h

a = 1.0e-2
b = 9.0e-2
c = 8.0e+3

def f(E):
    r = np.zeros([n])
    for i in range(0,n):
        if E[i] < E_c:
            r[i] = a*np.power(E[i],2)+b*E[i]+c
        elif E[i] == E_c:
            r[i] = a*np.power(E[i],2)+b*E[i]+c
        else:
            r[i] = 0
    return r

sigma = 40

def g(E):
    k = np.zeros([n])
    for i in range(0,n):
        k[i] = np.exp(-np.power(E[i],2)/(2*sigma))/(np.sqrt(2*np.pi)*sigma)
    return k

def g2(E):
    k = np.zeros([n])
    for i in range(0,n):
        k[i] = np.exp(np.power(E[i],2)/(2*sigma))/(np.sqrt(2*np.pi)*sigma)
    return k


def g1(E):
    k = np.zeros([n])
    for i in range(0,n):
        k[i] = np.exp(-np.power(E[i],2)/(2*0.001))/(np.sqrt(2*np.pi)*0.001)
    return k


def response(sigma) :
    alpha_1 = (1/2)*(a*(np.power(E,2)+np.power(sigma,2))+b*E+c)
    beta_1 = (-sigma/np.sqrt(2*np.pi)*a*(E+E_c))+b
    alpha_2 = sp.special.erfc((E-E_c)/(np.sqrt(2)*sigma))
    beta_2 = np.exp(-np.power((E-E_c),2)/(2*np.power(sigma,2)))
    total = alpha_1*alpha_2 + beta_1*beta_2
    return total

test = response(sigma)
print(test.shape,"test.shape")
E_max = E[np.argmax(test)]
print(E_max,"E_max")
R_max = test.max()
print(R_max,"R_max")

continuum_R_min = c-(np.power(b,2))/(4*a)
continuum_E_min = (-b)/(2*a)
R_c = ((R_max/2)-((2*a*E_c)/np.sqrt(2*np.pi))+(b*sigma)+((a*sigma**2)/2))
RR= a*(0.5*(E_c**2+sigma**2)-((sigma*2*E_c)/(2*np.pi)**0.5)) + b*(E_c*0.5+1) + c*0.5
print(R_c,"R_c")

plt.axhline(y=R_max,color="m",xmin=0.0 , xmax=0.541 ,linestyle='--',\
    label='$R_\max$={:0.3f}'.format(R_max),alpha=0.5 ,lw=1)
plt.axhline(y=R_max/2,color="gray",xmin=0.0 , xmax=0.6056 ,linestyle='-.',\
    label=r'$R_{}$={:0.3f}'.format("{half max}",R_max/2),alpha=0.5 ,lw=2)
plt.axvline(x=E_max,color="c",ymin=0.05, ymax= 0.48, linestyle='--',\
    label='$E_\max$={:0.3f}'.format(E_max),alpha=0.5 ,lw=1)
plt.plot(E_max,R_max,"ro",alpha=0.7,lw=4)
plt.plot(continuum_E_min,continuum_R_min,"ro",alpha=0.7,lw=4)
plt.plot(E_c,RR,"ro",alpha=0.7,lw=4)
plt.plot(E_c+3.044,R_c,"ro",alpha=0.7,lw=5)
#circ = patches.Circle((E_max,R_max),radius=5,fill=True,clip_on=True,alpha=0.8,fc="pink")
#plt.gca().add_patch(circ)
plt.axhline(y=continuum_R_min,color="r",xmin=0.0 , xmax=0.23 ,linestyle='--',\
    label='$R_\min$={:0.3f} on continuum'.format(continuum_R_min),alpha=0.5 ,lw=1)
plt.axvline(x=continuum_E_min,color="b",ymin=0.05, ymax= 0.407, linestyle='--',\
    label='$E_\min$={:0.3f} on continuum'.format(continuum_E_min),alpha=0.5 ,lw=1)
plt.axhline(y=RR,color="purple",xmin=0.0 , xmax=0.598 ,linestyle='--',\
    label='R($E_c$)={:0.3f} '.format(RR),alpha=0.5 ,lw=1)
plt.axhline(y=R_c,color="orange",xmin=0.0 , xmax=0.6056 ,linestyle='-',\
    label='R$^*$$_c$={:0.3f} '.format(R_c),alpha=0.5 ,lw=2)
plt.plot(E,f(E)+4.92e+1*g1(E-800),label="Ideal compton continuum, edge,\nand photo peak signal",lw = 3 ,ls = "--" )
plt.plot(E,response(sigma)+2e+6*g(E-800),label="Signal response with $\sigma$ = 40",lw=3)
plt.xlim(x_min,x_max)
plt.xlabel('Energy[keV] or channel of MCA[ch]',fontsize=22)
plt.ylabel('A.U',fontsize=22)
#plt.gca().xaxis.set_ticklabels([])
#plt.yscale("log")
#plt.axes().get_xaxis().set_visible(False)
#plt.axes().axis('off')
plt.title('Detector response to signal (or signal convolution with gaussian kernel)',fontsize = 24)
plt.legend(loc='best',fontsize=16)
plt.show()