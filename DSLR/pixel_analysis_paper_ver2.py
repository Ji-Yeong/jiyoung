import os
import numpy as np
import matplotlib.pyplot as plt
from skimage import io
from skimage.color import rgb2gray
from skimage.color import rgba2rgb
from skimage.color import rgb2hsv
from skimage import filters
from skimage.data import camera
from skimage.util import compare_images
from skimage.filters import gaussian
from skimage.segmentation import active_contour
from skimage.filters import try_all_threshold
from skimage import data, img_as_float
from skimage.segmentation import chan_vese
from scipy.optimize import curve_fit
import scipy

#filename = os.path.join('6MeV.PNG')
image5 = io.imread('0.PNG')
#image5 = io.imread('12MeV_new_v3_ref.png')
#print(image1.shape)
#image1 = io.imread('{}.png'.format("12MeV_new_v3"))
image1 = io.imread('{}.PNG'.format("12MeV"))
#image2 = rgb2hsv(image1)
image2 = rgba2rgb(image1)
image2 = rgb2hsv(image2)


blue1 = image1[:,:,2]/np.max(image1[:,:,2])*255
hsv_v1 = image2[:,:,2]/np.max(image2[:,:,2])*100
#blue = (image[:,:,2]/np.max(image[:,:,2]))*100.0
#image_gray = rgb2gray(image)
#print(image_gray.shape)
#blue = (image_gray[:,:]/np.max(image_gray[:,:]))*100.0
#image = img_as_float(img)
#print(image.shape)
#print(blue[206,168])
#print(np.max(blue),"max value")
#print(np.argmax(blue),"index at max value in 2D array flatten")
#print(np.unravel_index(blue.argmax(),blue.shape),"index max at 2D array")
blue11 = blue1.ravel()
hsv_new_v1 = np.copy(hsv_v1)
grid = plt.GridSpec(2,2, wspace=0.1, hspace=0.1)
plt.subplot(grid[0,0])
plt.imshow(image5)
#plt.imshow(image1[:,:,2],cmap=plt.cm.Blues)
plt.axis("off")
#plt.title("Beam exposed on Sample", fontsize=12)

plt.subplot(grid[0,1])
plt.imshow(image1)
plt.axis("off")
#plt.title("Beam not exposed on Sample", fontsize=12)

plt.subplot(grid[1,0])
#blue11[blue1<100]=np.nan
#blue21 = np.copy(blue11)
#blue21[(blue11/255)<0.2]=np.nan

hsv_new_v1[hsv_v1<5]=np.nan
plt.imshow(hsv_new_v1,aspect='auto',interpolation = 'none',vmin=0,vmax=256,alpha=1.0,cmap=plt.cm.hsv)
plt.imshow(image5,alpha=0.4)
plt.axis("off")
#plt.title("Intensity image $\geq$20% on fusion image", fontsize=12)

plt.subplot(grid[1,1])
x = np.copy(blue1.flatten())
#x = hsv_new_v1.ravel()
print(x.shape)
bin_heights, bin_borders, _= plt.hist(blue1.ravel(),bins=256,histtype="step")


bin_widths = np.diff(bin_borders)
bin_centers = bin_borders[:-1] + bin_widths / 2
x_array = bin_centers
y_array_2gauss = bin_heights

def _2gaussian(x, amp1,cen1,sigma1, amp2,cen2,sigma2):
    return amp1*(1/(sigma1*(np.sqrt(2*np.pi))))*(np.exp((-1.0/2.0)*(((x_array-cen1)/sigma1)**2))) + \
            amp2*(1/(sigma2*(np.sqrt(2*np.pi))))*(np.exp((-1.0/2.0)*(((x_array-cen2)/sigma2)**2)))
def _3gaussian(x, amp1,cen1,sigma1, amp2,cen2,sigma2, amp3,cen3,sigma3,amp4,cen4,sigma4):
    return (amp1*(1/(sigma1*(np.sqrt(2*np.pi))))*(np.exp((-1.0/2.0)*(((x_array-cen1)/sigma1)**2)))) + \
            (amp2*(1/(sigma2*(np.sqrt(2*np.pi))))*(np.exp((-1.0/2.0)*(((x_array-cen2)/sigma2)**2)))) + \
                (amp3*(1/(sigma3*(np.sqrt(2*np.pi))))*(np.exp((-1.0/2.0)*(((x_array-cen3)/sigma3)**2)))) + \
                    (amp4*(1/(sigma4*(np.sqrt(2*np.pi))))*(np.exp((-1.0/2.0)*(((x_array-cen4)/sigma4)**2))))

    
def _1gaussian(x, amp1,cen1,sigma1):
    return amp1*(1/(sigma1*(np.sqrt(2*np.pi))))*(np.exp((-1.0/2.0)*(((x_array-cen1)/sigma1)**2)))
"""
amp1= 8000.0
cen1 = 12.0
sigma1 = 3.0
amp2 = 2000.0
cen2 = 81.0
sigma2 = 2.0
amp3 = 1500.0
cen3 = 31.0
sigma3 = 1.40
"""

amp1= 1
cen1 = 2
sigma1 = .0001
amp2 = 1
cen2 = 40.4
sigma2 = 1.
amp3 = 1
cen3 = 10.0
sigma3 = 1.0
amp4 = 1
cen4 = 20.0
sigma4 = 1.0

#popt_2gauss, pcov_2gauss = scipy.optimize.curve_fit(_2gaussian, x_array, y_array_2gauss, p0=[amp1, cen1, sigma1, amp2, cen2, sigma2])
#popt_2gauss, pcov_2gauss = scipy.optimize.curve_fit(_3gaussian, x_array, y_array_2gauss, p0=[amp1, cen1, sigma1, amp2, cen2, sigma2,amp3, cen3, sigma3],absolute_sigma=True,method='lm')
popt_2gauss, pcov_2gauss = scipy.optimize.curve_fit(_3gaussian, x_array, y_array_2gauss, p0=[amp1, cen1, sigma1, amp2, cen2, sigma2,amp3, cen3, sigma3,amp4, cen4, sigma4],absolute_sigma=True,method='lm')
#popt_2gauss, pcov_2gauss = scipy.optimize.curve_fit(_3gaussian, x_array, y_array_2gauss )
#popt_2gauss, pcov_2gauss = scipy.optimize.curve_fit(_2gaussian, x_array, y_array_2gauss, p0=[10, 11, 5, 2, 84, 10])
print(popt_2gauss)
perr_2gauss = np.sqrt(np.diag(pcov_2gauss))
pars_1 = popt_2gauss[0:3]
pars_2 = popt_2gauss[3:6]
pars_3 = popt_2gauss[6:9]
pars_4 = popt_2gauss[9:12]
gauss_peak_1 = _1gaussian(x_array, *pars_1)
gauss_peak_2 = _1gaussian(x_array, *pars_2)
gauss_peak_3 = _1gaussian(x_array, *pars_3)
gauss_peak_4 = _1gaussian(x_array, *pars_4)

tt = gauss_peak_1+gauss_peak_2+gauss_peak_3+gauss_peak_4

plt.plot(x_array, gauss_peak_1, "g", alpha=0.2)
plt.fill_between(x_array, gauss_peak_1.min(), gauss_peak_1, facecolor="green", alpha=0.1)
plt.plot(x_array, gauss_peak_2, "y", alpha=0.2)
plt.fill_between(x_array, gauss_peak_2.min(), gauss_peak_2, facecolor="yellow", alpha=0.1) 
plt.plot(x_array, gauss_peak_3, "cyan", alpha=0.2)
plt.fill_between(x_array, gauss_peak_3.min(), gauss_peak_3, facecolor="cyan", alpha=0.1)
plt.plot(x_array, gauss_peak_4, "m", alpha=0.2)
plt.fill_between(x_array, gauss_peak_4.min(), gauss_peak_4, facecolor="m", alpha=0.1) 
plt.plot(x_array, tt, "red",ls='--', alpha=0.5)


"""
amp1= 1000
cen1 = 67
sigma1 = 5
amp2 = 2500
cen2 = 81
sigma2 = 5

x_array1 = np.copy(bin_centers[50:])
y_array_2gauss1 = np.copy(bin_heights[50:])
def _2gaussian1(x, amp1,cen1,sigma1, amp2,cen2,sigma2):
    return amp1*(1/(sigma1*(np.sqrt(2*np.pi))))*(np.exp((-1.0/2.0)*(((x_array1-cen1)/sigma1)**2))) + \
            amp2*(1/(sigma2*(np.sqrt(2*np.pi))))*(np.exp((-1.0/2.0)*(((x_array1-cen2)/sigma2)**2)))
def _1gaussian1(x, amp1,cen1,sigma1):
    return amp1*(1/(sigma1*(np.sqrt(2*np.pi))))*(np.exp((-1.0/2.0)*(((x_array1-cen1)/sigma1)**2)))

popt_2gauss_1, pcov_2gauss_1 = scipy.optimize.curve_fit(_2gaussian1, x_array1, y_array_2gauss1, p0=[amp1, cen1, sigma1, amp2, cen2, sigma2])
perr_2gauss_1 = np.sqrt(np.diag(pcov_2gauss_1))
pars_4 = popt_2gauss_1[0:3]
pars_5 = popt_2gauss_1[3:6]
gauss_peak_4 = _1gaussian1(x_array1, *pars_4)
gauss_peak_5 = _1gaussian1(x_array1, *pars_5)

plt.plot(x_array1, gauss_peak_4, "orange", alpha=0.2)
plt.fill_between(x_array1, gauss_peak_4.min(), gauss_peak_4, facecolor="orange", alpha=0.1)
plt.plot(x_array1, gauss_peak_5, "purple", alpha=0.2)
plt.fill_between(x_array1, gauss_peak_5.min(), gauss_peak_5, facecolor="purple", alpha=0.1) 
"""


#plt.hist(blue1.ravel(),bins=256, range=(0.0, 255.0),histtype="step")
#plt.plot(x_interval_for_fit, gaussian(x_interval_for_fit, *popt), label='fit')
#plt.yscale('log')
#plt.title("Histogram for Blue channel on range (Dark=0,Bright=100)", fontsize=12)
plt.show()
