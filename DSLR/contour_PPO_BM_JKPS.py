import os
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from numpy.core.function_base import linspace
from skimage import io
from skimage.color import rgb2gray
from skimage.color import rgba2rgb
from skimage.color import rgb2hsv
from skimage import filters
from skimage.data import camera
from skimage.util import compare_images
from skimage.filters import gaussian
from skimage.segmentation import active_contour
from skimage.filters import try_all_threshold
from skimage import data, img_as_float
from skimage.segmentation import chan_vese
from matplotlib import cm
from collections import OrderedDict

#image5 = io.imread('0.PNG')
#image5 = io.imread('PPO_12MeV_mobile_ref_1.jpg')
#image5 = io.imread('PPO_6MeV_ref.JPG')
#image5 = io.imread('PPO_9_12MeV_ref.JPG')
image5 = io.imread('PPO_BM_ref.JPG')
#i="12MeV.PNG"
#i="UV254.jpg"
#i="UV365.jpg"
#i="9MeV_v3.jpg"
#i="PPO_12MeV_1.JPG"
i="PPO_BM_9MeV_1.JPG"
#i="PPO_12MeV_oblique_1.JPG"
#i="PPO_12MeV_mobile_1.jpg"
#name = ["6MeV","9MeV","12MeV"]
image1 = io.imread('{}'.format(i))
#image11 = rgba2rgb(image1)
image12 = rgb2hsv(image1)

blue1 = image12[:,:,2]
print(blue1.shape)
blue11= np.copy(blue1)
blue11[blue1<=0.22]=np.nan
X=np.arange(0,2848,1)
Y=np.arange(0,4272,1)
#print(Y)
x,y = np.meshgrid(X, Y,sparse=False)

#grid = plt.GridSpec(1,2, wspace=0.1, hspace=0.1)

#plt.subplot(grid[1,2])
#plt.imshow(image1)
#cmap=plt.cm.get_cmap('rainbow', 73)

#plt.subplot(grid[1,3])
#plt.imshow(image5,alpha=0.6)
#plt.imshow(blue11,alpha=1)
contours=plt.contour(y.T,x.T,blue1,levels=np.arange(0.3,0.8,0.1),origin='lower')
plt.clabel(contours, inline=True,levels=np.arange(0.3,0.8,0.1), fontsize=8)
#plt.imshow(blue11, \
    #extent=[0, 5, 0, 5], origin='lower',
#           cmap='RdGy', alpha=0.5)
plt.colorbar()
#,cmap=plt.cm.jet).set_clim(0.1, 0.7)
#plt.colorbar()
#plt.axis("off")
#plt.legend(loc='best',fontsize=14)
#plt.savefig('systematic_diagam_9MeV.jpg', dpi=300, quality=95, optimize=True, progressive=True)
plt.show()

#plt.savefig("{}_processing.pdf".format(name),transparent=True,bbox_inches="tight",pad_inches=0.1)
