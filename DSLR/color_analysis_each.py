import os
import numpy as np
import matplotlib.pyplot as plt
from skimage import io
from skimage.color import rgb2gray
from skimage.color import rgba2rgb
from skimage import filters
from skimage.data import camera
from skimage.util import compare_images
from skimage.filters import gaussian
from skimage.segmentation import active_contour
from skimage.filters import try_all_threshold
from skimage import data, img_as_float
from skimage.segmentation import chan_vese


#i="6mev_p"
i="6MeV"
#name = ["6MeV","9MeV","12MeV"]
image1 = io.imread('{}.PNG'.format(i))
blue1 = image1[:,:,2]

#j="9mev_p"
j="9MeV"
#name = ["6MeV","9MeV","12MeV"]
image2 = io.imread('{}.PNG'.format(j))
blue2 = image2[:,:,2]

#k="12mev_p"
k="12MeV"
#name = ["6MeV","9MeV","12MeV"]
image3 = io.imread('{}.PNG'.format(k))
blue3 = image3[:,:,2]

plt.hist(blue1.ravel(),bins=256, range=(0.0, 255.0),alpha=0.9,histtype="step",density=False,lw=2,label="{} Blue".format(i))
plt.hist(blue2.ravel(),bins=256, range=(0.0, 255.0),alpha=0.9,histtype="step",density=False,lw=2,label="{} Blue".format(j))
plt.hist(blue3.ravel(),bins=256, range=(0.0, 255.0),alpha=0.9,histtype="step",density=False,lw=2,label="{} Blue".format(k))
plt.legend(loc='best',fontsize=22)
plt.title("Normailzed Histogram {} for Blue channel on range (Dark=0,Bright=255)".format("6,9,12MeV"), fontsize=22)
plt.xlabel('$C_{blue}$',fontsize=22)
plt.ylabel('Normalized A.U',fontsize=22)
#plt.yscale("log")
plt.show()
