import os
import numpy as np
import matplotlib.pyplot as plt
from skimage import io
from skimage.color import rgb2gray
from skimage.color import rgba2rgb
from skimage.color import rgb2hsv

import matplotlib as mpl
import matplotlib.font_manager
matplotlib.font_manager.findSystemFonts(fontpaths=None, fontext='ttf')
#mpl.use("pgf")
pgf_with_pdflatex = {
    "pgf.texsystem": "pdflatex",
    "pgf.preamble": [
         r"\usepackage[utf8x]{inputenc}",
         r"\usepackage[T1]{fontenc}",
         r"\usepackage{cmbright}",
         ]
}
mpl.rcParams.update(pgf_with_pdflatex)
mpl.rcParams['font.family'] = 'serif'

#image5 = io.imread('0.PNG')
i = "scan_6MeV"
#i = "scan0021"
image1 = io.imread('{}.jpg'.format(i))
image4 = io.imread('{}.jpg'.format(i))
#image1[:,:,0]=0
#image1[:,:,1]=0
image_hsv = rgb2hsv(image1)
image2 = rgb2gray(image4)
image3 = 1-image2
print(image2.shape)
pdd = np.mean(image3,axis=1)
pdd = pdd - pdd.min()
pdd_normal = pdd/pdd.max()*100

grid = plt.GridSpec(2,2, wspace=0.2, hspace=0.2)

plt.subplot(grid[0,0])
plt.imshow(image1,alpha=1.0)
plt.xlabel('Pixel horizontal(column) index',fontsize=16,horizontalalignment='right', x=1.0)
plt.ylabel('Pixel veritalcal(row) index',fontsize=16)


plt.subplot(grid[0,1])
plt.plot(pdd_normal)
#plt.autoscale(enable=True, axis='y', tight=None)
plt.xlabel('Pixel vertical(row) index',fontsize=16,horizontalalignment='right', x=1.0)
plt.ylabel('Percentage depth dose [%]',fontsize=16)

plt.subplot(grid[1,0])
#plt.hist(red.ravel(),bins=2560, range=(0.0, 255.0),alpha=0.5)
plt.hist(image_hsv[:,:,0].ravel(),bins=361,range=(0.0, 1.0),alpha=0.7,density=True,histtype="step",color="y",label="Hue (0~1) or (0~360)")
plt.hist(image_hsv[:,:,1].ravel(),bins=101,range=(0.0, 1.0),alpha=0.7,density=True,histtype="step",color="m",label="Saturation (0.0~1.0)")
plt.hist(image_hsv[:,:,2].ravel(),bins=101,range=(0.0, 1.0),alpha=0.7,density=True,histtype="step",color="c",label="Value[lightness] (0.0~1.0)")
plt.title(" {} HSV color space ".format(i) ,fontsize=12)
plt.legend(loc='best',fontsize=12)

plt.subplot(grid[1,1])
plt.hist(image1[:,:,0].ravel(),bins=256,range=(0.0, 255.0), alpha=0.7,histtype="step",color="r",label="R")
plt.hist(image1[:,:,1].ravel(),bins=256,range=(0.0, 255.0), alpha=0.7,histtype="step",color="g",label="G")
plt.hist(image1[:,:,2].ravel(),bins=256,range=(0.0, 255.0), alpha=0.7,histtype="step",color="b",label="B")
plt.title(" {} RGB color space ".format(i) ,fontsize=12)
plt.legend(loc='best',fontsize=12)
#plt.imshow(image2,alpha=1.0)
plt.show()