import os
import numpy as np
import matplotlib.pyplot as plt
from skimage import io
from skimage.color import rgb2gray
from skimage.color import rgba2rgb
from skimage import filters
from skimage.data import camera
from skimage.util import compare_images
from skimage.filters import gaussian
from skimage.segmentation import active_contour
from skimage.filters import try_all_threshold
from skimage import data, img_as_float
from skimage.segmentation import chan_vese

#filename = os.path.join('6MeV.PNG')
image5 = io.imread('0.PNG')
#print(image1.shape)
name="6MeV"
image1 = io.imread('{}.PNG'.format(name))

image2 = io.imread('{}.PNG'.format("9MeV"))
image3 = io.imread('{}.PNG'.format("12MeV"))

#image = rgba2rgb(image)
blue1 = image1[:,:,2]/np.max(image1[:,:,2])*255
blue2 = image2[:,:,2]/np.max(image2[:,:,2])*255
blue3 = image3[:,:,2]/np.max(image3[:,:,2])*255
#blue = (image[:,:,2]/np.max(image[:,:,2]))*100.0
#image_gray = rgb2gray(image)
#print(image_gray.shape)
#blue = (image_gray[:,:]/np.max(image_gray[:,:]))*100.0
#image = img_as_float(img)
#print(image.shape)
#print(blue[206,168])
#print(np.max(blue),"max value")
#print(np.argmax(blue),"index at max value in 2D array flatten")
#print(np.unravel_index(blue.argmax(),blue.shape),"index max at 2D array")
blue11 = np.copy(blue1)
grid = plt.GridSpec(2,3, wspace=0.1, hspace=0.1)
plt.subplot(grid[0,0])
plt.imshow(image1[:,:,2],cmap=plt.cm.Blues)
plt.axis("off")
plt.title("Beam exposed on Sample in Light Off", fontsize=12)

plt.subplot(grid[0,1])
plt.imshow(image1)
plt.axis("off")
plt.title("Beam not exposed on Sample in Light On", fontsize=12)

plt.subplot(grid[0,2])
blue11[blue1<100]=np.nan
blue21 = np.copy(blue11)
blue21[blue11>102]=np.nan
plt.imshow(blue21,vmin=0,vmax=10,alpha=1.0,cmap=plt.cm.Blues)
plt.imshow(image5,alpha=0.4)
plt.axis("off")
plt.title("Intensity image $\geq$18% on fusion image", fontsize=12)

plt.subplot(grid[1,0])
blue12 = np.copy(blue2)
blue12[blue2<84]=np.nan
blue22 = np.copy(blue12)
blue22[blue12>86]=np.nan
plt.imshow(blue22,vmin=0,vmax=10,alpha=1.0,cmap=plt.cm.Blues)
plt.imshow(image5,alpha=0.4)
plt.axis("off")
plt.title("Intensity image $\geq$24% on fusion image", fontsize=12)

plt.subplot(grid[1,1])
blue13 = np.copy(blue3)
blue13[blue3<70]=np.nan
#blue23 = np.copy(blue13)
#blue23[blue13>0]=np.nan
plt.imshow(blue13,vmin=0,vmax=1,alpha=1.0,cmap=plt.cm.Blues)
plt.imshow(image5,alpha=0.4)
plt.axis("off")
plt.title("Intensity image $\geq$30% on fusion image", fontsize=12)

plt.subplot(grid[1,2])
plt.hist(blue3.ravel(),bins=256, range=(0.0, 255.0))
#plt.yscale('log')
plt.title("Histogram for Blue channel on range (Dark=0,Bright=100)", fontsize=12)

#plt.subplots_adjust(wspace=1.0, hspace=1.5)
#fig.tight_layout(pad=0.4, w_pad=1.0, h_pad=1.5)
plt.show()
#plt.savefig("{}_processing.pdf".format(name),transparent=True,bbox_inches="tight",pad_inches=0.1)