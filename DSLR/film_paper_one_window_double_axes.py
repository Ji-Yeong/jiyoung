import os
import numpy as np
import matplotlib.pyplot as plt
from skimage import io
from skimage.color import rgb2gray
from skimage.color import rgba2rgb
from skimage.color import rgb2hsv
from matplotlib.pyplot import cm, xticks

import matplotlib as mpl
import matplotlib.font_manager
matplotlib.font_manager.findSystemFonts(fontpaths=None, fontext='ttf')
#mpl.use("pgf")
pgf_with_pdflatex = {
    "pgf.texsystem": "pdflatex",
    "pgf.preamble": [
         r"\usepackage[utf8x]{inputenc}",
         r"\usepackage[T1]{fontenc}",
         r"\usepackage{cmbright}",
         ]
}
mpl.rcParams.update(pgf_with_pdflatex)
mpl.rcParams['font.family'] = 'serif'
#plt.gcf().subplots_adjust(bottom=0.15)

plt.figure(figsize=(12.4,6.4))
#i = "scan_6MeV"
#image1 = io.imread('{}.jpg'.format(i))
#image5 = io.imread('scan0021.jpg')
image5 = io.imread('film_three.jpg')
print(image5)
xx=588 # 50/0.085 , dpi for x  is 297/3508
plt.imshow(image5,alpha=1.0)
plt.xlabel(r'Width [mm]'+'\n'+r'(pixel number)', fontsize=40, horizontalalignment='right', x=1.0)
plt.ylabel(r'Depth [mm]'+'\n'+r' (pixel number)',fontsize=40)
#plt.xlabel(r'Width $\left( \dfrac{[mm]}{10} \right)$',fontsize=26,horizontalalignment='right', x=1.0)
#plt.ylabel(r'Depth $\left( \dfrac{[mm]}{10} \right)$',fontsize=26)
#plt.xticks(np.arange(0, 3508, step=500),labels=[ "{0:.0f}".format(x*297/3508) for x in np.arange(0, 3508, step=500)])
#plt.yticks(np.arange(0, 2480, step=500),labels=[ "{0:.0f}".format(x*210/2480) for x in np.arange(0, 2480, step=500)])
labels_x = ["0",'\n0',"50",'\n(588)',"100",'\n(1176)',"150",'\n(1764)',"200",'\n(2352)',"250",'\n(2940)',"297",'\n(3508)']
labels_y = ["0",'',"50",'\n(588)',"100",'\n(1176)',"150",'\n(1764)',"200",' (2352)',"210",' (2480)']
new_labels_x = [ ''.join(x) for x in zip(labels_x[0::2], labels_x[1::2]) ]
new_labels_y = [ ''.join(x) for x in zip(labels_y[0::2], labels_y[1::2]) ]
plt.tight_layout()
plt.xticks([0,xx,2*xx,3*xx,4*xx,5*xx,3508],new_labels_x,fontsize=22)
plt.yticks([0,xx,2*xx,3*xx,4*xx,2480],new_labels_y,fontsize=22)
#plt.savefig('Film_A4.jpg', dpi=300, quality=95, optimize=True, progressive=True)
#plt.savefig('Film_A4.pdf',dpi=300,transparent=True)
plt.show()