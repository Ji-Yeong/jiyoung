import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm  

file1 = "2Ee-only-Emission.txt"
file2 = "2Ee70-H2O30-PPO0.63g-Emission.txt"
file3 = "2Ee90-H2O10-PPO1.05g+bM0.0042g-Emission.txt"

two_E_ethanol = np.genfromtxt(file1,dtype=np.float64)
PPO = np.genfromtxt(file2,dtype=np.float64)
PPO_bis_MSB = np.genfromtxt(file3,dtype=np.float64)

def x_match(wavelength):
    X_wave =1.065*np.exp(-1/2*(((wavelength-595.8)/33.33)**2))\
        +0.366*np.exp(-1/2*(((wavelength-446.8)/19.44)**2))
    return X_wave

def y_match(wavelength):
    Y_wave =1.014*np.exp(-1/2*((np.log(wavelength)-np.log(556.3))/0.075)**2)
    return Y_wave

def z_match(wavelength):
    Z_wave =1.839*np.exp(-1/2*((np.log(wavelength)-np.log(449.8))/0.051)**2 )
    return Z_wave
#xxxx= z_match(PPO_bis_MSB[:,0])
plt.plot(PPO_bis_MSB[:,0],PPO_bis_MSB[:,1],alpha=0.7,lw=3,ls="-",color="k",label='PPO + bis-MSB signal')
plt.plot(PPO_bis_MSB[:,0],x_match(PPO_bis_MSB[:,0])*PPO_bis_MSB[:,1],alpha=0.7,lw=3,ls="-.",color="c",label='composit of x and signal')
plt.plot(PPO_bis_MSB[:,0],y_match(PPO_bis_MSB[:,0])*PPO_bis_MSB[:,1],alpha=0.7,lw=3,ls="-.",color="y",label='composit of y and signal')
plt.plot(PPO_bis_MSB[:,0],z_match(PPO_bis_MSB[:,0])*PPO_bis_MSB[:,1],alpha=0.7,lw=3,ls="-.",color="m",label='composit of z and signal')
plt.plot(PPO_bis_MSB[:,0],x_match(PPO_bis_MSB[:,0]),alpha=0.7,lw=3,ls="--",color="r",label='x color matching')
plt.plot(PPO_bis_MSB[:,0],y_match(PPO_bis_MSB[:,0]),alpha=0.7,lw=3,ls="--",color="g",label='y color matching')
plt.plot(PPO_bis_MSB[:,0],z_match(PPO_bis_MSB[:,0]),alpha=0.7,lw=3,ls="--",color="b",label='z color matching')
plt.xlim(350,700)
plt.legend(loc='best',fontsize=16,frameon=False)

plt.show()