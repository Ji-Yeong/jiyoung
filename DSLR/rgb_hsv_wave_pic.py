import os
from re import T
import numpy as np
import matplotlib as mpl
#mpl.use('Agg')
import matplotlib.pyplot as plt
from skimage import io
from skimage.color import rgb2gray
from skimage.color import rgba2rgb
from skimage.color import rgb2hsv
from skimage.color import hsv2rgb
from matplotlib.pyplot import cm
from collections import OrderedDict

#import mplhep as hep
#hep.set_style(hep.style.ROOT) # For now ROOT defaults to CMS

import matplotlib.font_manager
matplotlib.font_manager.findSystemFonts(fontpaths=None, fontext='ttf')
#mpl.use("pgf")
pgf_with_pdflatex = {
    "pgf.texsystem": "pdflatex",
    "pgf.preamble": [
         r"\usepackage[utf8x]{inputenc}",
         r"\usepackage[T1]{fontenc}",
         r"\usepackage{cmbright}",
         ]
}
mpl.rcParams.update(pgf_with_pdflatex)
mpl.rcParams['font.family'] = 'serif'

#image5 = io.imread('PPO_6MeV_ref.JPG')
#image5 = io.imread('PPO_9_12MeV_ref.JPG')
image5 = io.imread('PPO_BM_ref.JPG')
#image5 = io.imread('0.PNG')
#image5 = io.imread('green+red.jpg')

#i="9MeV_v3.jpg"
i="PPO_BM_9MeV_1.JPG"
#i="PPO_9MeV_1.JPG"
#i="12MeV.PNG"
#i="test_image.png"
#i="test_image2.jpg"
#i="green+red.jpg"
image1 = io.imread('{}'.format(i))
#image1 = rgba2rgb(image1)
image12 = rgb2hsv(image1)
V_hsv = np.copy(image12)
#V_hsv[V_hsv[:,:,2]<0.1]=np.nan
V_hsv[V_hsv[:,:,2]<0.2]=np.nan
"""
image_b = io.imread('{}'.format(i))
image_b[:,:,0]=0
image_b[:,:,1]=0
image12 = rgb2hsv(image_b)
"""

red11 = image12[:,:,0]*360.0
red1 = np.copy(red11)
red1[red11>270.0]=np.nan

V_hsv11 = V_hsv[:,:,0]*360.0
V_hsv1 = np.copy(V_hsv11)
V_hsv1[V_hsv11>270.0]=np.nan

#wave1 = 700.0-320.0/270.0*(red1.ravel())
wave1 = 700.0-320.0/270.0*(red1.ravel())
wave_hsv = 700.0-320.0/270.0*(V_hsv1.ravel())
#wave1 = 360-red1.ravel()
#wave1 = wave1*700/360

blue1 = image12[:,:,2]
print(blue1.shape)
blue11= np.copy(blue1)
blue11[blue1<0.18]=np.nan

bin_heights, bin_borders, _= plt.hist(blue1.ravel(),bins=256,range=(0.0, 255.0),histtype="step")
bin_widths = np.diff(bin_borders)
bin_centers = bin_borders[:-1] + bin_widths / 2
x = bin_centers
y = bin_heights
#plt.clf()

grid = plt.GridSpec(2,2, wspace=0.1, hspace=0.2)
plt.subplot(grid[0,1])
plt.hist(image12[:,:,0].ravel(),bins=101,range=(0.0, 1.0),alpha=0.8,density=True,histtype="step",color="k",ls=":",lw=2,label="Hue (0~1.0)")
plt.hist(image12[:,:,1].ravel(),bins=101,range=(0.0, 1.0),alpha=0.8,density=True,histtype="step",color="m",ls="--",lw=2,label="Saturation (0~1.0)")
plt.hist(image12[:,:,2].ravel(),bins=101,range=(0.0, 1.0),alpha=0.8,density=True,histtype="step",color="c",ls="-",lw=2,label="Value (0~1.0)")
#plt.yscale('log')
plt.xlabel('Coefficient number in HSV',fontsize=20,horizontalalignment='right',x=1.0)
plt.ylabel('# of events [A.U.]',fontsize=20)
plt.yticks(fontsize=20)
plt.xticks(fontsize=20)
#plt.legend(loc='best',fontsize=16,frameon=False)
plt.legend(loc='best',fontsize=20,bbox_to_anchor=(0.5, 0.6),frameon=False)


plt.subplot(grid[0,0])
#plt.hist(image13[:,:,0].ravel(),bins=256,range=(0.0, 255.0),density=True, alpha=0.7,histtype="step",color="r",ls=":",lw=2,label="Red")
#plt.hist(image13[:,:,1].ravel(),bins=256,range=(0.0, 255.0),density=True, alpha=0.7,histtype="step",color="g",ls="--",lw=2,label="Green")
#plt.hist(image13[:,:,2].ravel(),bins=256,range=(0.0, 255.0),density=True, alpha=0.7,histtype="step",color="b",ls="-",lw=2,label="Blue")
plt.hist(image1[:,:,0].ravel(),bins=256,range=(0.0, 255.0),density=True, alpha=0.7,histtype="step",color="r",ls=":",lw=2,label="Red")
plt.hist(image1[:,:,1].ravel(),bins=256,range=(0.0, 255.0),density=True, alpha=0.7,histtype="step",color="g",ls="--",lw=2,label="Green")
plt.hist(image1[:,:,2].ravel(),bins=256,range=(0.0, 255.0),density=True, alpha=0.7,histtype="step",color="b",ls="-",lw=2,label="Blue")
#plt.axvline(x=153/3,color="y",ls="-.",lw=2,label="30 percent of 255")
plt.xlabel('Pixel intensity number in RGB',fontsize=20,horizontalalignment='right',x=1.0)
plt.ylabel('# of events [A.U.]',fontsize=20)
plt.yticks(fontsize=20)
plt.xticks(fontsize=20)
plt.ylim(0,1.5e-2)
#plt.ylim(0,2.5e-2)
plt.ticklabel_format(axis="y",style='sci',scilimits=(0,2e-2))
#plt.legend(loc='best',fontsize=16,frameon=False)
plt.legend(loc='best',fontsize=20,bbox_to_anchor=(0.85, 0.88),frameon=False)

plt.subplot(grid[1,0])
plt.hist(wave1, alpha=0.7,bins=361,histtype="step",density=True,color="k",label="wavelength")
plt.hist(wave_hsv, alpha=0.7,bins=361,histtype="step",density=True,color="r",label="wavelength(signal region)")
#plt.xlabel(r"Degree [$^\circ$]",fontsize=16,horizontalalignment='right',x=1.0)
plt.xlabel(r"wavelength [nm]",fontsize=20,horizontalalignment='right',x=1.0)
plt.ylabel(ylabel='# of events [A.U.]',fontsize=20)
plt.legend(loc='best',fontsize=20,frameon=False)
plt.xticks(np.arange(380, 700, step=20),fontsize=20)
plt.yticks(fontsize=20)
#$,[ "{0:.0f}".format(x*210/2480) for x in np.arange(0, 1480, step=100)])

plt.subplot(grid[1,1])
plt.clf()
#plt.imshow(image1)
plt.imshow(image5,alpha=1.0)
#plt.imshow(V_hsv,alpha=0.3,cmap=plt.cm.jet)
#plt.imshow(blue11,alpha=0.5,vmin=0.2,vmax=0.80,cmap=plt.cm.jet)
plt.imshow(blue11,alpha=0.3,cmap=plt.cm.jet)
#plt.colorbar()
plt.axis("off")
plt.show()

#plt.savefig("{}_processing.pdf".format(name),transparent=True,bbox_inches="tight",pad_inches=0.1)
#plt.savefig('Figure-4.pdf',dpi=300,transparent=True)
#plt.savefig('Figure-4.jpg', dpi=300, quality=95, optimize=True, progressive=True)  
plt.show()