import os
import numpy as np
import matplotlib.pyplot as plt
from skimage import io
from skimage.color import rgb2gray
from skimage.color import rgba2rgb
from skimage.color import rgb2hsv
import matplotlib as mpl

import mplhep as hep
hep.set_style(hep.style.ROOT) # For now ROOT defaults to CMS

#import matplotlib.font_manager
#matplotlib.font_manager.findSystemFonts(fontpaths=None, fontext='ttf')
##mpl.use("pgf")
#pgf_with_pdflatex = {
#    "pgf.texsystem": "pdflatex",
#    "pgf.preamble": [
#         r"\usepackage[utf8x]{inputenc}",
#         r"\usepackage[T1]{fontenc}",
#         r"\usepackage{cmbright}",
#         ]
#}
#mpl.rcParams.update(pgf_with_pdflatex)
#mpl.rcParams['font.family'] = 'serif'

image1 = io.imread('{}.JPG'.format("PPO_BM_12MeV_1"))
image2 = rgb2hsv(image1)
blue1 = image1[:,:,2]/np.max(image1[:,:,2])*255
hsv_v1 = image2[:,:,2]/np.max(image2[:,:,2])*100
blue11 = blue1.ravel()
hsv_new_v1 = np.copy(hsv_v1)

x = np.copy(blue1.flatten())
print(x.shape)
bin_heights, bin_borders, _= plt.hist(blue1.ravel(),bins=256,histtype="step")
bin_widths = np.diff(bin_borders)
bin_centers = bin_borders[:-1] + bin_widths / 2
x = bin_centers
y = bin_heights

image5 = io.imread('12MeV.PNG')
image6 = rgba2rgb(image5)
image6 = rgb2hsv(image6)

blue5 = image5[:,:,2]/np.max(image5[:,:,2])*255
hsv_v5 = image6[:,:,2]/np.max(image6[:,:,2])*100
blue5 = blue5.ravel()
hsv_new_v5 = np.copy(hsv_v5)

x5 = np.copy(blue5.flatten())
print(x.shape)
bin_heights5, bin_borders5, _= plt.hist(blue5.ravel(),bins=256,histtype="step")
bin_widths5 = np.diff(bin_borders5)
bin_centers5 = bin_borders5[:-1] + bin_widths5 / 2
x5 = bin_centers5
y5 = bin_heights5
plt.clf()


from lmfit.models import ExponentialModel,GaussianModel

exp_mod = ExponentialModel(prefix='exp_')
pars = exp_mod.guess(y, x=x)

"""
gauss1 = GaussianModel(prefix='g1_')
pars = gauss1.guess(y, x=x)

pars['g1_center'].set(value=1, min=0, max=3)
pars['g1_sigma'].set(value=10, min=3)
pars['g1_amplitude'].set(value=6e5, min=1e4)
"""
gauss2 = GaussianModel(prefix='g2_')
pars.update(gauss2.make_params())

pars['g2_center'].set(value=165, min=163, max=167)
pars['g2_sigma'].set(value=10, min=1)
pars['g2_amplitude'].set(value=1e4, min=1e3)

gauss3 = GaussianModel(prefix='g3_')
pars.update(gauss3.make_params())

pars['g3_center'].set(value=25, min=20, max=50)
pars['g3_sigma'].set(value=4, min=1)
pars['g3_amplitude'].set(value=1e5, min=1e3)

gauss4 = GaussianModel(prefix='g4_')
pars.update(gauss4.make_params())

pars['g4_center'].set(value=130, min=120, max=150)
pars['g4_sigma'].set(value=2, min=1)
pars['g4_amplitude'].set(value=1e3, min=1e2)

#mod = gauss1 + gauss2 + gauss3 + gauss4 
mod1 = exp_mod  + gauss2 + gauss3 + gauss4 

init = mod1.eval(pars, x=x)
out = mod1.fit(y, pars, x=x)

gauss5 = GaussianModel(prefix='g5_')
pars5 = gauss5.guess(y, x=x5)

pars5['g5_center'].set(value=12, min=0, max=15)
pars5['g5_sigma'].set(value=3, min=2)
pars5['g5_amplitude'].set(value=8e3, min=1e2)

gauss6 = GaussianModel(prefix='g6_')
pars5.update(gauss6.make_params())

pars5['g6_center'].set(value=81, min=60, max=100)
pars5['g6_sigma'].set(value=2, min=1)
pars5['g6_amplitude'].set(value=2e3, min=1e2)

gauss7 = GaussianModel(prefix='g7_')
pars5.update(gauss7.make_params())

pars5['g7_center'].set(value=31, min=20, max=50)
pars5['g7_sigma'].set(value=4, min=1)
pars5['g7_amplitude'].set(value=1.5e3, min=1e2)

mod5 = gauss5 + gauss6 + gauss7 

init5 = mod5.eval(pars5, x=x5)
out5 = mod5.fit(y5, pars5, x=x5)

print(out5.fit_report(min_correl=0.5))

comps = out.eval_components(x=x)
comps5 = out5.eval_components(x=x5)
d1,=plt.plot(x, y/y.max()*100,ls="none", color="b",marker="^",markersize=10,fillstyle='full',alpha=0.2,label='Measured data(DSLR)')
#axes[1].plot(x, comps['g1_'], 'g--', label='Gaussian component 1')
d2,=plt.plot(x, comps['g2_']/y.max()*100, 'g-',alpha=1.0,lw=2, label='PPO + bis-MSB(DSLR)')
#axes[1].fill_between(x, (comps['g2_']/y.max()*100).min(), comps['g2_']/y.max()*100, facecolor="r", alpha=0.1)
#axes[1].plot(x, comps['g3_']/y.max()*100, 'k--',alpha=0.6,lw=2, label='DSLR Gaussian component 2')
#axes[1].plot(x, comps['g4_']/y.max()*100, 'y--',alpha=0.6,lw=2, label='DSLR Gaussian component 3')
#axes[1].plot(x, comps['exp_']/y.max()*100, 'g--',alpha=0.6,lw=2, label='DSLR Exponential component')
d3,=plt.plot(x, out.best_fit/y.max()*100, 'g--',alpha=1,lw=4, label='Global fitting(DSLR)')
plt.fill_between(x, (comps['g2_']/y.max()*100).min(), comps['g2_']/y.max()*100, facecolor="g", alpha=0.05)
#axes[1].plot(x5, comps5['g5_']/y5.max()*100, 'm:',alpha=0.6,lw=2, label='Smartphone Gaussian component 1')
p1,=plt.plot(x5, y5/y5.max()*100,ls='none', color="b",marker="o",markersize=10,fillstyle='none',alpha=0.5,label='Measured data(mobile)')
p2,=plt.plot(x5, comps5['g6_']/y5.max()*100, 'r-.',alpha=1.0,lw=2, label='PPO + bis-MSB(mobile)')
plt.fill_between(x5, (comps5['g6_']/y5.max()*100).min(), comps5['g6_']/y5.max()*100, facecolor="r", alpha=0.05)
#axes[1].plot(x5, comps5['g7_']/y5.max()*100, 'y:',alpha=0.6,lw=2, label='Smartphone Gaussian component 3')
p3,=plt.plot(x5, out5.best_fit/y5.max()*100, 'r:',alpha=1,lw=4, label='Global fitting(mobile)')
plt.xlabel('Blue pixel intensity number',fontsize=20,horizontalalignment='right', x=1.0)
plt.ylabel('# of events [A.U.]',fontsize=20)
#plt.legend(loc='best',fontsize=30,frameon=False,bbox_to_anchor=(0.9, 0.93))
#axes[1].legend(loc='best',fontsize=24,frameon=False,bbox_to_anchor=(0.9, 0.85))
leg1=plt.legend([d1,d2,d3],['Measured data(DSLR)','PPO + bis-MSB(DSLR)','Gaussian fitting(DSLR)'],\
    loc='best',fontsize=20,frameon=False,bbox_to_anchor=(0.9, 0.60))
plt.legend([p1,p2,p3],['Measured data(Mobile)','PPO + bis-MSB(Mobile)','Gaussian fitting(Mobile)'],\
    loc='best',fontsize=20,frameon=False,bbox_to_anchor=(0.9, 0.90))
plt.gca().add_artist(leg1)
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
plt.show()