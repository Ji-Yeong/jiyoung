import os
import numpy as np
import matplotlib.pyplot as plt
from skimage import io
from skimage.color import rgb2gray
from skimage.color import rgba2rgb
from skimage.color import rgb2hsv
from matplotlib.pyplot import cm, xticks

import matplotlib as mpl
import matplotlib.font_manager
matplotlib.font_manager.findSystemFonts(fontpaths=None, fontext='ttf')
#mpl.use("pgf")
pgf_with_pdflatex = {
    "pgf.texsystem": "pdflatex",
    "pgf.preamble": [
         r"\usepackage[utf8x]{inputenc}",
         r"\usepackage[T1]{fontenc}",
         r"\usepackage{cmbright}",
         ]
}
mpl.rcParams.update(pgf_with_pdflatex)
mpl.rcParams['font.family'] = 'serif'

#image5 = io.imread('0.PNG')
i = "film_12MeV_1"
#i = "scan0021"
image5 = io.imread('film_12MeV_1.jpg')
image1 = io.imread('{}.jpg'.format(i))
image4 = io.imread('{}.jpg'.format(i))
#image1[:,:,0]=0
#image1[:,:,1]=0
image2 = rgb2gray(image4)
image3 = 1-image2
print(image2.shape)
pdd = np.mean(image3,axis=1)
pdd = pdd - pdd.min()
pdd_normal = pdd/pdd.max()*100
print(np.mean(image3,axis=1).shape)

grid = plt.GridSpec(2,2, wspace=0.2, hspace=0.2)

xx=588 # 50/0.085 , dpi for x  is 297/3508
plt.subplot(grid[0,0])
plt.imshow(image5,alpha=1.0)
"""
plt.xlabel(r'Width [mm]'+'\n'+r'(or Pixel number)', fontsize=26, horizontalalignment='right', x=1.0)
plt.ylabel(r'Depth [mm]'+r' (or Pixel number)',fontsize=26)
"""
#plt.xlabel(r'Width $\left( \dfrac{[mm]}{10} \right)$',fontsize=26,horizontalalignment='right', x=1.0)
#plt.ylabel(r'Depth $\left( \dfrac{[mm]}{10} \right)$',fontsize=26)
#plt.xticks(np.arange(0, 3508, step=500),labels=[ "{0:.0f}".format(x*297/3508) for x in np.arange(0, 3508, step=500)])
#plt.yticks(np.arange(0, 2480, step=500),labels=[ "{0:.0f}".format(x*210/2480) for x in np.arange(0, 2480, step=500)])
labels_x = ["0",'\n0',"50",'\n(588)',"100",'\n(1176)',"150",'\n(1764)',"200",'\n(2352)',"250",'\n(2940)',"297",'\n(3508)']
labels_y = ["0",'',"50",' (588)',"100",' (1176)',"150",' (1764)',"200",' (2352)',"210",' (2480)']
new_labels_x = [ ''.join(x) for x in zip(labels_x[0::2], labels_x[1::2]) ]
new_labels_y = [ ''.join(x) for x in zip(labels_y[0::2], labels_y[1::2]) ]

plt.xticks([0,xx,2*xx,3*xx,4*xx,5*xx,3508],new_labels_x,fontsize=10)
plt.yticks([0,xx,2*xx,3*xx,4*xx,2480],new_labels_y,fontsize=10)

#plt.xticks([0,xx,2*xx,3*xx,4*xx,5*xx,3508],labels=["0","50","100","150","200","250","297"])
#plt.yticks([0,xx,2*xx,3*xx,4*xx,2480],labels=["0","50","100","150","200","210"])


plt.subplot(grid[0,1])
plt.imshow(image1,alpha=1.0)
#plt.imshow(image1,alpha=1.0,origin='lower')
#,cmap=plt.cm.gray)
plt.xlabel(r'Width [mm]',fontsize=26)
#horizontalalignment='right', x=1.0)
plt.ylabel(r'Depth [mm]',fontsize=26)
plt.xticks(np.arange(0, 400, step=100),[ "{0:.0f}".format(x*297/3508) for x in np.arange(0, 400, step=100)])
plt.yticks(np.arange(0, 1768, step=100),[ "{0:.0f}".format(x*210/2480) for x in np.arange(0, 1768, step=100)])


plt.subplot(grid[1,0])
plt.hist(image1[:,:,0].ravel(),bins=256,range=(0.0, 255.0),density=True, alpha=0.7,ls="-",histtype="step",color="r",label="R")
plt.hist(image1[:,:,1].ravel(),bins=256,range=(0.0, 255.0),density=True, alpha=0.7,ls="--",histtype="step",color="g",label="G")
plt.hist(image1[:,:,2].ravel(),bins=256,range=(0.0, 255.0),density=True, alpha=0.7,ls="-.",histtype="step",color="b",label="B")
#plt.title(" {} RGB color space ".format(i) ,fontsize=12)
plt.legend(loc='best',fontsize=26,bbox_to_anchor=(0.92, 0.76),frameon=False)
plt.xlabel('Pixel intesity number',fontsize=26,horizontalalignment='right', x=1.0)
plt.ylabel('# of events [A.U.]',fontsize=26)
plt.xticks(fontsize=24)
plt.yticks(fontsize=24)
#plt.imshow(image2,alpha=1.0)

plt.subplot(grid[1,1])
plt.plot(np.arange(0, 1768),pdd_normal,color="k")
#print(pdd_normal.shape)
#plt.scatter(pdd_normal)
#plt.autoscale(enable=True, axis='y', tight=None)
#xticks=[100, 200, 300, 400, 500,600,700,800,900,1000,1100,1200,1300,1400,1500]
#plt.xticks(xticks,["$%f$" % x for x in xticks])
#plt.xticklabels(["$%.1f$" % y for y in xticks], fontsize=18)
plt.xlabel(r'Depth [mm]',fontsize=26, horizontalalignment='right', x=1.0)
plt.ylabel('Percentage depth dose [%]',fontsize=26)
plt.xticks(np.arange(0, 1768, step=100),[ "{0:.0f}".format(x*210/2480) for x in np.arange(0, 1768, step=100)])

plt.show()