import os   #파이선 해석기 상에서 리눅스 명령어 실행
from re import T
import numpy as np  #수치해석용 배열연산 라이브러리
import matplotlib.pyplot as plt #유명한 시각화
# as plt 해당 모듈호출을 plt라는 호출 예약어로 할당
from skimage import io  #이미지 분석용 라이브러리
from skimage.color import rgb2gray
from skimage.color import rgba2rgb
from skimage.color import rgb2hsv
from skimage.color import hsv2rgb
from matplotlib.pyplot import cm    #컬러 맵
from collections import OrderedDict # 정렬된 사전 기능

""" 이 부분은 latex 조판 시스템, 논문에서 쓰는 세리프 글자체 불러오기 위함 """
import matplotlib as mpl
import matplotlib.font_manager
matplotlib.font_manager.findSystemFonts(fontpaths=None, fontext='ttf')
pgf_with_pdflatex = {
    "pgf.texsystem": "pdflatex",
    "pgf.preamble": [
         r"\usepackage[utf8x]{inputenc}",
         r"\usepackage[T1]{fontenc}",
         r"\usepackage{cmbright}",
         ]
}
mpl.rcParams.update(pgf_with_pdflatex)
mpl.rcParams['font.family'] = 'serif'
""" """

image5 = io.imread('PPO_BM_ref.JPG') #이미지 합성시 배경 이미지
#filename = os.path.join('6MeV.PNG')    #skimage의 io 말고 py.os 이용시 문법
#image1 = io.imread(filename)

i="PPO_BM_9MeV_1.JPG"
image1 = io.imread('{}'.format(i))  #분석할 이미지 파일 불러오기
#대부분은 jpg 이미지 파일은 rgb 성분임

image12 = rgb2hsv(image1) #rgb->hsv색조,채도,명도(밝기) 기저로 변환
#간혹 가다가 png 파일은 rgba 되어 있음, 여기서 alpha는 불투명도.
#rgba->rgb->hsv
V_hsv = np.copy(image12)    # values 0.2 이상만 추릴려고 복사(신호 후부 지역만 추출)
print(V_hsv.shape)  #예제로 배열 차원 확인

V_hsv[V_hsv[:,:,2]<0.2]=np.nan  # numpy의 인덱셍, 슬라이싱 문법 , 논리 제어
#배열 논리 제어한 것임. 즉 values값이 0.2 보다 낮은 값은 0으로 할당한 효과\
#0으로 할당해서 표시하면 0으로된것들 어둡게나와서 그것들 배제하려고 np.nan 할당

red11 = image12[:,:,0]*360.0    #변환된 hsv값 0~1으로 규격화된 값임.다시 역 규격화
red1 = np.copy(red11)   #hue 270~360 성분은 1개의 rgb 주색으로 분리가 안됨.
red1[red11>270.0]=np.nan    #그래서 제외하는 것임

V_hsv11 = V_hsv[:,:,0]*360.0
V_hsv1 = np.copy(V_hsv11)
V_hsv1[V_hsv11>270.0]=np.nan

wave1 = 700.0-320.0/270.0*(red1.ravel())
wave_hsv = 700.0-320.0/270.0*(V_hsv1.ravel())

blue1 = image12[:,:,2]
print(blue1.shape)
blue11= np.copy(blue1)
blue11[blue1<0.2]=np.nan

"""
#참고로 히스토그램에서 x,y 좌표 저장하는것 예제로 넣었음
bin_heights, bin_borders, _= plt.hist(blue1.ravel(),bins=256,range=(0.0, 255.0),histtype="step")
bin_widths = np.diff(bin_borders)
bin_centers = bin_borders[:-1] + bin_widths / 2
x = bin_centers
y = bin_heights
#plt.clf()  #matplotlib pyplot 및 imshow 대표적인 시각화 명령어니 
#구글에서 검색하면서 구현 해볼것.
"""


#시각화 부분, 이부분은 검색으로 알아 볼 것. 
#데이터 전처리는 이미 다했음. 그 다음에 시각화하는데 시간을 많이 쓸것임.
#명령어가 뭔지검색으로 훈련하길 바람.
grid = plt.GridSpec(2,2, wspace=0.1, hspace=0.2)
plt.subplot(grid[0,1])
plt.hist(image12[:,:,0].ravel(),bins=101,range=(0.0, 1.0),alpha=0.8,density=True,histtype="step",color="k",ls=":",lw=2,label="Hue (0~1.0)")
plt.hist(image12[:,:,1].ravel(),bins=101,range=(0.0, 1.0),alpha=0.8,density=True,histtype="step",color="m",ls="--",lw=2,label="Saturation (0~1.0)")
plt.hist(image12[:,:,2].ravel(),bins=101,range=(0.0, 1.0),alpha=0.8,density=True,histtype="step",color="c",ls="-",lw=2,label="Value (0~1.0)")
#plt.yscale('log')
plt.xlabel('Coefficient number in HSV',fontsize=16,horizontalalignment='right',x=1.0)
plt.ylabel('# of events [A.U.]',fontsize=16)
#plt.legend(loc='best',fontsize=16,frameon=False)
plt.legend(loc='best',fontsize=16,bbox_to_anchor=(0.5, 0.6),frameon=False)


plt.subplot(grid[0,0])
#plt.hist(image13[:,:,0].ravel(),bins=256,range=(0.0, 255.0),density=True, alpha=0.7,histtype="step",color="r",ls=":",lw=2,label="Red")
#plt.hist(image13[:,:,1].ravel(),bins=256,range=(0.0, 255.0),density=True, alpha=0.7,histtype="step",color="g",ls="--",lw=2,label="Green")
#plt.hist(image13[:,:,2].ravel(),bins=256,range=(0.0, 255.0),density=True, alpha=0.7,histtype="step",color="b",ls="-",lw=2,label="Blue")
plt.hist(image1[:,:,0].ravel(),bins=256,range=(0.0, 255.0),density=True, alpha=0.7,histtype="step",color="r",ls=":",lw=2,label="Red")
plt.hist(image1[:,:,1].ravel(),bins=256,range=(0.0, 255.0),density=True, alpha=0.7,histtype="step",color="g",ls="--",lw=2,label="Green")
plt.hist(image1[:,:,2].ravel(),bins=256,range=(0.0, 255.0),density=True, alpha=0.7,histtype="step",color="b",ls="-",lw=2,label="Blue")
#plt.axvline(x=153/3,color="y",ls="-.",lw=2,label="30 percent of 255")
plt.xlabel('Pixel intensity number in RGB',fontsize=16,horizontalalignment='right',x=1.0)
plt.ylabel('# of events [A.U.]',fontsize=16)
plt.ylim(0,1.5e-2)
#plt.ylim(0,2.5e-2)
plt.ticklabel_format(axis="y",style='sci',scilimits=(0,2e-2))
#plt.legend(loc='best',fontsize=16,frameon=False)
plt.legend(loc='best',fontsize=16,bbox_to_anchor=(0.85, 0.88),frameon=False)

plt.subplot(grid[1,0])
plt.hist(wave1, alpha=0.7,bins=361,histtype="step",density=True,color="k",label="wavelength")
plt.hist(wave_hsv, alpha=0.7,bins=361,histtype="step",density=True,color="r",label="wavelength(signal region)")
#plt.xlabel(r"Degree [$^\circ$]",fontsize=16,horizontalalignment='right',x=1.0)
plt.xlabel(r"wavelength [nm]",fontsize=16,horizontalalignment='right',x=1.0)
plt.ylabel(ylabel='# of events [A.U.]',fontsize=16)
plt.legend(loc='best',fontsize=16,frameon=False)
plt.xticks(np.arange(380, 700, step=20))
#$,[ "{0:.0f}".format(x*210/2480) for x in np.arange(0, 1480, step=100)])

plt.subplot(grid[1,1])
#plt.imshow(image1)
plt.imshow(image5,alpha=1.0)
#plt.imshow(V_hsv,alpha=0.3,cmap=plt.cm.jet)
plt.imshow(blue11,alpha=0.3,cmap=plt.cm.jet)
plt.axis("off")
#plt.savefig("{}_processing.pdf".format(name),transparent=True,bbox_inches="tight",pad_inches=0.1)
#plt.savefig('Figure-4.pdf',dpi=300,transparent=True)
#plt.savefig('Figure-4.jpg', dpi=300, quality=95, optimize=True, progressive=True)  
plt.show()  #   스크린상에 보여주기,ROOT의 Draw와 같음
#plt.show() 메소드와 plt.savefig() 한 스크립트상에서 둘다 사용하고 싶으면
#savefig() 먼저 실행한 후 show() 메소드 순으로 할것. 
