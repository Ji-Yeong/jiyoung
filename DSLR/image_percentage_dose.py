import os
import numpy as np
import matplotlib.pyplot as plt
from skimage import io
from skimage.color import rgb2gray
from skimage.color import rgba2rgb
from skimage.color import rgb2hsv
from skimage.color import hsv2rgb
from matplotlib.pyplot import cm
from collections import OrderedDict

import matplotlib as mpl
import matplotlib.font_manager
matplotlib.font_manager.findSystemFonts(fontpaths=None, fontext='ttf')
#mpl.use("pgf")
pgf_with_pdflatex = {
    "pgf.texsystem": "pdflatex",
    "pgf.preamble": [
         r"\usepackage[utf8x]{inputenc}",
         r"\usepackage[T1]{fontenc}",
         r"\usepackage{cmbright}",
         ]
}
mpl.rcParams.update(pgf_with_pdflatex)
mpl.rcParams['font.family'] = 'serif'

#image5 = io.imread('PPO_6MeV_ref.JPG')
#image5 = io.imread('PPO_9_12MeV_ref.JPG')
image5 = io.imread('PPO_BM_ref.JPG')
#image5 = io.imread('0.PNG')

#i="9MeV_v3.jpg"
i="PPO_BM_9MeV_1.JPG"
#i="PPO_9MeV_1.JPG"
#i="9MeV.PNG"
#i="test_image.png"
#i="test_image2.jpg"
image1 = io.imread('{}'.format(i))

image12 = rgb2hsv(image1)
V_hsv_ref = np.copy(image12)

V_hsv_1 = np.copy(image12)
V_hsv_1[V_hsv_ref[:,:,2]<0.1]=np.nan
V_hsv_2 = np.copy(image12)
V_hsv_2[V_hsv_ref[:,:,2]<0.2]=np.nan
V_hsv_3 = np.copy(image12)
V_hsv_3[V_hsv_ref[:,:,2]<0.3]=np.nan
V_hsv_4 = np.copy(image12)
V_hsv_4[V_hsv_ref[:,:,2]<0.4]=np.nan
V_hsv_5 = np.copy(image12)
V_hsv_5[V_hsv_ref[:,:,2]<0.5]=np.nan
V_hsv_6 = np.copy(image12)
V_hsv_6[V_hsv_ref[:,:,2]<0.6]=np.nan
V_hsv_7 = np.copy(image12)
V_hsv_7[V_hsv_ref[:,:,2]<0.7]=np.nan
V_hsv_8 = np.copy(image12)
V_hsv_8[V_hsv_ref[:,:,2]<0.8]=np.nan

"""
V_hsv_1 = np.copy(image12)
V_hsv_1[V_hsv_ref[:,:,2]<0.1]=np.nan
V_hsv_2 = np.copy(image12)
V_hsv_2[V_hsv_ref[:,:,2]<0.2]=np.nan
V_hsv_3 = np.copy(image12)
V_hsv_3[V_hsv_ref[:,:,2]<0.3]=np.nan
V_hsv_4 = np.copy(image12)
V_hsv_4[V_hsv_ref[:,:,2]<0.4]=np.nan
V_hsv_5 = np.copy(image12)
V_hsv_5[V_hsv_ref[:,:,2]<0.5]=np.nan
V_hsv_6 = np.copy(image12)
V_hsv_6[V_hsv_ref[:,:,2]<0.6]=np.nan
V_hsv_7 = np.copy(image12)
V_hsv_7[V_hsv_ref[:,:,2]<0.7]=np.nan
V_hsv_8 = np.copy(image12)
V_hsv_8[V_hsv_ref[:,:,2]<0.8]=np.nan
"""

grid = plt.GridSpec(2,4, wspace=0.1, hspace=0.2)
plt.subplot(grid[0,0])
plt.imshow(image5,alpha=1.0)
plt.imshow(V_hsv_1,alpha=0.3,cmap='hsv')
plt.axis("off")

plt.subplot(grid[0,1])
plt.imshow(image5,alpha=1.0)
plt.imshow(V_hsv_2,alpha=0.3,cmap=plt.cm.hsv)
plt.axis("off")

plt.subplot(grid[0,2])
plt.imshow(image5,alpha=1.0)
plt.imshow(V_hsv_3,alpha=0.3,cmap=plt.cm.hsv)
plt.axis("off")

plt.subplot(grid[0,3])
plt.imshow(image5,alpha=1.0)
plt.imshow(V_hsv_4,alpha=0.3,cmap=plt.cm.hsv)
plt.axis("off")

plt.subplot(grid[1,0])
plt.imshow(image5,alpha=1.0)
plt.imshow(V_hsv_5,alpha=0.3,cmap=plt.cm.hsv)
plt.axis("off")

plt.subplot(grid[1,1])
plt.imshow(image5,alpha=1.0)
plt.imshow(V_hsv_6,alpha=0.3,cmap=plt.cm.hsv)
plt.axis("off")

plt.subplot(grid[1,2])
plt.imshow(image5,alpha=1.0)
plt.imshow(V_hsv_7,alpha=0.3,cmap=plt.cm.hsv)
plt.axis("off")

plt.subplot(grid[1,3])
plt.imshow(image5,alpha=1.0)
plt.imshow(V_hsv_8,alpha=0.3,cmap=plt.cm.hsv)
plt.axis("off")


plt.show()

#plt.savefig("{}_processing.pdf".format(name),transparent=True,bbox_inches="tight",pad_inches=0.1)
#plt.savefig('Figure-4.pdf',dpi=300,transparent=True)
#plt.savefig('Figure-4.jpg', dpi=300, quality=95, optimize=True, progressive=True)  