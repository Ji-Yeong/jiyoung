import os
import numpy as np
import matplotlib.pyplot as plt
from skimage import io
from skimage.color import rgb2gray
from skimage import filters
from skimage.data import camera
from skimage.util import compare_images
from skimage.filters import gaussian
from skimage.segmentation import active_contour
from skimage.filters import try_all_threshold
from skimage import data, img_as_float
from skimage.segmentation import chan_vese

filename = os.path.join('6MeV.PNG')
#image = io.imread('12MeV.PNG')
image = io.imread(filename)
image_gray = rgb2gray(image)
#image = img_as_float(img)
# Feel free to play around with the parameters to see how they impact the result
print(image.shape)
cv = chan_vese(image[:,:,0], mu=0.25, lambda1=1, lambda2=1, tol=1e-3, max_iter=200,
               dt=0.5, init_level_set="checkerboard", extended_output=True)
cv1 = chan_vese(image[:,:,1], mu=0.25, lambda1=1, lambda2=1, tol=1e-3, max_iter=200,
               dt=0.5, init_level_set="checkerboard", extended_output=True)
cv2 = chan_vese(image[:,:,2], mu=0.25, lambda1=1, lambda2=1, tol=1e-3, max_iter=200,
               dt=0.5, init_level_set="checkerboard", extended_output=True)
cv_gray = chan_vese(image_gray, mu=0.25, lambda1=1, lambda2=1, tol=1e-3, max_iter=200,
               dt=0.5, init_level_set="checkerboard", extended_output=True)

fig, axes = plt.subplots(2, 4, figsize=(12, 12))
ax = axes.flatten()

ax[0].imshow(image )
ax[0].set_axis_off()
ax[0].set_title("Original Image", fontsize=12)

ax[1].imshow(cv[0],cmap=plt.cm.Reds)
ax[1].set_axis_off()
title = "Chan-Vese segmentation - {} iterations for Red".format(len(cv[2]))
ax[1].set_title(title, fontsize=12)

ax[2].imshow(cv[1])
ax[2].set_axis_off()
ax[2].set_title("Final Level Set for Red", fontsize=12)

ax[3].imshow(cv1[0],cmap=plt.cm.Greens)
ax[3].set_axis_off()
title = "Chan-Vese segmentation - {} iterations for Green".format(len(cv1[2]))
ax[3].set_title(title, fontsize=12)

ax[4].imshow(cv1[1])
ax[4].set_axis_off()
ax[4].set_title("Final Level Set for Green", fontsize=12)

ax[5].imshow(cv2[0],cmap=plt.cm.Blues)
ax[5].set_axis_off()
title = "Chan-Vese segmentation - {} iterations for Blue".format(len(cv2[2]))
ax[5].set_title(title, fontsize=12)

ax[6].imshow(cv2[1])
ax[6].set_axis_off()
ax[6].set_title("Final Level Set for Blue", fontsize=12)

ax[7].imshow(cv_gray[0])
ax[7].set_axis_off()
title = "Chan-Vese segmentation - {} iterations for Gray".format(len(cv_gray[2]))
ax[7].set_title(title, fontsize=12)

#ax[3].plot(cv2[2])
#ax[3].set_title("Evolution of energy over iterations", fontsize=12)

fig.tight_layout()
plt.subplots_adjust(wspace=0, hspace=0)
plt.show()