import os
import numpy as np
import matplotlib.pyplot as plt
from skimage import io
from skimage.color import rgb2gray
from skimage.color import rgba2rgb
from skimage.color import rgb2hsv
from skimage import filters
from skimage.data import camera
from skimage.util import compare_images
from skimage.filters import gaussian
from skimage.segmentation import active_contour
from skimage.filters import try_all_threshold
from skimage import data, img_as_float
from skimage.segmentation import chan_vese


#i="6mev_p"
i="6MeV"
#name = ["6MeV","9MeV","12MeV"]
image1 = io.imread('{}.PNG'.format(i))
#image11 = io.imread('{}.PNG'.format(i))
image11 = rgba2rgb(image1)
image12 = rgb2hsv(image11)

red1 = image12[:,:,0]
red11 = image12[:,:,0]*360
red1 = np.copy(red11)
red1[red11>270]=np.nan

wave1 = 650.0-250.0/270.0*(red1.ravel())
green1 = image12[:,:,1]
blue1 = image12[:,:,2]
weights_red1 = np.ones_like(red1.ravel())/float(len(red1.ravel()))
weights_green1 = np.ones_like(green1.ravel())/float(len(green1.ravel()))
weights_blue1 = np.ones_like(blue1.ravel())/float(len(blue1.ravel()))


#j="9mev_p"
j="9MeV"
#name = ["6MeV","9MeV","12MeV"]
image2 = io.imread('{}.PNG'.format(j))
#image21 = io.imread('{}.PNG'.format(j))
image21 = rgba2rgb(image2)
image22 = rgb2hsv(image21)

red2 = image22[:,:,0]
red22 = image22[:,:,0]*360
red2 = np.copy(red22)
red2[red22>270]=np.nan
wave2 = 650.0-250.0/270.0*(red2.ravel())
green2 = image22[:,:,1]
blue2 = image22[:,:,2]

"""
weights_red2 = np.ones_like(red2.ravel())/float(len(red2.ravel()))
weights_green2 = np.ones_like(green2.ravel())/float(len(green2.ravel()))
weights_blue2 = np.ones_like(blue2.ravel())/float(len(blue2.ravel()))
"""
#k="12mev_p"
k="12MeV"
#name = ["6MeV","9MeV","12MeV"]
image3 = io.imread('{}.PNG'.format(k))
#image31 = io.imread('{}.PNG'.format(k))
image31 = rgba2rgb(image3)
image32 = rgb2hsv(image31)
red3 = image32[:,:,0]
red33 = image32[:,:,0]*360
red3 = np.copy(red33)
red3[red33>270]=np.nan
wave3 = 650.0-250.0/270.0*(red3.ravel())
green3 = image32[:,:,1]
blue3 = image32[:,:,2]
"""
weights_red3 = np.ones_like(red3.ravel())/float(len(red3.ravel()))
weights_green3 = np.ones_like(green3.ravel())/float(len(green3.ravel()))
weights_blue3 = np.ones_like(blue3.ravel())/float(len(blue3.ravel()))
"""
grid = plt.GridSpec(2,3, wspace=0.1, hspace=0.1)
plt.subplot(grid[0,0])
#plt.hist(red.ravel(),bins=2560, range=(0.0, 255.0),alpha=0.5)
plt.hist(red1.ravel(),bins=361,alpha=0.7,histtype="step",color="r",label="H")
plt.hist(green1.ravel(),bins=101,alpha=0.7,histtype="step",color="g",label="S")
plt.hist(blue1.ravel(),bins=101,alpha=0.7,histtype="step",color="b",label="V")
plt.yscale('log')
#plt.xscale('log')
plt.title("Histogram {} for HSV color space ".format(i) ,fontsize=12)
plt.subplot(grid[0,1])
plt.hist(red2.ravel(),bins=361, alpha=0.7,histtype="step",color="r",label="H")
plt.hist(green2.ravel(),bins=101, alpha=0.7,histtype="step",color="g",label="S")
plt.hist(blue2.ravel(),bins=101, alpha=0.7,histtype="step",color="b",label="V")
plt.yscale('log')
#plt.xscale('log')
plt.title("Histogram {} for HSV color space ".format(j), fontsize=12)
plt.subplot(grid[0,2])
plt.hist(red3.ravel(),bins=360, alpha=0.7,histtype="step",color="r",label="H")
plt.hist(green3.ravel(),bins=101,alpha=0.7,histtype="step",color="g",label="S")
plt.hist(blue3.ravel(),bins=101, alpha=0.7,histtype="step",color="b",label="V")
plt.yscale('log')
#plt.xscale('log')
plt.title("Histogram {} for HSV space range ".format(k), fontsize=12)

plt.subplot(grid[1,0])
plt.hist(wave1, alpha=0.7,bins=370,histtype="step",color="gray")
plt.axvline(x=421,label="bM $\lambda$= 421 nm line")
#plt.hist(red1.ravel()*360.0,bins=361,alpha=0.7,histtype="step",label="H")
plt.yscale('log')
plt.title(" {} wavelength convert to HSV color space ".format(i) ,fontsize=12)
plt.subplot(grid[1,1])
plt.hist(wave2, alpha=0.7,bins=370,histtype="step",color="gray")
plt.axvline(x=421,label="bM $\lambda$= 421 nm line")
#plt.hist(red2.ravel()*360.0,bins=361,alpha=0.7,histtype="step",label="H")
plt.yscale('log')
#plt.xscale('log')
plt.title(" {} wavelength convert to HSV color space".format(j), fontsize=12)
plt.subplot(grid[1,2])
plt.hist(wave3, alpha=0.7,bins=370,histtype="step",color="gray")
plt.axvline(x=421,label="bM $\lambda$= 421 nm line")
#plt.hist(red3.ravel()*360.0,bins=361,alpha=0.7,histtype="step",label="H")
plt.yscale('log')
#plt.xscale('log')
#plt.legend(loc='best',fontsize=14)
plt.title(" {} wavelength convert to HSV color space".format(k), fontsize=12)

"""
plt.subplot(grid[1,0])
#plt.hist(blue1.ravel(),bins=2560, range=(0.0, 255.0),alpha=0.9,fc='b',histtype="step")
#plt.yscale('log')
plt.imshow(image1)
plt.title("Histogram {} for Blue channel on range (Dark=0,Bright=255)".format(i), fontsize=12)
plt.subplot(grid[1,1])
#plt.hist(blue2.ravel(),bins=2560, range=(0.0, 255.0),alpha=0.9,fc='b',histtype="step")
#plt.yscale('log')
plt.imshow(image2)
plt.title("Histogram {} for Blue channel on range (Dark=0,Bright=255)".format(j), fontsize=12)
plt.subplot(grid[1,2])
#plt.hist(blue3.ravel(),bins=2560, range=(0.0, 255.0),alpha=0.9,fc='b',histtype="step")
#plt.yscale('log')
plt.imshow(image3)
plt.title("Histogram {} for Blue channel on range (Dark=0,Bright=255)".format(k), fontsize=12)
#plt.subplots_adjust(wspace=1.0, hspace=1.5)
#fig.tight_layout(pad=0.4, w_pad=1.0, h_pad=1.5)
"""
#plt.legend(loc='best',fontsize=14)
plt.show()

#plt.savefig("{}_processing.pdf".format(name),transparent=True,bbox_inches="tight",pad_inches=0.1)
