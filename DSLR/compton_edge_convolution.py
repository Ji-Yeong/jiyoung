import numpy as np
import matplotlib.pyplot as plt
import scipy as sp
import scipy.special

n=500000
E = np.linspace(-550.0,1250.0,n)
E_c = 500.0

def H(E):
    h = np.zeros([n])
    for i in range(0,n):
        if E[i] < E_c:
            h[i] = 1
        elif E[i] == E_c:
            h[i] = 1
        else:
            h[i] = 0
    return h

a = 7.0e-3
b = 1.0e-2
c = 8.0e+3

def f(E):
    r = np.zeros([n])
    for i in range(0,n):
        if E[i] < E_c:
            r[i] = a*np.power(E[i],2)+b*E[i]+c
        elif E[i] == E_c:
            r[i] = a*np.power(E[i],2)+b*E[i]+c
        else:
            r[i] = 0
    return r

def g(E,sigma):
    k = np.zeros([n])
    for i in range(0,n):
        k[i] = 5e+3*np.exp(-np.power(E[i],2)/(2*sigma))/(np.sqrt(2*np.pi)*sigma)
    return k

def response(sigma) :
    alpha_1 = 1/2*(a*(np.power(E,2)+np.power(sigma,2))+b*E+c)
    beta_1 = -sigma/np.sqrt(2*np.pi)*a*(E+E_c)+b
    alpha_2 = sp.special.erfc((E-E_c)/(np.sqrt(2)*sigma))
    beta_2 = np.exp(-np.power((E-E_c),2)/(2*np.power(sigma,2)))
    total = alpha_1*alpha_2 + beta_1*beta_2
    return total

plt.plot(E,f(E),label="ideal continuum, edge signal",lw = 2 ,ls = "--" )
plt.plot(E,response(240),label="response with $\sigma$ = 240",alpha=0.5)
plt.plot(E,response(200),label="response with $\sigma$ = 200",alpha=0.5)
plt.plot(E,response(160),label="response with $\sigma$ = 160",alpha=0.5)
plt.plot(E,response(120),label="response with $\sigma$ = 120",alpha=0.5)
plt.plot(E,response(80),label="response with $\sigma$ = 80",alpha=0.5)
plt.plot(E,response(40),label="response with $\sigma$ = 40",alpha=0.5)
plt.plot(E,response(20),label="response with $\sigma$ = 20",alpha=0.5)
plt.plot(E,response(10),label="response with $\sigma$ = 10",alpha=0.5)
plt.xlabel('Energy or channel of MCA',fontsize=22)
#plt.gca().xaxis.set_ticklabels([])
plt.ylabel('A.U',fontsize=22)
#plt.yscale("log")
plt.title('Detector response to signal (or signal convolution with gaussian kernel)',fontsize = 24)
plt.legend(loc='best',fontsize=22)
plt.show()