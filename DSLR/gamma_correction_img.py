import os
import numpy as np
import matplotlib.pyplot as plt
from skimage import io
from skimage.color import rgb2gray,rgba2rgb,rgb2hsv,hsv2rgb
from skimage import exposure
from matplotlib.pyplot import cm
from collections import OrderedDict

import matplotlib as mpl
import matplotlib.font_manager
matplotlib.font_manager.findSystemFonts(fontpaths=None, fontext='ttf')
#mpl.use("pgf")
pgf_with_pdflatex = {
    "pgf.texsystem": "pdflatex",
    "pgf.preamble": [
         r"\usepackage[utf8x]{inputenc}",
         r"\usepackage[T1]{fontenc}",
         r"\usepackage{cmbright}",
         ]
}
mpl.rcParams.update(pgf_with_pdflatex)
mpl.rcParams['font.family'] = 'serif'

#image5 = io.imread('PPO_6MeV_ref.JPG')
#image5 = io.imread('PPO_9_12MeV_ref.JPG')
image5 = io.imread('PPO_BM_ref.JPG')
#image5 = io.imread('0.PNG')

#i="9MeV_v3.jpg"
i="PPO_BM_9MeV_1.JPG"
#i="PPO_9MeV_1.JPG"
#i="9MeV.PNG"
#i="test_image.png"
#i="test_image2.jpg"
image1 = io.imread('{}'.format(i))

gamma_corrected_1 = exposure.adjust_gamma(image1,gamma=1,gain=1)
gamma_corrected_2 = exposure.adjust_gamma(image1,gamma=0.5,gain=1)
gamma_corrected_3 = exposure.adjust_gamma(image1,gamma=1.5,gain=1)
#image12 = rgb2hsv(image1)
#V_hsv_ref = np.copy(image12)
#V_hsv_1 = np.copy(image12)
#V_hsv_1[V_hsv_ref[:,:,2]<0.1]=np.nan
gamma_corrected_4 = exposure.adjust_gamma(image1,gamma=1,gain=0.5)
gamma_corrected_5 = exposure.adjust_gamma(image1,gamma=1,gain=1.5)


grid = plt.GridSpec(4,3, wspace=0.1, hspace=0.2)
#plt.subplot(grid[0,0])
#plt.imshow(image1)
#plt.title("original")
#plt.axis("off")

plt.subplot(grid[0:2,0])
plt.title("gamma=1,gain=1 or original")
plt.imshow(gamma_corrected_1)
plt.axis("off")

plt.subplot(grid[0,1])
plt.title("gamma=0.5,gain=1")
plt.imshow(gamma_corrected_2)
plt.axis("off")

plt.subplot(grid[0,2])
plt.title("gamma=1.5,gain=1")
plt.imshow(gamma_corrected_3)
plt.axis("off")

plt.subplot(grid[1,1])
plt.title("gamma=1,gain=0.5")
plt.imshow(gamma_corrected_4)
plt.axis("off")

plt.subplot(grid[1,2])
plt.title("gamma=1,gain=1.5")
plt.imshow(gamma_corrected_5)
plt.axis("off")

plt.subplot(grid[2:4,0])
plt.title("gamma=1,gain=1 and original")
plt.hist(gamma_corrected_1[:,:,0].ravel(),bins=2**16,range=(0.0, 255.0),density=True, alpha=0.7,histtype="step",color="r",ls=":",lw=2,label="Red")
plt.hist(gamma_corrected_1[:,:,1].ravel(),bins=2**16,range=(0.0, 255.0),density=True, alpha=0.7,histtype="step",color="g",ls="--",lw=2,label="Green")
plt.hist(gamma_corrected_1[:,:,2].ravel(),bins=2**16,range=(0.0, 255.0),density=True, alpha=0.7,histtype="step",color="b",ls="-",lw=2,label="Blue")
plt.ylim(0,5e-2)
plt.ticklabel_format(axis="y",style='sci',scilimits=(0,2e-2))

plt.subplot(grid[2,1])
plt.title("gamma=0.5,gain=1")
plt.hist(gamma_corrected_2[:,:,0].ravel(),bins=256,range=(0.0, 255.0),density=True, alpha=0.7,histtype="step",color="r",ls=":",lw=2,label="Red")
plt.hist(gamma_corrected_2[:,:,1].ravel(),bins=256,range=(0.0, 255.0),density=True, alpha=0.7,histtype="step",color="g",ls="--",lw=2,label="Green")
plt.hist(gamma_corrected_2[:,:,2].ravel(),bins=256,range=(0.0, 255.0),density=True, alpha=0.7,histtype="step",color="b",ls="-",lw=2,label="Blue")
plt.ylim(0,5e-2)
plt.ticklabel_format(axis="y",style='sci',scilimits=(0,2e-2))

plt.subplot(grid[2,2])
plt.title("gamma=1.5,gain=1")
plt.hist(gamma_corrected_3[:,:,0].ravel(),bins=256,range=(0.0, 255.0),density=True, alpha=0.7,histtype="step",color="r",ls=":",lw=2,label="Red")
plt.hist(gamma_corrected_3[:,:,1].ravel(),bins=256,range=(0.0, 255.0),density=True, alpha=0.7,histtype="step",color="g",ls="--",lw=2,label="Green")
plt.hist(gamma_corrected_3[:,:,2].ravel(),bins=256,range=(0.0, 255.0),density=True, alpha=0.7,histtype="step",color="b",ls="-",lw=2,label="Blue")
plt.ylim(0,5e-2)
plt.ticklabel_format(axis="y",style='sci',scilimits=(0,2e-2))

plt.subplot(grid[3,1])
plt.title("gamma=1.0,gain=0.5")
plt.hist(gamma_corrected_4[:,:,0].ravel(),bins=256,range=(0.0, 255.0),density=True, alpha=0.7,histtype="step",color="r",ls=":",lw=2,label="Red")
plt.hist(gamma_corrected_4[:,:,1].ravel(),bins=256,range=(0.0, 255.0),density=True, alpha=0.7,histtype="step",color="g",ls="--",lw=2,label="Green")
plt.hist(gamma_corrected_4[:,:,2].ravel(),bins=256,range=(0.0, 255.0),density=True, alpha=0.7,histtype="step",color="b",ls="-",lw=2,label="Blue")
plt.ylim(0,5e-2)
plt.ticklabel_format(axis="y",style='sci',scilimits=(0,2e-2))

plt.subplot(grid[3,2])
plt.title("gamma=1,gain=1.5")
plt.hist(gamma_corrected_5[:,:,0].ravel(),bins=256,range=(0.0, 255.0),density=True, alpha=0.7,histtype="step",color="r",ls=":",lw=2,label="Red")
plt.hist(gamma_corrected_5[:,:,1].ravel(),bins=256,range=(0.0, 255.0),density=True, alpha=0.7,histtype="step",color="g",ls="--",lw=2,label="Green")
plt.hist(gamma_corrected_5[:,:,2].ravel(),bins=256,range=(0.0, 255.0),density=True, alpha=0.7,histtype="step",color="b",ls="-",lw=2,label="Blue")
plt.ylim(0,5e-2)
plt.ticklabel_format(axis="y",style='sci',scilimits=(0,2e-2))





plt.show()

#plt.savefig("{}_processing.pdf".format(name),transparent=True,bbox_inches="tight",pad_inches=0.1)
#plt.savefig('Figure-4.pdf',dpi=300,transparent=True)
#plt.savefig('Figure-4.jpg', dpi=300, quality=95, optimize=True, progressive=True)  