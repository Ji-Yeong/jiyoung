import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm  

file1 = "2Ee-only-Emission.txt"
file2 = "2Ee70-H2O30-PPO0.63g-Emission.txt"
file3 = "2Ee90-H2O10-PPO1.05g+bM0.0042g-Emission.txt"

two_E_ethanol = np.genfromtxt(file1,dtype=np.float64)
PPO = np.genfromtxt(file2,dtype=np.float64)
PPO_bis_MSB = np.genfromtxt(file3,dtype=np.float64)

from lmfit.models import SkewedGaussianModel,GaussianModel

gauss1 = GaussianModel(prefix='g1_')
pars1 = gauss1.guess(PPO_bis_MSB[:,1], x=PPO_bis_MSB[:,0])
pars1['g1_center'].set(value=405, min=380, max=700)
pars1['g1_sigma'].set(value=5, min=3)
pars1['g1_amplitude'].set(value=300, min=200)

gauss2 = GaussianModel(prefix='g2_')
pars1.update(gauss2.make_params())
pars1['g2_center'].set(value=421, min=380, max=700)
pars1['g2_sigma'].set(value=10, min=1)
pars1['g2_amplitude'].set(value=400, min=180)

gauss3 = GaussianModel(prefix='g3_')
pars1.update(gauss3.make_params())
pars1['g3_center'].set(value=463, min=380, max=700)
pars1['g3_sigma'].set(value=20, min=1)
pars1['g3_amplitude'].set(value=100, min=50)

mod1 = gauss1 + gauss2 + gauss3

init1 = mod1.eval(pars1, x=PPO_bis_MSB [:,0])
out1 = mod1.fit(PPO_bis_MSB [:,1], pars1, x=PPO_bis_MSB [:,0])
comps1 = out1.eval_components(x=PPO_bis_MSB [:,0])
print(out1.fit_report(min_correl=0.5))

gauss5 = GaussianModel(prefix='g5_')
pars2 = gauss5.guess(PPO[:,1], x=PPO[:,0])
pars2['g5_center'].set(value=340, min=300, max=500)
pars2['g5_sigma'].set(value=5, min=3)
#pars2['g5_gamma'].set(value=5, min=1)
pars2['g5_amplitude'].set(value=200, min=100)

#gauss6 = SkewedGaussianModel(prefix='g6_')
gauss6 = GaussianModel(prefix='g6_')
pars2.update(gauss6.make_params())
pars2['g6_center'].set(value=350, min=300, max=500)
pars2['g6_sigma'].set(value=10, min=1)
#pars2['g6_gamma'].set(value=10, min=1)
pars2['g6_amplitude'].set(value=200, min=100)

gauss7 = SkewedGaussianModel(prefix='g7_')
pars2.update(gauss7.make_params())
pars2['g7_center'].set(value=370, min=300, max=500)
pars2['g7_sigma'].set(value=5, min=3)
pars2['g7_gamma'].set(value=5, min=1)
pars2['g7_amplitude'].set(value=200, min=100)

mod2 = gauss5 + gauss6 + gauss7

init2 = mod2.eval(pars2, x=PPO [:,0])
out2 = mod2.fit(PPO[:,1], pars2, x=PPO[:,0])
comps2 = out2.eval_components(x=PPO[:,0])
print(out2.fit_report(min_correl=0.5))

#plt.plot(two_E_ethanol[:,0],two_E_ethanol[:,1]/two_E_ethanol[:,1].sum(),label='2E-ethanol')
plt.plot(PPO[:,0],PPO[:,1]/PPO[:,1].sum(),label='Masured data(PPO)')
plt.plot(PPO[:,0],out2.best_fit/out2.best_fit.sum(),alpha=0.7,lw=3,ls="--",color="m",label='Global fitting')
plt.plot(PPO[:,0],comps2['g5_']/out2.best_fit.sum(),alpha=0.6,lw=2,ls="-.",color="r",label='PPO + bis-MSB component 1')
plt.plot(PPO[:,0],comps2['g6_']/out2.best_fit.sum(),alpha=0.6,lw=2,ls="-.",color="g",label='PPO + bis-MSB component 2')
plt.plot(PPO[:,0],comps2['g7_']/out2.best_fit.sum(),alpha=0.6,lw=2,ls="-.",color="b",label='PPO + bis-MSB component 3')


plt.plot(PPO_bis_MSB[:,0],PPO_bis_MSB[:,1]/PPO_bis_MSB[:,1].sum(),alpha=0.8,lw=5,marker="o",color="c",ls="none",label='Masured data(PPO + bis-MSB)' )
plt.plot(PPO_bis_MSB[:,0],out1.best_fit/out1.best_fit.sum(),alpha=0.7,lw=3,ls="--",color="k",label='PPO + bis-MSB Global fitting')
plt.plot(PPO_bis_MSB[:,0],comps1['g1_']/out1.best_fit.sum(),alpha=0.6,lw=2,ls="-.",color="r",label='PPO + bis-MSB component 1')
plt.plot(PPO_bis_MSB[:,0],comps1['g2_']/out1.best_fit.sum(),alpha=0.6,lw=2,ls="-.",color="g",label='PPO + bis-MSB component 2')
plt.plot(PPO_bis_MSB[:,0],comps1['g3_']/out1.best_fit.sum(),alpha=0.6,lw=2,ls="-.",color="b",label='PPO + bis-MSB component 3')
plt.fill_between(PPO_bis_MSB[:,0], (comps1['g1_']/out1.best_fit.sum()).min(), comps1['g1_']/out1.best_fit.sum(), facecolor="r", alpha=0.1)
plt.fill_between(PPO_bis_MSB[:,0], (comps1['g2_']/out1.best_fit.sum()).min(), comps1['g2_']/out1.best_fit.sum(), facecolor="g", alpha=0.1)
plt.fill_between(PPO_bis_MSB[:,0], (comps1['g3_']/out1.best_fit.sum()).min(), comps1['g3_']/out1.best_fit.sum(), facecolor="b", alpha=0.1)
plt.xlim(300,600)
plt.legend(loc='best',fontsize=16,frameon=False)

plt.show()