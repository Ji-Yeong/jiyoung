
import os
import numpy as np
import matplotlib.pyplot as plt
from skimage import io
from skimage.data import shepp_logan_phantom
from skimage.transform import radon, rescale
from skimage.color import rgb2gray
from skimage.color import rgba2rgb
from skimage.color import rgb2hsv
from skimage.color import hsv2rgb

#filename = os.path.join('./data/CTHead1.dcm')
filename = os.path.join('PPO_BM_12MeV_1.JPG')
image = io.imread(filename)
#image = rgb2gray(image)
image_red = image[:,:,0]
image_green = image[:,:,1]
image_blue = image[:,:,2]
#image = shepp_logan_phantom()
#image = rescale(image, scale=0.4, mode='reflect', multichannel=True)
image_red = rescale(image_red, scale=0.6, mode='reflect', multichannel=False)
image_green = rescale(image_green, scale=0.6, mode='reflect', multichannel=False)
image_blue = rescale(image_blue, scale=0.6, mode='reflect', multichannel=False)

fig, axs = plt.subplots(nrows=3, ncols=2, figsize=(8, 4.5))
ax1 = axs[0, 0]
ax2 = axs[0, 1]
ax3 = axs[1, 0]
ax4 = axs[1, 1]
ax5 = axs[2, 0]
ax6 = axs[2, 1]
"""
fig = plt.figure(figsize=(8, 4.5))
ax1 = fig.add_subplot(3, 2, 1)
ax2 = fig.add_subplot(3, 2, 2)
ax3 = fig.add_subplot(3, 2, 3)
ax4 = fig.add_subplot(3, 2, 4)
ax5 = fig.add_subplot(3, 2, 5)
ax6 = fig.add_subplot(3, 2, 6)
"""
ax1.set_title("Original")
ax1.imshow(image_red, cmap=plt.cm.Reds_r)

theta = np.linspace(0., 180., max(image_red.shape), endpoint=False)
sinogram_red = radon(image_red, theta=theta, circle=True)
ax2.set_title("Radon transform\n(Sinogram)")
ax2.set_xlabel("Projection angle (deg)")
ax2.set_ylabel("Projection position (pixels)")
ax2.imshow(sinogram_red, cmap=plt.cm.Reds_r,
           extent=(0, 180, 0, sinogram_red.shape[0]), aspect='auto')

ax3.set_title("Original")
ax3.imshow(image_green, cmap=plt.cm.Greens_r)

theta = np.linspace(0., 180., max(image_green.shape), endpoint=False)
sinogram_green = radon(image_green, theta=theta, circle=True)
ax4.set_title("Radon transform\n(Sinogram)")
ax4.set_xlabel("Projection angle (deg)")
ax4.set_ylabel("Projection position (pixels)")
ax4.imshow(sinogram_green, cmap=plt.cm.Greens_r,
           extent=(0, 180, 0, sinogram_green.shape[0]), aspect='auto')

ax5.set_title("Original")
ax5.imshow(image_blue, cmap=plt.cm.Blues_r)

theta = np.linspace(0., 180., max(image_blue.shape), endpoint=False)
sinogram_blue = radon(image_blue, theta=theta, circle=True)
ax6.set_title("Radon transform\n(Sinogram)")
ax6.set_xlabel("Projection angle (deg)")
ax6.set_ylabel("Projection position (pixels)")
ax6.imshow(sinogram_blue, cmap=plt.cm.Blues_r,
           extent=(0, 180, 0, sinogram_blue.shape[0]), aspect='auto')
fig.tight_layout()
plt.show()