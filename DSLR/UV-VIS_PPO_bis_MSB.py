import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm  

file1 = "2Ee-only-Emission.txt"
file2 = "2Ee70-H2O30-PPO0.63g-Emission.txt"
file3 = "2Ee90-H2O10-PPO1.05g+bM0.0042g-Emission.txt"

two_E_ethanol = np.genfromtxt(file1,dtype=np.float64)
PPO = np.genfromtxt(file2,dtype=np.float64)
PPO_bis_MSB = np.genfromtxt(file3,dtype=np.float64)

from lmfit.models import SkewedGaussianModel,GaussianModel

gauss1 = GaussianModel(prefix='g1_')
pars1 = gauss1.guess(PPO_bis_MSB[:,1], x=PPO_bis_MSB[:,0])
pars1['g1_center'].set(value=405, min=380, max=700)
pars1['g1_sigma'].set(value=5, min=3)
pars1['g1_amplitude'].set(value=300, min=200)

gauss2 = GaussianModel(prefix='g2_')
pars1.update(gauss2.make_params())
pars1['g2_center'].set(value=421, min=380, max=700)
pars1['g2_sigma'].set(value=10, min=1)
pars1['g2_amplitude'].set(value=400, min=180)

gauss3 = SkewedGaussianModel(prefix='g3_')
pars1.update(gauss3.make_params())
pars1['g3_center'].set(value=463, min=380, max=700)
pars1['g3_sigma'].set(value=10, min=1)
pars1['g3_gamma'].set(value=10, min=1)
pars1['g3_amplitude'].set(value=200, min=50)

gauss4 = GaussianModel(prefix='g4_')
pars1.update(gauss4.make_params())
pars1['g4_center'].set(value=493, min=380, max=700)
pars1['g4_sigma'].set(value=20, min=1)
#pars1['g4_gamma'].set(value=20, min=1)
pars1['g4_amplitude'].set(value=20, min=10)

mod1 = gauss1 + gauss2 + gauss3 + gauss4 

init1 = mod1.eval(pars1, x=PPO_bis_MSB [:,0])
out1 = mod1.fit(PPO_bis_MSB [:,1], pars1, x=PPO_bis_MSB [:,0])
comps1 = out1.eval_components(x=PPO_bis_MSB [:,0])
print(out1.fit_report(min_correl=0.5))

plt.plot(PPO_bis_MSB[:,0],PPO_bis_MSB[:,1]/PPO_bis_MSB[:,1].sum(),alpha=0.8,lw=5,marker="o",color="c",ls="none",label='Masured data(PPO + bis-MSB)' )
plt.plot(PPO_bis_MSB[:,0],out1.best_fit/out1.best_fit.sum(),alpha=0.7,lw=3,ls="--",color="k",label='PPO + bis-MSB Global fitting')
plt.plot(PPO_bis_MSB[:,0],comps1['g1_']/out1.best_fit.sum(),alpha=0.6,lw=2,ls="-.",color="r",label='PPO + bis-MSB component 1')
plt.plot(PPO_bis_MSB[:,0],comps1['g2_']/out1.best_fit.sum(),alpha=0.6,lw=2,ls="-.",color="g",label='PPO + bis-MSB component 2')
plt.plot(PPO_bis_MSB[:,0],comps1['g3_']/out1.best_fit.sum(),alpha=0.6,lw=2,ls="-.",color="b",label='PPO + bis-MSB component 3')
plt.plot(PPO_bis_MSB[:,0],comps1['g4_']/out1.best_fit.sum(),alpha=0.6,lw=2,ls="-.",color="y",label='PPO + bis-MSB component 4')
plt.fill_between(PPO_bis_MSB[:,0], (comps1['g1_']/out1.best_fit.sum()).min(), comps1['g1_']/out1.best_fit.sum(), facecolor="r", alpha=0.1)
plt.fill_between(PPO_bis_MSB[:,0], (comps1['g2_']/out1.best_fit.sum()).min(), comps1['g2_']/out1.best_fit.sum(), facecolor="g", alpha=0.1)
plt.fill_between(PPO_bis_MSB[:,0], (comps1['g3_']/out1.best_fit.sum()).min(), comps1['g3_']/out1.best_fit.sum(), facecolor="b", alpha=0.1)
plt.fill_between(PPO_bis_MSB[:,0], (comps1['g4_']/out1.best_fit.sum()).min(), comps1['g4_']/out1.best_fit.sum(), facecolor="y", alpha=0.1)
plt.xlim(350,600)
plt.legend(loc='best',fontsize=16,frameon=False)

plt.show()