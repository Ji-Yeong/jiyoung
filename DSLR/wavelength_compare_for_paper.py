import os
import numpy as np
import matplotlib.pyplot as plt
from skimage import io
from skimage.color import rgb2gray
from skimage.color import rgba2rgb
from skimage.color import rgb2hsv
from matplotlib.pyplot import cm
from collections import OrderedDict

import matplotlib as mpl
import matplotlib.font_manager
matplotlib.font_manager.findSystemFonts(fontpaths=None, fontext='ttf')
#mpl.use("pgf")
pgf_with_pdflatex = {
    "pgf.texsystem": "pdflatex",
    "pgf.preamble": [
         r"\usepackage[utf8x]{inputenc}",
         r"\usepackage[T1]{fontenc}",
         r"\usepackage{cmbright}",
         ]
}
mpl.rcParams.update(pgf_with_pdflatex)
mpl.rcParams['font.family'] = 'serif'

i="PPO_BM_9MeV_1.JPG"
image1 = io.imread('{}'.format(i))
image12 = rgb2hsv(image1)

j="PPO_9MeV_1.JPG"
image2 = io.imread('{}'.format(j))
image22 = rgb2hsv(image2)



red11 = image12[:,:,0]*360.0
hue1=np.copy(red11)
red1 = np.copy(red11)
red1[red11>270.0]=np.nan
wave1 = 700.0-320.0/270.0*(red1.ravel())

red21 = image22[:,:,0]*360.0
hue2 = np.copy(red21)
red2 = np.copy(red21)
red2[red21>270.0]=np.nan
wave2 = 700.0-320.0/270.0*(red2.ravel())

grid = plt.GridSpec(2,2, wspace=0.2, hspace=0.2)
plt.subplot(grid[0,0])
plt.hist(wave1, alpha=0.7,bins=321,histtype="step",density=True,color="k",label=" PPO + bis-MSB sample wavelength")
#plt.xlabel(r"Degree [$^\circ$]",fontsize=16,horizontalalignment='right',x=1.0)
plt.xlabel(r"wavelength [nm]",fontsize=16,horizontalalignment='right',x=1.0)
plt.ylabel(ylabel='# of events [Normalized A.U.]',fontsize=16)
plt.legend(loc='best',fontsize=16,frameon=False)
plt.xticks(np.arange(380, 700, step=20))

plt.subplot(grid[0,1])
plt.hist(wave2, alpha=0.7,bins=321,histtype="step",density=True,color="k",label=" PPO sample wavelength")
#plt.xlabel(r"Degree [$^\circ$]",fontsize=16,horizontalalignment='right',x=1.0)
plt.xlabel(r"wavelength [nm]",fontsize=16,horizontalalignment='right',x=1.0)
plt.ylabel(ylabel='# of events [Normalized A.U.]',fontsize=16)
plt.legend(loc='best',fontsize=16,frameon=False)
plt.xticks(np.arange(380, 700, step=20))

plt.subplot(grid[1,0])
plt.hist(hue1.ravel(),alpha=0.7,bins=361,histtype="step",color="k",density=True,label="Hue(PPO + bis-MSB sample)")
plt.axvspan(270, 360, alpha=0.5,color='red',label="forbiden range (270~360)")
plt.xlabel(r"Degree [$^\circ$]",fontsize=16,horizontalalignment='right',x=1.0)
#plt.xlabel(r"wavelength [nm]",fontsize=16,horizontalalignment='right',x=1.0)
plt.ylabel(ylabel='# of events [Normalized A.U.]',fontsize=16)
plt.legend(loc='best',fontsize=16,frameon=False)


plt.subplot(grid[1,1])
plt.hist(hue2.ravel(), alpha=0.7,bins=361,histtype="step",color="k",density=True,label="Hue(PPO sample)")
plt.axvspan(270, 360, alpha=0.5,color='red',label="forbiden range (270~360)")
plt.xlabel(r"Degree [$^\circ$]",fontsize=16,horizontalalignment='right',x=1.0)
#plt.xlabel(r"wavelength [nm]",fontsize=16,horizontalalignment='right',x=1.0)
plt.ylabel(ylabel='# of events [Normalized A.U.]',fontsize=16)
plt.legend(loc='best',fontsize=16,frameon=False)
plt.show()

