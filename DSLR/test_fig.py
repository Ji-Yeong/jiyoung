import numpy as np
import matplotlib.pyplot as plt
from skimage import io

i="test_image2"
image1 = io.imread('{}.jpg'.format(i))

"""
red = image1.copy()
red[:,:,1]=0
red[:,:,2]=0

green = image1.copy()
green[:,:,0]=0
green[:,:,2]=0

blue = image1.copy()
blue[:,:,0]=0
blue[:,:,1]=0
"""
plt.subplot(2,2,1)
io.imshow(image1)

plt.subplot(2,2,2)
io.imshow(image1[:,:,0])

plt.subplot(2,2,3)
io.imshow(image1[:,:,1])

plt.subplot(2,2,4)
io.imshow(image1[:,:,2])

plt.show()