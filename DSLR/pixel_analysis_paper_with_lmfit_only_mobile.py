import os
import numpy as np
import matplotlib.pyplot as plt
from skimage import io
from skimage.color import rgb2gray
from skimage.color import rgba2rgb
from skimage.color import rgb2hsv
import matplotlib as mpl
import matplotlib.font_manager
matplotlib.font_manager.findSystemFonts(fontpaths=None, fontext='ttf')
#mpl.use("pgf")
pgf_with_pdflatex = {
    "pgf.texsystem": "pdflatex",
    "pgf.preamble": [
         r"\usepackage[utf8x]{inputenc}",
         r"\usepackage[T1]{fontenc}",
         r"\usepackage{cmbright}",
         ]
}
mpl.rcParams.update(pgf_with_pdflatex)
mpl.rcParams['font.family'] = 'serif'
#mpl.rc('hatch', color='k', linewidth=3)

image1 = io.imread('{}.JPG'.format("PPO_BM_12MeV_1"))
image2 = rgb2hsv(image1)
blue1 = image1[:,:,2]/np.max(image1[:,:,2])*255
hsv_v1 = image2[:,:,2]/np.max(image2[:,:,2])*100
blue11 = blue1.ravel()
hsv_new_v1 = np.copy(hsv_v1)

x = np.copy(blue1.flatten())
#x = hsv_new_v1.ravel()
print(x.shape)
bin_heights, bin_borders, _= plt.hist(blue1.ravel(),bins=256,histtype="step")
bin_widths = np.diff(bin_borders)
bin_centers = bin_borders[:-1] + bin_widths / 2
#x_array = bin_centers
#y_array = bin_heights
x = bin_centers
y = bin_heights

image5 = io.imread('12MeV.PNG')
image6 = rgba2rgb(image5)
image6 = rgb2hsv(image6)

blue5 = image5[:,:,2]/np.max(image5[:,:,2])*255
hsv_v5 = image6[:,:,2]/np.max(image6[:,:,2])*100
blue5 = blue5.ravel()
hsv_new_v5 = np.copy(hsv_v5)

x5 = np.copy(blue5.flatten())
#x = hsv_new_v1.ravel()
print(x.shape)
bin_heights5, bin_borders5, _= plt.hist(blue5.ravel(),bins=256,histtype="step")
bin_widths5 = np.diff(bin_borders5)
bin_centers5 = bin_borders5[:-1] + bin_widths5 / 2
#x_array = bin_centers
#y_array = bin_heights
x5 = bin_centers5
y5 = bin_heights5
plt.clf()
"""
from lmfit import Minimizer, Parameters, report_fit
from lmfit.lineshapes import gaussian, lorentzian

def residual(pars, x, data):
    model = (gaussian(x, pars['amp_g'], pars['cen_g'], pars['wid_g']) +
             lorentzian(x, pars['amp_l'], pars['cen_l'], pars['wid_l']))
    return model - data

pfit = Parameters()
pfit.add(name='amp_g', value=1e5)
pfit.add(name='amp_l', value=1e4)
pfit.add(name='cen_g', value=0)
pfit.add(name='peak_split', value=102.5, min=100, max=160, vary=True)
pfit.add(name='cen_l', value=160)
pfit.add(name='wid_g', value=10)
pfit.add(name='wid_l', expr='wid_g')

mini = Minimizer(residual, pfit, fcn_args=(x, data))
out = mini.leastsq()
best_fit = data + out.residual

report_fit(out.params)

plt.plot(x, data, 'bo')
plt.plot(x, best_fit, 'r--', label='best fit')
plt.legend(loc='best')

plt.show()
"""

from lmfit.models import ExponentialModel,GaussianModel

exp_mod = ExponentialModel(prefix='exp_')
pars = exp_mod.guess(y, x=x)

"""
gauss1 = GaussianModel(prefix='g1_')
pars = gauss1.guess(y, x=x)

pars['g1_center'].set(value=1, min=0, max=3)
pars['g1_sigma'].set(value=10, min=3)
pars['g1_amplitude'].set(value=6e5, min=1e4)
"""
gauss2 = GaussianModel(prefix='g2_')
pars.update(gauss2.make_params())

pars['g2_center'].set(value=165, min=163, max=167)
pars['g2_sigma'].set(value=10, min=1)
pars['g2_amplitude'].set(value=1e4, min=1e3)

gauss3 = GaussianModel(prefix='g3_')
pars.update(gauss3.make_params())

pars['g3_center'].set(value=25, min=20, max=50)
pars['g3_sigma'].set(value=4, min=1)
pars['g3_amplitude'].set(value=1e5, min=1e3)

gauss4 = GaussianModel(prefix='g4_')
pars.update(gauss4.make_params())

pars['g4_center'].set(value=130, min=120, max=150)
pars['g4_sigma'].set(value=2, min=1)
pars['g4_amplitude'].set(value=1e3, min=1e2)

#mod = gauss1 + gauss2 + gauss3 + gauss4 
mod1 = exp_mod  + gauss2 + gauss3 + gauss4 

init = mod1.eval(pars, x=x)
out = mod1.fit(y, pars, x=x)


gauss5 = GaussianModel(prefix='g5_')
pars5 = gauss5.guess(y, x=x5)

pars5['g5_center'].set(value=12, min=0, max=15)
pars5['g5_sigma'].set(value=3, min=2)
pars5['g5_amplitude'].set(value=8e3, min=1e2)

gauss6 = GaussianModel(prefix='g6_')
pars5.update(gauss6.make_params())

pars5['g6_center'].set(value=81, min=60, max=100)
pars5['g6_sigma'].set(value=2, min=1)
pars5['g6_amplitude'].set(value=2e3, min=1e2)

gauss7 = GaussianModel(prefix='g7_')
pars5.update(gauss7.make_params())

pars5['g7_center'].set(value=31, min=20, max=50)
pars5['g7_sigma'].set(value=4, min=1)
pars5['g7_amplitude'].set(value=1.5e3, min=1e2)

mod5 = gauss5 + gauss6 + gauss7 

init5 = mod5.eval(pars5, x=x5)
out5 = mod5.fit(y5, pars5, x=x5)

print(out5.fit_report(min_correl=0.5))
#plt.gca().spines['bottom'].set_position(('data',0))
#fig, axes = plt.subplots(1, 2, figsize=(12.8, 4.8))
#axes[0].plot(x, init, 'k--', label='initial fit')
#axes[0].plot(x5, y5/y5.max()*100,ls='none', color="k",marker="o",markersize=10,fillstyle='none',alpha=0.6,label='Measured data (mobile)')
#axes[0].plot(x, y/y.max()*100,ls="none", color="b",marker="o",markersize=10,fillstyle='full',alpha=0.6,label='Measured data (DSLR)')
#axes[0].plot(x, out.best_fit/y.max()*100, 'r--',alpha=0.8,lw=3, label='Global fitting (DSLR)')
#axes[0].plot(x5, out5.best_fit/y5.max()*100, 'g:.',alpha=0.8,lw=3, label='Global fitting (mobile)')
#axes[0].set_xlabel('Blue pixel intesity number',fontsize=24,horizontalalignment='right', x=1.0)
#axes[0].set_ylabel('# of events [Normalized A.U.]',fontsize=24)
#axes[0].legend(loc='best',fontsize=24,frameon=False,bbox_to_anchor=(0.9, 0.85))
plt.figure(figsize=(12, 10))
plt.gca().spines['top'].set_linewidth(2)
plt.gca().spines['bottom'].set_linewidth(2)
plt.gca().spines['left'].set_linewidth(2)
plt.gca().spines['right'].set_linewidth(2)
comps = out.eval_components(x=x)
comps5 = out5.eval_components(x=x5)
p1,=plt.plot(x5, y5/y5.max()*100,ls='none', color="b",marker="o",markersize=10,fillstyle='none',alpha=0.5,label='Measured data')
#d1,=axes[1].plot(x, y/y.max()*100,ls="none", color="b",marker="o",markersize=10,fillstyle='full',alpha=0.5,label='Measured data in DSLR')
#axes[1].plot(x, comps['g1_'], 'g--', label='Gaussian component 1')
#d2,=axes[1].plot(x, comps['g2_']/y.max()*100, 'r-',alpha=1.0,lw=2, label='PPO + bis-MSB in DSLR')
#axes[1].fill_between(x, (comps['g2_']/y.max()*100).min(), comps['g2_']/y.max()*100, facecolor="r", alpha=0.1)
#axes[1].plot(x, comps['g3_']/y.max()*100, 'k--',alpha=0.6,lw=2, label='DSLR Gaussian component 2')
#axes[1].plot(x, comps['g4_']/y.max()*100, 'y--',alpha=0.6,lw=2, label='DSLR Gaussian component 3')
#axes[1].plot(x, comps['exp_']/y.max()*100, 'g--',alpha=0.6,lw=2, label='DSLR Exponential component')
#d3,=axes[1].plot(x, out.best_fit/y.max()*100, 'r--',alpha=0.7,lw=3, label='Global fitting at data in DSLR')
#axes[1].plot(x5, comps5['g5_']/y5.max()*100, 'm:',alpha=0.6,lw=2, label='Smartphone Gaussian component 1')
p2,=plt.plot(x5, comps5['g6_']/y5.max()*100, 'r-.',alpha=1.0,lw=3, label='PPO + bis-MSB')
plt.fill_between(x5, (comps5['g6_']/y5.max()*100).min(), comps5['g6_']/y5.max()*100, facecolor="r", alpha=0.2)
#axes[1].plot(x5, comps5['g7_']/y5.max()*100, 'y:',alpha=0.6,lw=2, label='Smartphone Gaussian component 3')
p3,=plt.plot(x5, out5.best_fit/y5.max()*100, 'g:',alpha=1.0,lw=5, label='Global fitting')
plt.xlabel('Blue pixel intensity number',fontsize=30,horizontalalignment='right', x=1.0)
plt.ylabel('# of events [A.U.]',fontsize=30)
plt.legend(loc='best',fontsize=30,frameon=False,bbox_to_anchor=(0.9, 0.93))
#axes[1].legend(loc='best',fontsize=24,frameon=False,bbox_to_anchor=(0.9, 0.85))
#leg1=plt.legend([p1,p2,p3],['Measured data','PPO + bis-MSB','Global fitting'],\
#    loc='best',fontsize=26,frameon=False,bbox_to_anchor=(0.9, 0.93))
#axes[1].legend([d1,d2,d3],['Measured data (DSLR)','PPO + bis-MSB (DSLR)','Global fitting (DSLR)'],\
#    loc='best',fontsize=24,frameon=False,bbox_to_anchor=(0.9, 0.80))
#plt.add_artist(leg1)
plt.xticks(fontsize=30)
plt.yticks(fontsize=30)
#plt.show()
plt.savefig('fig7_c.pdf',dpi=300,transparent=True)
plt.savefig('fig7_c.jpg', dpi=300, quality=95, optimize=True, progressive=True)  