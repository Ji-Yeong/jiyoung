import os
from re import T
import numpy as np
import matplotlib as mpl
#mpl.use('Agg')
import matplotlib.pyplot as plt
from skimage import io
from skimage.color import rgb2gray
from skimage.color import rgba2rgb
from skimage.color import rgb2hsv
from skimage.color import hsv2rgb
from matplotlib.pyplot import cm
from collections import OrderedDict

import mplhep as hep
hep.set_style(hep.style.ROOT) # For now ROOT defaults to CMS

cut = 0.022
hue_cut=270
i="405nm_blue_beam_40mA_nozoom_DSLR_1s_1.JPG"
j="405nm_blue_beam_40mA_nozoom_DSLR_1s_2.JPG"
k="405nm_blue_beam_40mA_zoomx8_DSLR_1s_1.JPG"
l="440nm_blue_beam_40mA_nozoom_1s_1.JPG"
m="440nm_blue_beam_40mA_nozoom_1s_2.JPG"
r="473nm_blue_beam_40mA_nozoom_1s_1.JPG"
image1 = io.imread('{}'.format(l))
#image1 = rgba2rgb(image1) # PNG 파일인 경우
image12 = rgb2hsv(image1) # return HSV 
V_hsv = np.copy(image12)
V_hsv[V_hsv[:,:,2]<cut]=np.nan # HSV with V_cut

red11 = image12[:,:,0]*360.0 #HUE 범위 0~1 을 0~360 늘림
red1 = np.copy(red11)
red1[red11>hue_cut]=np.nan

V_hsv11 = V_hsv[:,:,0]*360.0
V_hsv1 = np.copy(V_hsv11)
V_hsv1[V_hsv11>hue_cut]=np.nan

blue1 = image12[:,:,2] 
blue11= np.copy(blue1)
blue11[blue1<cut]=np.nan #V cut 이상만 선택 후 시각화

"""
bin_heights, bin_borders, _= plt.hist(blue1.ravel(),bins=256,range=(0.0, 255.0),histtype="step")
bin_widths = np.diff(bin_borders)
bin_centers = bin_borders[:-1] + bin_widths / 2
x = bin_centers
y = bin_heights
plt.clf()
"""
grid = plt.GridSpec(2,2, wspace=0.1, hspace=0.2)

plt.subplot(grid[0,0])
plt.hist(red1.ravel(), alpha=0.7,bins=271,histtype="step",density=True,color="k",label=r'Normal_HUE')
plt.hist(V_hsv1.ravel(), alpha=0.7,bins=271,histtype="step",density=True,color="b",label=r'Normal_HUE' +'\n'+ r'(signal region)')
#plt.xlabel(r"Degree [$^\circ$]",fontsize=16,horizontalalignment='right',x=1.0)
plt.xlabel(r"HUE [Degree]",fontsize=20,horizontalalignment='right',x=1.0)
plt.ylabel(ylabel='# of events [A.U.]',fontsize=20)
plt.legend(loc='best',fontsize=20,frameon=False)
#plt.xticks(np.arange(380, 700, step=20),fontsize=20)
plt.yticks(fontsize=20)


plt.subplot(grid[0,1])
plt.hist(image12[:,:,0].ravel(),bins=301,range=(0.0, 1.0),alpha=0.8,density=True,histtype="step",color="k",ls=":",lw=2,label="Hue (0~1.0)")
plt.hist(image12[:,:,1].ravel(),bins=301,range=(0.0, 1.0),alpha=0.8,density=True,histtype="step",color="m",ls="--",lw=2,label="Saturation (0~1.0)")
plt.hist(image12[:,:,2].ravel(),bins=301,range=(0.0, 1.0),alpha=0.8,density=True,histtype="step",color="c",ls="-",lw=2,label="Value (0~1.0)")
#plt.yscale('log')
plt.xlabel('Coefficient number in HSV',fontsize=20,horizontalalignment='right',x=1.0)
plt.ylabel('# of events [A.U.]',fontsize=20)
plt.yticks(fontsize=20)
plt.xticks(fontsize=20)
#plt.legend(loc='best',fontsize=16,frameon=False)
plt.legend(loc='best',fontsize=20,bbox_to_anchor=(0.5, 0.6),frameon=False)

plt.subplot(grid[1,0])
plt.hist(red1.ravel(), alpha=0.7,bins=271,histtype="step",density=False,color="k",label="HUE")
plt.hist(V_hsv1.ravel(), alpha=0.7,bins=271,histtype="step",density=False,color="r",label="HUE(signal region)")
#plt.xlabel(r"Degree [$^\circ$]",fontsize=16,horizontalalignment='right',x=1.0)
plt.xlabel(r"HUE [Degree]",fontsize=20,horizontalalignment='right',x=1.0)
plt.ylabel(ylabel='# of events [A.U.]',fontsize=20)
plt.legend(loc='best',fontsize=20,frameon=False)
#plt.xticks(np.arange(380, 700, step=20),fontsize=20)
plt.yticks(fontsize=20)
#$,[ "{0:.0f}".format(x*210/2480) for x in np.arange(0, 1480, step=100)])

plt.subplot(grid[1,1])
#plt.imshow(blue11,alpha=0.5,vmin=0.1,vmax=0.80,cmap=plt.cm.jet)
plt.imshow(blue11,alpha=0.3,cmap=plt.cm.jet)
#plt.axis("off")
plt.show()

#plt.savefig("{}_processing.pdf".format(name),transparent=True,bbox_inches="tight",pad_inches=0.1)
#plt.savefig('Figure-4.pdf',dpi=300,transparent=True)
#plt.savefig('Figure-4.jpg', dpi=300, quality=95, optimize=True, progressive=True)  
