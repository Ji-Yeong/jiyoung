import numpy as np
from matplotlib import pyplot as plt
from iminuit import Minuit
from iminuit.cost import LeastSquares
""" declear array elements number """
n = 65
""" DATA I/O """
file1 = "hue_digitize_2.txt"
hue_wavelength_data = np.genfromtxt(file1,dtype=np.float64,comments='#')

a_0_pram=1.0
a_1_pram=1.0
a_2_pram=1.0
a_3_pram=1.0
a_4_pram=1.0
a_5_pram=1.0
a_6_pram=1.0

x1 = hue_wavelength_data[:,1]
y1 = hue_wavelength_data[:,0]

total=np.zeros([n])
total2=np.zeros([n])
total3=np.zeros([n])
total4=np.zeros([n])
total5=np.zeros([n])


def hue_wavelength_fit_7(x1,a_0,a_1,a_2,a_3,a_4,a_5):
     for i in range (0,n) :
          total5[i] = -a_1*np.exp(a_2*x1[i])/(a_1*np.exp(a_2*x1[i])+1)+a_0-y1[i]
     return total5

least_squares5 = LeastSquares(x1,y1,yerror=0.1,model=hue_wavelength_fit_7)
m5 = Minuit(least_squares5,grad=None\
     ,a_0=a_0_pram ,a_1=a_1_pram,a_2=a_2_pram,a_3=a_3_pram,a_4=a_4_pram,a_5=a_5_pram\
    #,error_a_0=0.01,error_a_1=0.01,error_a_2=0.01,error_a_3=0.01\
        #,limit_a_0=(-1e+1,1e+1),limit_a_1=(-1e+1,1e+1),limit_a_2=(-1e+1,1e+1),limit_a_3=(-1e+1,1e+1)\
             )

m5.migrad(ncall=1e+3,resume=True,nsplit=4,precision=1e-1)
#print(m.migrad(ncall=1e+3,resume=True,nsplit=4,precision=1e-1))
m5.hesse(ncall=1e+3)
#m4.hesse()   # run covariance estimator
#print(m.errors)  # x: 1, y: 1, z: 1
par5 =dict(m5.values)
print(m5.values)

def hue_wavelength_fit_6(x1,a_0,a_1,a_2,a_3,a_4,a_5,a_6):
     for i in range (0,n) :
          total4[i] = a_6*np.power(x1[i],6)+a_5*np.power(x1[i],5)+a_4*np.power(x1[i],4)+a_3*np.power(x1[i],3)+a_2*np.power(x1[i],2)+a_1*x1[i]+a_0-y1[i]
     return total4

least_squares4 = LeastSquares(x1,y1,yerror=0.1,model=hue_wavelength_fit_6)
m4 = Minuit(least_squares4,grad=None\
     ,a_0=a_0_pram ,a_1=a_1_pram,a_2=a_2_pram,a_3=a_3_pram,a_4=a_4_pram,a_5=a_5_pram,a_6=a_6_pram\
    #,error_a_0=0.01,error_a_1=0.01,error_a_2=0.01,error_a_3=0.01\
        #,limit_a_0=(-1e+1,1e+1),limit_a_1=(-1e+1,1e+1),limit_a_2=(-1e+1,1e+1),limit_a_3=(-1e+1,1e+1)\
             )

m4.migrad(ncall=1e+3,resume=True,nsplit=4,precision=1e-1)
#print(m.migrad(ncall=1e+3,resume=True,nsplit=4,precision=1e-1))
m4.hesse(ncall=1e+3)
#m4.hesse()   # run covariance estimator
#print(m.errors)  # x: 1, y: 1, z: 1
par4 =dict(m4.values)
print(m4.values)



def hue_wavelength_fit_5(x1,a_0,a_1,a_2,a_3,a_4,a_5):
     for i in range (0,n) :
          total3[i] = a_5*np.power(x1[i],5)+a_4*np.power(x1[i],4)+a_3*np.power(x1[i],3)+a_2*np.power(x1[i],2)+a_1*x1[i]+a_0-y1[i]
     return total3

least_squares3 = LeastSquares(x1,y1,yerror=0.1,model=hue_wavelength_fit_5)
m3 = Minuit(least_squares3,grad=None\
     ,a_0=a_0_pram ,a_1=a_1_pram,a_2=a_2_pram,a_3=a_3_pram,a_4=a_4_pram,a_5=a_5_pram\
    #,error_a_0=0.01,error_a_1=0.01,error_a_2=0.01,error_a_3=0.01\
        #,limit_a_0=(-1e+1,1e+1),limit_a_1=(-1e+1,1e+1),limit_a_2=(-1e+1,1e+1),limit_a_3=(-1e+1,1e+1)\
             )

m3.migrad(ncall=1e+3,resume=True,nsplit=4,precision=1e-1)
#print(m.migrad(ncall=1e+3,resume=True,nsplit=4,precision=1e-1))
m3.hesse(ncall=1e+3)

#print(m.errors)  # x: 1, y: 1, z: 1
par3 =dict(m3.values)
print(m3.values)


def hue_wavelength_fit_4(x1,a_0,a_1,a_2,a_3,a_4):
     for i in range (0,n) :
          total[i] = a_4*np.power(x1[i],4)+a_3*np.power(x1[i],3)+a_2*np.power(x1[i],2)+a_1*x1[i]+a_0-y1[i]
     return total

least_squares1 = LeastSquares(x1,y1,yerror=0.1,model=hue_wavelength_fit_4)
m = Minuit(least_squares1,grad=None\
     ,a_0=a_0_pram ,a_1=a_1_pram,a_2=a_2_pram,a_3=a_3_pram,a_4=a_4_pram\
    #,error_a_0=0.01,error_a_1=0.01,error_a_2=0.01,error_a_3=0.01\
        #,limit_a_0=(-1e+1,1e+1),limit_a_1=(-1e+1,1e+1),limit_a_2=(-1e+1,1e+1),limit_a_3=(-1e+1,1e+1)\
             )

m.migrad(ncall=1e+3,resume=True,nsplit=4,precision=1e-1)
#print(m.migrad(ncall=1e+3,resume=True,nsplit=4,precision=1e-1))
m.hesse(ncall=1e+3)
m.hesse()   # run covariance estimator
#print(m.errors)  # x: 1, y: 1, z: 1
par =dict(m.values)
print(m.values)

def hue_wavelength_fit_3(x1,a_0,a_1,a_2,a_3):
     for i in range (0,n) :
          total2[i] = a_3*np.power(x1[i],3)+a_2*np.power(x1[i],2)+a_1*x1[i]+a_0-y1[i]
     return total2

least_squares2 = LeastSquares(x1,y1,yerror=0.1,model=hue_wavelength_fit_3)
m2 = Minuit(least_squares2,grad=None,errordef=1\
     ,a_0=a_0_pram ,a_1=a_1_pram,a_2=a_2_pram,a_3=a_3_pram\
    #,error_a_0=0.01,error_a_1=0.01,error_a_2=0.01,error_a_3=0.01\
        #,limit_a_0=(-1e+1,1e+1),limit_a_1=(-1e+1,1e+1),limit_a_2=(-1e+1,1e+1),limit_a_3=(-1e+1,1e+1)\
             ,fix_a_0=False,fix_a_1=False,fix_a_2=False,fix_a_3=False)

m2.migrad(ncall=1e+3,resume=True,nsplit=4,precision=1e-1)
#print(m2.migrad(ncall=1e+3,resume=True,nsplit=4,precision=1e-1))
m2.hesse(ncall=1e+3)
#print(repr(m.params))
print(m2.values)
par2 =dict(m2.values)
#print("%f " %par.get('n_intinsic_pram'))
#print(m.errors)
#print(m2.fval / (len(y1) - m2.nfit),"reduced chi2 J_diffusion" )


plt.plot(x1, y1, 'bo',alpha=0.4)

plt.plot(x1,hue_wavelength_fit_7(x1,par5.get('a_0'),par5.get('a_1'),par5.get('a_2'),par5.get('a_3')\
     ,par5.get('a_4'),par5.get('a_5')),'-', label=r'$\chi^{2}$ sigmoid type ',alpha=0.4)
plt.plot(x1,hue_wavelength_fit_6(x1,par4.get('a_0'),par4.get('a_1'),par4.get('a_2'),par4.get('a_3')\
     ,par4.get('a_4'),par4.get('a_5'),par4.get('a_6')),'-', label=r'$\chi^{2}$ polynomial $6^{th}$ order',alpha=0.4)
plt.plot(x1,hue_wavelength_fit_5(x1,par3.get('a_0'),par3.get('a_1'),par3.get('a_2'),par3.get('a_3')\
     ,par3.get('a_4'),par3.get('a_5')),'-', label=r'$\chi^{2}$ polynomial $ 5^{th}$ order',alpha=0.4)
plt.plot(x1,hue_wavelength_fit_4(x1,par.get('a_0'),par.get('a_1'),par.get('a_2'),par.get('a_3')\
     ,par.get('a_4')),'-', label=r'$\chi^{2}$ polynomial $ 4^{th}$ order',alpha=0.4)
plt.plot(x1,hue_wavelength_fit_3(x1,par2.get('a_0'),par2.get('a_1'),par2.get('a_2')\
     ,par2.get('a_3')),'-', label=r'$\chi^{2}$ polynomial $ 3^{th}$ order',alpha=0.4)
plt.ylim(480,670)
plt.xlim(0,270)
plt.xlabel(r"HUE [Degree]",fontsize=20,horizontalalignment='right',x=1.0)
plt.ylabel(r"$\lambda$[nm]",fontsize=20)

#plt.xlabel(r"$\lambda$[nm]",fontsize=20,horizontalalignment='right',x=1.0)
#plt.ylabel(r"HUE [Degree]",fontsize=20)
#plt.xlim(480,670)
#plt.ylim(0,270)
plt.legend(fontsize=12,loc='best',)
plt.show()