import os
import numpy as np
import matplotlib.pyplot as plt
from skimage import io
from skimage.color import rgb2gray
from skimage.color import rgba2rgb
from skimage.color import rgb2hsv
from matplotlib.pyplot import cm, xticks

import matplotlib as mpl
import matplotlib.font_manager
matplotlib.font_manager.findSystemFonts(fontpaths=None, fontext='ttf')
#mpl.use("pgf")
pgf_with_pdflatex = {
    "pgf.texsystem": "pdflatex",
    "pgf.preamble": [
         r"\usepackage[utf8x]{inputenc}",
         r"\usepackage[T1]{fontenc}",
         r"\usepackage{cmbright}",
         ]
}
mpl.rcParams.update(pgf_with_pdflatex)
mpl.rcParams['font.family'] = 'serif'

i = "film_12MeV_1"

image4 = io.imread('{}.jpg'.format(i))
image2 = rgb2gray(image4)
image3 = 1-image2
#print(image2.shape)
pdd = np.mean(image3,axis=1)
pdd = pdd - pdd.min()
pdd_normal = pdd/pdd.max()*100

j = "film_9MeV_1"
image6 = io.imread('{}.jpg'.format(j))
image7 = rgb2gray(image6)
image8 = 1-image7
pdd2 = np.mean(image8,axis=1)
pdd2 = pdd2 - pdd2.min()
pdd_normal2 = pdd2/pdd2.max()*100

image10 = io.imread('scan_6MeV.jpg')
image11 = rgb2gray(image10)
image12 = 1-image11
pdd3 = np.mean(image12,axis=1)
pdd3 = pdd3 - pdd3.min()
pdd_normal3 = pdd3/pdd3.max()*100

#print(np.mean(image3,axis=1).shape)

plt.plot(np.arange(0, 1768),pdd_normal,alpha=0.3,color="r",ls="none",marker= "s",markersize=8,fillstyle="none",label="12MeV")
plt.plot(np.arange(0, 1752),pdd_normal2,alpha=0.3,color="g",ls="none",marker= "o",markersize=8,fillstyle="none",label="9MeV")
plt.plot(np.arange(0, 1458),pdd_normal3,alpha=0.3,color="b",ls="none",marker= "x",markersize=8,label="6MeV")
plt.xlabel(r'Depth [mm]',fontsize=32, horizontalalignment='right', x=1.0)
plt.ylabel('Percentage depth dose [%]',fontsize=32)
plt.xticks(np.arange(0, 1768, step=100),[ "{0:.0f}".format(x*210/2480) for x in np.arange(0, 1768, step=100)],fontsize=20)
plt.yticks(fontsize=20)
plt.legend(loc='best',fontsize=22,bbox_to_anchor=(0.82, 0.76),frameon=False,markerscale=3)
#bbox_to_anchor=(0.82, 0.76),
plt.show()