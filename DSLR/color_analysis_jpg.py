import os
import numpy as np
import matplotlib.pyplot as plt
from skimage import io
from skimage.color import rgb2gray
from skimage.color import rgba2rgb
from skimage import filters
from skimage.data import camera
from skimage.util import compare_images
from skimage.filters import gaussian
from skimage.segmentation import active_contour
from skimage.filters import try_all_threshold
from skimage import data, img_as_float
from skimage.segmentation import chan_vese


i="6MeV_v3"
#name = ["6MeV","9MeV","12MeV"]
image1 = io.imread('{}.jpg'.format(i))
red1 = image1[:,:,0]
green1 = image1[:,:,1]
blue1 = image1[:,:,2]
weights_red1 = np.ones_like(red1.ravel())/float(len(red1.ravel()))
weights_green1 = np.ones_like(green1.ravel())/float(len(green1.ravel()))
weights_blue1 = np.ones_like(blue1.ravel())/float(len(blue1.ravel()))

j="9MeV_v3"
#name = ["6MeV","9MeV","12MeV"]
image2 = io.imread('{}.jpg'.format(j))
red2 = image2[:,:,0]
green2 = image2[:,:,1]
blue2 = image2[:,:,2]
weights_red2 = np.ones_like(red2.ravel())/float(len(red2.ravel()))
weights_green2 = np.ones_like(green2.ravel())/float(len(green2.ravel()))
weights_blue2 = np.ones_like(blue2.ravel())/float(len(blue2.ravel()))

k="12MeV_v3"
#name = ["6MeV","9MeV","12MeV"]
image3 = io.imread('{}.jpg'.format(k))
red3 = image3[:,:,0]
green3 = image3[:,:,1]
blue3 = image3[:,:,2]
weights_red3 = np.ones_like(red3.ravel())/float(len(red3.ravel()))
weights_green3 = np.ones_like(green3.ravel())/float(len(green3.ravel()))
weights_blue3 = np.ones_like(blue3.ravel())/float(len(blue3.ravel()))

grid = plt.GridSpec(2,3, wspace=0.1, hspace=0.1)
plt.subplot(grid[0,0])
#plt.hist(red.ravel(),bins=2560, range=(0.0, 255.0),alpha=0.5)
plt.hist(red1.ravel(),bins=256, range=(0.0, 255.0),alpha=0.7,histtype="step",density=True,weights=weights_red1,color="r")
plt.hist(green1.ravel(),bins=256, range=(0.0, 255.0),alpha=0.7,histtype="step",density=True,weights=weights_green1,color="g")
plt.hist(blue1.ravel(),bins=256, range=(0.0, 255.0),alpha=0.7,histtype="step",density=True,weights=weights_blue1,color="b")
plt.yscale('log')
#plt.xscale('log')
plt.title("Histogram {} for color space on range (Dark=0,Bright=255)".format(i) ,fontsize=12)
plt.subplot(grid[0,1])
plt.hist(red2.ravel(),bins=256, range=(0.0, 255.0),alpha=0.7,histtype="step",density=True,weights=weights_red2,color="r")
plt.hist(green2.ravel(),bins=256, range=(0.0, 255.0),alpha=0.7,histtype="step",density=True,weights=weights_green2,color="g")
plt.hist(blue2.ravel(),bins=256, range=(0.0, 255.0),alpha=0.7,histtype="step",density=True,weights=weights_blue2,color="b")
plt.yscale('log')
#plt.xscale('log')
plt.title("Histogram {} for color space on range (Dark=0,Bright=255)".format(j), fontsize=12)
plt.subplot(grid[0,2])
plt.hist(red3.ravel(),bins=256, range=(0.0, 255.0),alpha=0.7,histtype="step",density=True,weights=weights_red3,color="r")
plt.hist(green3.ravel(),bins=256, range=(0.0, 255.0),alpha=0.7,histtype="step",density=True,weights=weights_green3,color="g")
plt.hist(blue3.ravel(),bins=256, range=(0.0, 255.0),alpha=0.7,histtype="step",density=True,weights=weights_blue3,color="b")
plt.yscale('log')
#plt.xscale('log')
plt.title("Histogram {} for color space on range (Dark=0,Bright=255)".format(k), fontsize=12)

plt.subplot(grid[1,0])
#plt.hist(blue1.ravel(),bins=2560, range=(0.0, 255.0),alpha=0.9,fc='b',histtype="step")
#plt.yscale('log')
plt.imshow(image1)
plt.title("Histogram {} for Blue channel on range (Dark=0,Bright=255)".format(i), fontsize=12)
plt.subplot(grid[1,1])
#plt.hist(blue2.ravel(),bins=2560, range=(0.0, 255.0),alpha=0.9,fc='b',histtype="step")
#plt.yscale('log')
plt.imshow(image2)
plt.title("Histogram {} for Blue channel on range (Dark=0,Bright=255)".format(j), fontsize=12)
plt.subplot(grid[1,2])
#plt.hist(blue3.ravel(),bins=2560, range=(0.0, 255.0),alpha=0.9,fc='b',histtype="step")
#plt.yscale('log')
plt.imshow(image3)
plt.title("Histogram {} for Blue channel on range (Dark=0,Bright=255)".format(k), fontsize=12)
#plt.subplots_adjust(wspace=1.0, hspace=1.5)
#fig.tight_layout(pad=0.4, w_pad=1.0, h_pad=1.5)
plt.show()
#plt.savefig("{}_processing.pdf".format(name),transparent=True,bbox_inches="tight",pad_inches=0.1)