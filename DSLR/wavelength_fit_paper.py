import os
import numpy as np
import matplotlib.pyplot as plt
from skimage import io
from skimage.color import rgb2gray
from skimage.color import rgba2rgb
from skimage.color import rgb2hsv
from matplotlib.pyplot import cm
from collections import OrderedDict

import matplotlib as mpl
import matplotlib.font_manager
matplotlib.font_manager.findSystemFonts(fontpaths=None, fontext='ttf')
#mpl.use("pgf")
pgf_with_pdflatex = {
    "pgf.texsystem": "pdflatex",
    "pgf.preamble": [
         r"\usepackage[utf8x]{inputenc}",
         r"\usepackage[T1]{fontenc}",
         r"\usepackage{cmbright}",
         ]
}
mpl.rcParams.update(pgf_with_pdflatex)
mpl.rcParams['font.family'] = 'serif'

#i="9MeV_v3.jpg"
i="PPO_BM_9MeV_1.JPG"
#i="9MeV.PNG"
image1 = io.imread('{}'.format(i))
image12 = rgb2hsv(image1)

j="PPO_9MeV_1.JPG"
#i="9MeV.PNG"
image2 = io.imread('{}'.format(j))
image22 = rgb2hsv(image2)

"""
image_b = io.imread('{}'.format(i))
image_b[:,:,0]=0
image_b[:,:,1]=0
image12 = rgb2hsv(image_b)
"""

red11 = image12[:,:,0]*360.0
hue1=np.copy(red11)
red1 = np.copy(red11)
red1[red11>270.0]=np.nan
wave1 = 700.0-320.0/270.0*(red1.ravel())

red21 = image22[:,:,0]*360.0
hue2 = np.copy(red21)
red2 = np.copy(red21)
red2[red21>270.0]=np.nan
wave2 = 700.0-320.0/270.0*(red2.ravel())

import lmfit
from collections import defaultdict
from lmfit.models import ExponentialModel,GaussianModel
bin_heights1, bin_borders1, _= plt.hist(wave1,bins=321,histtype="step")
bin_widths1 = np.diff(bin_borders1)
bin_centers1 = bin_borders1[:-1] + bin_widths1 / 2
x1 = bin_centers1
y1 = bin_heights1

bin_heights2, bin_borders2, _= plt.hist(wave2,bins=321,histtype="step")
bin_widths2 = np.diff(bin_borders2)
bin_centers2 = bin_borders2[:-1] + bin_widths2 / 2
x2 = bin_centers2
y2 = bin_heights2

mu = 410
sigma =10
amplitude = 100

params = lmfit.Parameters()
params.add('mu', 415, min=380,max=500)
params.add('sigma', 12, min=1)
params.add('amplitude', 6e6, min=1e5)
#method = 'L-BFGS-B'
""" 
params.add('mu', 415, min=380,max=500)
params.add('sigma', 12, min=1)
params.add('amplitude', 6e6, min=1e5)
method = 'L-BFGS-B'
"""
#method = 'CG'
method = 'Nelder-Mead'
def normal_dist(params,x_data,y_data ):
    mu = params['mu'].value
    sigma = params['sigma'].value
    amplitude = params['amplitude'].value
    y_model = amplitude/(sigma*(2*np.pi)**0.5)*np.exp(-(x_data-mu)**2/(2*sigma**2))
    return y_model - y_data

o1 = lmfit.minimize(normal_dist, params, args=(x1, y1), method=method)
print("# Fit using sum of squares:\n")
#lmfit.report_fit(o1)    
o2 = lmfit.minimize(normal_dist, params, args=(x1, y1), method=method,
                    #reduce_fcn='negentropy')
                    reduce_fcn='neglogcauchy')
print("\n\n# Robust Fit, using log-likelihood with Cauchy PDF:\n")
#lmfit.report_fit(o2)

params = lmfit.Parameters()
params.add('mu', 400, min=380,max=500)
params.add('sigma', 12, min=1)
params.add('amplitude', 6e6, min=1e5)
"""
params.add('mu', 400, min=380,max=500)
params.add('sigma', 12, min=1)
params.add('amplitude', 6e6, min=1e5)
"""
o3 = lmfit.minimize(normal_dist, params, args=(x2, y2), method=method)
print("# Fit using sum of squares:\n")
#lmfit.report_fit(o3)    
o4 = lmfit.minimize(normal_dist, params, args=(x2, y2), method=method,
                    #reduce_fcn='negentropy')
                    reduce_fcn='neglogcauchy')
print("\n\n# Robust Fit, using log-likelihood with Cauchy PDF:\n")
#lmfit.report_fit(o4)


gauss1 = GaussianModel(prefix='g1_')
#params = lmfit.Parameters()
pars1 = gauss1.guess(y1, x=x1)
pars1['g1_center'].set(value=415, min=380, max=500)
pars1['g1_sigma'].set(value=5, min=1)
pars1['g1_amplitude'].set(value=0.02, min=5e-4)
#print(pars1)

mod1 = gauss1

init1 = mod1.eval(pars1, x=x1)
out1 = mod1.fit(y1, pars1, x=x1,method='L-BFGS-B')
print(out1.fit_report(min_correl=0.5))

gauss6 = GaussianModel(prefix='g6_')
#params = lmfit.Parameters()
pars2 = gauss6.guess(y2, x=x2)
pars2['g6_center'].set(value=405, min=380, max=500)
pars2['g6_sigma'].set(value=5, min=1)
pars2['g6_amplitude'].set(value=0.002, min=5e-4)
#print(pars1)

mod2 = gauss6

init2 = mod2.eval(pars2, x=x2)
out2 = mod2.fit(y2, pars2, x=x2,method='L-BFGS-B')
print(out2.fit_report(min_correl=0.5))


grid = plt.GridSpec(2,2, wspace=0.2, hspace=0.2)
plt.subplot(grid[0,0])
plt.hist(wave1, alpha=0.7,bins=321,histtype="step",color="k",label=" PPO + bis-MSB sample wavelength")
plt.plot(x1, out1.best_fit,alpha=0.7,label=" gaussian fit")
plt.plot(x1, o1.residual,alpha=0.6,label=" sum of squares fit(L-BFGS-B method)")
plt.plot(x1, o2.residual,alpha=0.6,label=" robust fit(L-BFGS-B,log-likelihood) ")
#plt.plot(x1, out1.best_fit/y1.max())
#plt.xlabel(r"Degree [$^\circ$]",fontsize=16,horizontalalignment='right',x=1.0)
plt.xlabel(r"wavelength [nm]",fontsize=16,horizontalalignment='right',x=1.0)
plt.ylabel(ylabel='# of events [Normalized A.U.]',fontsize=16)
plt.legend(loc='best',fontsize=16,frameon=False)
plt.ylim(0,1e6)
#plt.xlim(380,460)
plt.ticklabel_format(axis='y',style='sci',scilimits=(0,6))
plt.xticks(np.arange(380, 700, step=20))
plt.xlim(380,460)

plt.subplot(grid[0,1])
plt.hist(wave2, alpha=0.7,bins=321,histtype="step",color="k",label=" PPO sample wavelength")
plt.plot(x2, out2.best_fit,alpha=0.7,label=" gaussian fit")
plt.plot(x2, o3.residual,alpha=0.6,label=" sum of squares fit(L-BFGS-B method)")
plt.plot(x2, o4.residual,alpha=0.6,label=" robust fit(L-BFGS-B method,log-likelihood) ")
#plt.hist(wave2, alpha=0.7,bins=321,histtype="step",density=True,color="k",label=" PPO sample wavelength")
#plt.xlabel(r"Degree [$^\circ$]",fontsize=16,horizontalalignment='right',x=1.0)
plt.xlabel(r"wavelength [nm]",fontsize=16,horizontalalignment='right',x=1.0)
plt.ylabel(ylabel='# of events [Normalized A.U.]',fontsize=16)
plt.ylim(0,1e6)
#plt.xlim(380,460)
plt.ticklabel_format(axis='y',style='sci',scilimits=(0,6))
plt.legend(loc='best',fontsize=16,frameon=False)
plt.xticks(np.arange(380, 700, step=20))
plt.xlim(380,460)

plt.subplot(grid[1,0])
plt.hist(hue1.ravel(),alpha=0.7,bins=361,histtype="step",color="k",density=True,label="Hue(PPO + bis-MSB sample)")
plt.axvspan(270, 360, alpha=0.5,color='red',label="forbiden range (270~360)")
plt.xlabel(r"Degree [$^\circ$]",fontsize=16,horizontalalignment='right',x=1.0)
#plt.xlabel(r"wavelength [nm]",fontsize=16,horizontalalignment='right',x=1.0)
plt.ylabel(ylabel='# of events [Normalized A.U.]',fontsize=16)
plt.legend(loc='best',fontsize=16,frameon=False)


plt.subplot(grid[1,1])
plt.hist(hue2.ravel(), alpha=0.7,bins=361,histtype="step",color="k",density=True,label="Hue(PPO sample)")
plt.axvspan(270, 360, alpha=0.5,color='red',label="forbiden range (270~360)")
plt.xlabel(r"Degree [$^\circ$]",fontsize=16,horizontalalignment='right',x=1.0)
#plt.xlabel(r"wavelength [nm]",fontsize=16,horizontalalignment='right',x=1.0)
plt.ylabel(ylabel='# of events [Normalized A.U.]',fontsize=16)
plt.legend(loc='best',fontsize=16,frameon=False)
plt.show()

#plt.savefig("{}_processing.pdf".format(name),transparent=True,bbox_inches="tight",pad_inches=0.1)
