#import os
import numpy as np
import matplotlib.pyplot as plt
from skimage import io
#from skimage.color import rgb2gray
#from skimage.color import rgba2rgb

import mplhep as hep
hep.set_style(hep.style.ROOT) # For now ROOT defaults to CMS


#import matplotlib as mpl
#import matplotlib.font_manager
#matplotlib.font_manager.findSystemFonts(fontpaths=None, fontext='ttf')
##mpl.use("pgf")
#pgf_with_pdflatex = {
#    "pgf.texsystem": "pdflatex",
#    "pgf.preamble": [
#         r"\usepackage[utf8x]{inputenc}",
#         r"\usepackage[T1]{fontenc}",
#         r"\usepackage{cmbright}",
#         ]
#}
#mpl.rcParams.update(pgf_with_pdflatex)
#mpl.rcParams['font.family'] = 'serif'


i="PPO_BM_6MeV_1"
image1 = io.imread('{}.JPG'.format(i))

j="PPO_BM_9MeV_1"
image2 = io.imread('{}.JPG'.format(j))

k="PPO_BM_12MeV_1"
image3 = io.imread('{}.JPG'.format(k))

grid = plt.GridSpec(2,3, wspace=0.2, hspace=0.1)

plt.subplot(grid[1,0])
plt.hist(image1[:,:,0].ravel(),bins=256, range=(0.0, 255.0),alpha=0.7,histtype="step",color="red",ls="-",lw=2,label="6MeV")
plt.hist(image2[:,:,0].ravel(),bins=256, range=(0.0, 255.0),alpha=0.9,histtype="step",color="magenta",ls="--",lw=2,label="9MeV")
plt.hist(image3[:,:,0].ravel(),bins=256, range=(0.0, 255.0),alpha=0.8,histtype="step",color="firebrick",ls=":",lw=2,label="12MeV")
plt.xlabel('Red pixel intesity number',fontsize=20,horizontalalignment='right',x=1.0)
plt.ylabel('# of events [A.U.]',fontsize=20)
plt.ylim(0,2e5)
plt.legend(loc='best',fontsize=20,frameon=False)
plt.ticklabel_format(style='sci',scilimits=(0,5))
#bbox_to_anchor=(0.55, 0.625))
#plt.yscale('log')

plt.subplot(grid[1,1])
plt.hist(image1[:,:,1].ravel(),bins=256, range=(0.0, 255.0),alpha=0.7,histtype="step",color="green",ls="-",lw=2,label="6MeV")
plt.hist(image2[:,:,1].ravel(),bins=256, range=(0.0, 255.0),alpha=0.9,histtype="step",color="lime",ls="--",lw=2,label="9MeV")
plt.hist(image3[:,:,1].ravel(),bins=256, range=(0.0, 255.0),alpha=0.8,histtype="step",color="lightseagreen",ls=":",lw=2,label="12MeV")
plt.xlabel('Green pixel intesity number',fontsize=20,horizontalalignment='right',x=1.0)
plt.ylabel('# of events [A.U.]',fontsize=20)
plt.ylim(0,2e5)
plt.legend(loc='best',fontsize=20,frameon=False)
plt.ticklabel_format(style='sci',scilimits=(0,5))
#,bbox_to_anchor=(0.55, 0.625))
#plt.yscale('log')

plt.subplot(grid[1,2])
plt.hist(image1[:,:,2].ravel(),bins=256, range=(0.0, 255.0),alpha=0.9,histtype="step",color="blue",ls="-",lw=2,label="6MeV")
plt.hist(image2[:,:,2].ravel(),bins=256, range=(0.0, 255.0),alpha=0.7,histtype="step",color="cornflowerblue",ls="--",lw=2,label="9MeV")
plt.hist(image3[:,:,2].ravel(),bins=256, range=(0.0, 255.0),alpha=0.9,histtype="step",color="deepskyblue",ls=":",lw=2,label="12MeV")
plt.xlabel('Blue pixel intesity number',fontsize=20,horizontalalignment='right',x=1.0)
plt.ylabel('# of events [A.U.]',fontsize=20)
plt.ylim(0,2e5)
plt.legend(loc='best',fontsize=20,frameon=False)
plt.ticklabel_format(style='sci',scilimits=(0,5))
#,bbox_to_anchor=(0.55, 0.625))
#plt.yscale('log')

plt.subplot(grid[0,0])
#plt.hist(blue1.ravel(),bins=2560, range=(0.0, 255.0),alpha=0.9,fc='b',histtype="step")
#plt.yscale('log')
plt.imshow(image1)
plt.axis("off")
#plt.title("Histogram {} for Blue channel on range (Dark=0,Bright=255)".format(i), fontsize=12)
plt.subplot(grid[0,1])
#plt.hist(blue2.ravel(),bins=2560, range=(0.0, 255.0),alpha=0.9,fc='b',histtype="step")
#plt.yscale('log')
plt.imshow(image2)
plt.axis("off")
#plt.title("Histogram {} for Blue channel on range (Dark=0,Bright=255)".format(j), fontsize=12)
plt.subplot(grid[0,2])
#plt.hist(blue3.ravel(),bins=2560, range=(0.0, 255.0),alpha=0.9,fc='b',histtype="step")
#plt.yscale('log')
plt.imshow(image3)
plt.axis("off")
#plt.title("Histogram {} for Blue channel on range (Dark=0,Bright=255)".format(k), fontsize=12)
#plt.subplots_adjust(wspace=1.0, hspace=1.5)
#fig.tight_layout(pad=0.4, w_pad=1.0, h_pad=1.5)

plt.show()
#plt.savefig("{}_processing.pdf".format(name),transparent=True,bbox_inches="tight",pad_inches=0.1)
#plt.savefig('Figure-7-b.pdf',dpi=300,transparent=True)
#plt.savefig('Figure-7-b.jpg', dpi=300, quality=95, optimize=True, progressive=True)  
