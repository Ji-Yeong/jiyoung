import os
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from skimage import io
from skimage.color import rgb2gray
from skimage.color import rgba2rgb
from skimage.color import rgb2hsv
from skimage import filters
from skimage.data import camera
from skimage.util import compare_images
from skimage.filters import gaussian
from skimage.segmentation import active_contour
from skimage.filters import try_all_threshold
from skimage import data, img_as_float
from skimage.segmentation import chan_vese
from matplotlib import cm
from collections import OrderedDict

#image5 = io.imread('0.PNG')
image5 = io.imread('12MeV_v3_ref.jpg')
#i="12MeV.PNG"
#i="UV254.jpg"
#i="UV365.jpg"
i="12MeV_v3.jpg"
#name = ["6MeV","9MeV","12MeV"]
image1 = io.imread('{}'.format(i))
#image11 = rgba2rgb(image1)
image12 = rgb2hsv(image1)

R1 = image1[:,:,0]
R2 = image1[:,:,1]
R3 = image1[:,:,2]

red11 = image12[:,:,0]*360.0
red1 = np.copy(red11)
red1[red11>=270.0]=np.nan

wave1 = 650.0-250.0/270.0*(red1.ravel())
green1 = image12[:,:,1]
blue1 = image12[:,:,2]
blue11= np.copy(blue1)
blue11[blue1<=0.05]=np.nan
grid = plt.GridSpec(2,2, wspace=0.1, hspace=0.1)
#weights_red1 = np.ones_like(red1.ravel())/float(len(red1.ravel()))
#weights_green1 = np.ones_like(green1.ravel())/float(len(green1.ravel()))
#weights_blue1 = np.ones_like(blue1.ravel())/float(len(blue1.ravel()))

plt.subplot(grid[0,0])
plt.hist(R1.ravel(),bins=256,range=(0.0, 255.0), alpha=0.7,histtype="step",color="r",label="R")
plt.hist(R2.ravel(),bins=256,range=(0.0, 255.0), alpha=0.7,histtype="step",color="g",label="G")
plt.hist(R3.ravel(),bins=256,range=(0.0, 255.0), alpha=0.7,histtype="step",color="b",label="B")
#plt.hist(R1, alpha=0.7,histtype="step",color="gray")
#plt.hist(red1.ravel()*360.0,bins=361,alpha=0.7,histtype="step",label="H")
plt.yscale('log')
plt.title(" {} RGB color space ".format(i) ,fontsize=12)
plt.legend(loc='best',fontsize=12)


plt.subplot(grid[0,1])
#plt.hist(red.ravel(),bins=2560, range=(0.0, 255.0),alpha=0.5)
plt.hist(image12[:,:,0].ravel(),bins=361,range=(0.0, 1.0),alpha=0.7,density=True,histtype="step",color="y",label="Hue (0~360)")
plt.hist(green1.ravel(),bins=101,range=(0.0, 1.0),alpha=0.7,density=True,histtype="step",color="m",label="Saturation (0.0~1.0)")
plt.hist(blue1.ravel(),bins=101,range=(0.0, 1.0),alpha=0.7,density=True,histtype="step",color="c",label="Value[lightness] (0.0~1.0)")
#plt.gca().twiny()
plt.yscale('log')
plt.title(" {} HSV color space ".format(i) ,fontsize=12)
plt.legend(loc='best',fontsize=12)
#plt.xscale('log')



plt.subplot(grid[1,0])
plt.hist(wave1, alpha=0.7,bins=361,histtype="step",color="gray",label="sample $\lambda$ spectrum")
#plt.hist(red1.ravel()*360.0,bins=361,alpha=0.7,histtype="step",label="H")
plt.yscale('log')
plt.axvline(x=421,label="bM $\lambda$= 421 nm line")
plt.title(" {} wavelength spectrum , no correction".format(i) ,fontsize=12)
plt.legend(loc='best',fontsize=12)

plt.subplot(grid[1,1])
#plt.imshow(image1)
#cmap=plt.cm.get_cmap('rainbow', 73)
plt.imshow(image5,alpha=0.8)
plt.imshow(blue11,alpha=0.5,cmap=plt.cm.jet)
"""
plt.subplot(grid[1,0])
#plt.hist(blue1.ravel(),bins=2560, range=(0.0, 255.0),alpha=0.9,fc='b',histtype="step")
#plt.yscale('log')
plt.imshow(image1)
plt.title("Histogram {} for Blue channel on range (Dark=0,Bright=255)".format(i), fontsize=12)
plt.subplot(grid[1,1])
#plt.hist(blue2.ravel(),bins=2560, range=(0.0, 255.0),alpha=0.9,fc='b',histtype="step")
#plt.yscale('log')
plt.imshow(image2)
plt.title("Histogram {} for Blue channel on range (Dark=0,Bright=255)".format(j), fontsize=12)
plt.subplot(grid[1,2])
#plt.hist(blue3.ravel(),bins=2560, range=(0.0, 255.0),alpha=0.9,fc='b',histtype="step")
#plt.yscale('log')
plt.imshow(image3)
plt.title("Histogram {} for Blue channel on range (Dark=0,Bright=255)".format(k), fontsize=12)
#plt.subplots_adjust(wspace=1.0, hspace=1.5)
#fig.tight_layout(pad=0.4, w_pad=1.0, h_pad=1.5)
"""
#plt.legend(loc='best',fontsize=14)
plt.show()

#plt.savefig("{}_processing.pdf".format(name),transparent=True,bbox_inches="tight",pad_inches=0.1)
