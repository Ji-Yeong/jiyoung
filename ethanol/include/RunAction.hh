
#ifndef  RunAction_h
#define  RunAction_h 1

#include "G4UserRunAction.hh"
#include "globals.hh"
#include <iostream>

class G4Run;
class TrackingAction;

class  RunAction : public G4UserRunAction
{
  public:
     RunAction();
    virtual ~ RunAction();

    virtual void BeginOfRunAction(const G4Run*);
    virtual void   EndOfRunAction(const G4Run*);

  //5월24일 추가 { 시작
  private:

  /////////////////
  // Histogramming
  //
  void CreateHistogram();
  void WriteHistogram();

  /////////////////
  // Worker
  //
  void BeginMaster(const G4Run*);
  void EndMaster(const G4Run*);

  /////////////////
  // Worker
  //
  void InitializeWorker(const G4Run*);
  void BeginWorker(const G4Run*);
  void EndWorker(const G4Run*);

  /////////////////
  // Print Info
  //
  void PrintRunInfo(const G4Run* run);

  /////////////////
  // Attributes
  //
  TrackingAction* fpTrackingAction;
  G4String fFileName; 
  bool fInitialized;
  bool fDebug;
  // 끝 }
};

#endif

