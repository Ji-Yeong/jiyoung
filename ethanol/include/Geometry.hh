//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Geometry.hh
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#ifndef Geometry_h
#define Geometry_h 1

#include "G4VUserDetectorConstruction.hh"
#include "G4VPhysicalVolume.hh"
#include "G4LogicalVolume.hh"
#include "G4Box.hh"
#include "G4Sphere.hh"
#include "G4Material.hh"
#include "G4NistManager.hh"
#include "G4PVPlacement.hh"
#include "G4UserLimits.hh"
#include "G4VisAttributes.hh"

//ElectroMagneticField
#include "G4ElectroMagneticField.hh"
#include "G4ElectricField.hh"
#include "G4EqMagElectricField.hh"
#include "G4UniformElectricField.hh"
#include "G4UniformMagField.hh"
#include "G4ThreeVector.hh"
#include "G4DormandPrince745.hh"

class G4LogicalVolume;
class G4VPhysicalVolume;

class G4FieldManager;
class G4ChordFinder;
class G4ElectroMagneticField;
class G4EquationOfMotion;
class G4Mag_EqRhs;
class G4MagIntegratorStepper;
class G4MagInt_Driver;
class G4EqMagElectricField;

class G4Region;

//------------------------------------------------------------------------------
  class Geometry : public G4VUserDetectorConstruction
//------------------------------------------------------------------------------
{
  public:
    Geometry();
   ~Geometry();
    void ElectricField();
    G4VPhysicalVolume* Construct();
    G4Region* GetTargetRegion()  {return fRegion;}

  
  private:
  
  G4double           fWorldSizeX;
  G4double           fWorldSizeY;
  G4double           fWorldSizeZ;

  G4VPhysicalVolume* fPhysiWorld;
  G4LogicalVolume*   fLogicWorld;
  G4VSolid* hollowBox;  
  G4Box*             fSolidWorld;

  G4Material*        fTargetMaterial;
  G4Material*        fBasketMaterial;
  G4Material*        fWorldMaterial;
  G4Region*          fRegion;

  void DefineMaterials();

  G4VPhysicalVolume* ConstructDetector();  
  

};
#endif
