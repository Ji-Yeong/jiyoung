//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Geant4 Application: Tutorial course for Hep/Medicine Users
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#include "Geometry.hh"
#include "UserActionInitialization.hh"
#include "PhysicsList.hh"

#include "G4UImanager.hh"
#include "G4UIcommand.hh"
#include "G4RunManager.hh"
#include "G4MTRunManager.hh"
#include "Randomize.hh"

#include "G4VisExecutive.hh"
#include "G4UIExecutive.hh"
#include "G4VModularPhysicsList.hh"
#include "G4EmStandardPhysics_option3.hh"
#include "FTFP_BERT.hh"
#include "QBBC.hh"
#include "G4EmExtraPhysics.hh"
#include "G4EmPenelopePhysics.hh"
//-------------------------------------------------------------------------------
  int main( int argc, char** argv )
//-------------------------------------------------------------------------------
{
   
   // Construct the default run manager
   #ifdef G4MULTITHREADED
   G4MTRunManager* runManager = new G4MTRunManager;
   G4int nThreads = std::min(G4Threading::G4GetNumberOfCores(),12);
   runManager->SetNumberOfThreads(nThreads);
   #else
   G4RunManager* runManager = new G4RunManager();
   #endif
   
   // Construct the default run manager
   //auto runManager = new G4RunManager;

   //choose the Random engine
   //CLHEP::HepRandom::setTheEngine(new CLHEP::Ranlux64Engine);
   G4Random::setTheEngine(new CLHEP::RanecuEngine);

// Set up mandatory user initialization: Geometry
   Geometry* detector = new Geometry;
   runManager->SetUserInitialization(detector);

// Set up mandatory user initialization: Physics-List
   G4VModularPhysicsList *physicslist = new FTFP_BERT;
   //G4VModularPhysicsList *physicslist = new QBBC_EMZ;
   //physicslist->RegisterPhysics(new G4EmExtraPhysics); 
   //physicslist->RegisterPhysics(new G4EmPenelopePhysics); 
   physicslist->RegisterPhysics(new G4EmStandardPhysics_option3); 
   runManager->SetUserInitialization( physicslist );
   
   //runManager->SetUserInitialization(new PhysicsList); //microelectronics
   //PhysicsList* phys = new PhysicsList(); // SHIP define the A' particle
   //runManager->SetUserInitialization( phys );
   
// Set up user initialization: User Actions
   runManager->SetUserInitialization( new UserActionInitialization(detector) );

// Initialize G4 kernel
   runManager->Initialize();

// Create visualization environment
   G4VisManager* visManager = new G4VisExecutive;
   visManager->Initialize();
   G4UImanager* UImanager = G4UImanager::GetUIpointer();

// Start interactive session
   G4UIExecutive* session = NULL;
   if (argc==1)   // Define UI session for interactive mode.
   {
      session = new G4UIExecutive(argc, argv);
   }
  
  if (argc==1)   // Define UI session for interactive mode.
  {
    UImanager->ApplyCommand("/control/execute ethanol.mac");
    session->SessionStart();
    delete session;
  }
  else           // Batch mode
  {
    G4String command = "/control/execute ";
    G4String fileName = argv[1];
    UImanager->ApplyCommand(command+fileName);
  }

// Job termination
   //delete uiExec;
   delete session;
   delete visManager;
   delete runManager;

   return 0;
}
