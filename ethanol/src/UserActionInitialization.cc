//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// UserActionInitialization.cc
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#include "G4RunManager.hh"

#include "UserActionInitialization.hh"
#include "PrimaryGenerator.hh"
#include "RunAction.hh"

#include "SteppingAction.hh"
#include "Geometry.hh"



//------------------------------------------------------------------------------
  UserActionInitialization::UserActionInitialization(Geometry* )
  : G4VUserActionInitialization()
//------------------------------------------------------------------------------
{}
//------------------------------------------------------------------------------
  UserActionInitialization::~UserActionInitialization()
//------------------------------------------------------------------------------
{}
//------------------------------------------------------------------------------
  void UserActionInitialization::BuildForMaster() const
//------------------------------------------------------------------------------
{
	// In MT mode, to be clearer, the RunAction class for the master thread might be
	// different than the one used for the workers.
	// This RunAction will be called before and after starting the
	// workers.
	// For more details, please refer to :
	// https://twiki.cern.ch/twiki/bin/view/Geant4/Geant4MTForApplicationDevelopers
	//
	// RunAction* runAction= new RunAction(fDetectorConstruction);
	// SetUserAction(runAction);
}
//------------------------------------------------------------------------------
  void UserActionInitialization::Build() const
//------------------------------------------------------------------------------
{
    SetUserAction( new PrimaryGenerator() );
       
    RunAction* runAction = new RunAction();
    SetUserAction(runAction);
    SetUserAction(new SteppingAction());

}




