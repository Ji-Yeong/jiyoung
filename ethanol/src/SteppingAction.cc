
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

#include "Analysis.hh"

#include "SteppingAction.hh"
#include "RunAction.hh"
#include "Geometry.hh"
#include "PrimaryGenerator.hh"

#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"
#include "G4SteppingManager.hh"
#include "G4VTouchable.hh"
#include "G4VPhysicalVolume.hh"
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

SteppingAction::SteppingAction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

SteppingAction::~SteppingAction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void SteppingAction::UserSteppingAction(const G4Step* step)
{ 
    const G4VProcess* CurrentProcess= step->GetPreStepPoint()->GetProcessDefinedStep();
    if (CurrentProcess != nullptr) 
    {
      G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
      analysisManager -> FillNtupleFColumn(0, step->GetTotalEnergyDeposit()/keV);
      //analysisManager -> FillNtupleFColumn(1, step->GetTrack()->GetTrackLength()/mm);
      analysisManager -> FillNtupleFColumn(1, step->GetTrack()->GetKineticEnergy()/keV);
      //analysisManager -> FillNtupleFColumn(3, step->GetTrack()->GetPosition().x()/mm);
      analysisManager -> FillNtupleFColumn(2, step->GetTrack()->GetPosition().y()/mm);
      //analysisManager -> FillNtupleFColumn(5, step->GetTrack()->GetPosition().z()/mm); 
      analysisManager -> FillNtupleIColumn(3, step->GetTrack()->GetTrackID()); 
      //analysisManager -> FillNtupleIColumn(5, step->GetTrack()->GetParentID()); 
      analysisManager -> AddNtupleRow();
    }
   
 
}    
