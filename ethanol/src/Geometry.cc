#include "Geometry.hh"

#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4VPhysicalVolume.hh"
#include "G4SubtractionSolid.hh"
#include "G4ThreeVector.hh"
#include "G4RotationMatrix.hh"
#include "G4Transform3D.hh"
#include "G4NistManager.hh"
#include "G4VisAttributes.hh"
#include "G4SystemOfUnits.hh"
//#include "SensitiveVolume.hh"
#include "G4SDManager.hh"

#include "G4Region.hh"
#include "G4ProductionCuts.hh"

#include "G4TransportationManager.hh"
#include "G4IntegrationDriver.hh"
#include "G4ChordFinder.hh"
#include "G4FieldManager.hh"
#include "G4UniformMagField.hh"
#include "G4UniformElectricField.hh"
#include "G4PropagatorInField.hh"

  Geometry::Geometry():fPhysiWorld(NULL), fLogicWorld(NULL), fSolidWorld(NULL) {}
  Geometry::~Geometry() {}
//------------------------------------------------------------------------------
  G4VPhysicalVolume* Geometry::Construct()
//------------------------------------------------------------------------------
{
  // Silicon is defined from NIST material database
  G4NistManager * man = G4NistManager::Instance();
  //G4Material * Si = man->FindOrBuildMaterial("G4_Si");
  
  G4double z, a , density,fractionmass;
  G4String name, symbol;
  G4int ncomponents, natoms;
  G4Element* el_H;
  G4Element* el_C;
  G4Element* el_O;
  a = 1.01*g/mole;  el_H = new G4Element(name="Hydrogen", symbol="H", z = 1.0,  a);
  a = 12.011*g/mole;  el_C = new G4Element(name="Cabon", symbol="C", z = 6.0, a);
  a = 15.99*g/mole;  el_O = new G4Element(name="Oxygen", symbol="O", z = 8.0, a);
  
  density = 1.000*g/cm3;//물 정의
  G4Material* H2O = new G4Material("Water",density,ncomponents=2);
  H2O->AddElement(el_H, natoms=2);
  H2O->AddElement(el_O, natoms=1);

  density = 1.19*g/cm3;//PMMA 정의 C5H8O2
  G4Material* PMMA = new G4Material("PMMA_Acrylic",density,ncomponents=3);
  PMMA->AddElement(el_H, natoms=8);
  PMMA->AddElement(el_O, natoms=2);
  PMMA->AddElement(el_C, natoms=5);

  density = 0.930*g/cm3; //2-에톡시 에탈올 정의
  G4Material* Two_EthOxyEthanol = new G4Material("TwoEthOxyEthanol",density,ncomponents=3);
  Two_EthOxyEthanol ->AddElement(el_C, natoms=4);
  Two_EthOxyEthanol ->AddElement(el_H, natoms=10);
  Two_EthOxyEthanol ->AddElement(el_O, natoms=2);

  density = 0.94518*g/cm3; // mixture  or compound 정의 선영이가 측정한 밀도 값 넣으면 됨
  G4Material * alcohol = new G4Material("alcohol",density,ncomponents=2);
  alcohol->AddMaterial(H2O,fractionmass=30.0*perCent);
  alcohol->AddMaterial(Two_EthOxyEthanol,fractionmass=70.0*perCent);
  
  // G4Material * alcohol = man->FindOrBuildMaterial("G4_ETHYL_ALCOHOL");
  //G4Material * basket = man->FindOrBuildMaterial("G4_GLASS_PLATE");
  G4Material * basket = man->FindOrBuildMaterial("PMMA_Acrylic");
  G4Material * vaccum = man->FindOrBuildMaterial("G4_Galactic");

  // Default materials in setup.
  fTargetMaterial = alcohol;
  fWorldMaterial = vaccum;
  fBasketMaterial = basket;
  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

  // WORLD VOLUME
  
  fWorldSizeX  = 80*cm; 
  fWorldSizeY  = 280*cm; 
  fWorldSizeZ  = 80*cm;

  fSolidWorld = new G4Box("World",				     //its name
			   fWorldSizeX/2,fWorldSizeY/2,fWorldSizeZ/2);  //its size
  

  fLogicWorld = new G4LogicalVolume(fSolidWorld,	//its solid
				   fWorldMaterial,		//its material
				   "World");		//its name
  
  fPhysiWorld = new G4PVPlacement(0,			//no rotation
  				 G4ThreeVector(),	//at (0,0,0)
                                 "World",		//its name
                                 fLogicWorld,		//its logical volume
                                 0,			//its mother  volume
                                 false,			//no boolean operation
                                 10);			//copy number

  
  G4double TargetSizeX_basket =  60*cm;
  G4double TargetSizeY_basket =  60*cm; 
  G4double TargetSizeZ_basket =  60*cm; 

  G4Box* basketSolid = new G4Box("Basket_solid",				     //its name
				 TargetSizeX_basket/2,TargetSizeY_basket/2,TargetSizeZ_basket/2);   //its size

  G4double TargetSizeX =  59.0*cm;
  G4double TargetSizeY =  59.5*cm; 
  G4double TargetSizeZ =  59.0*cm; 

  G4Box* targetSolid = new G4Box("Target",				     //its name
				 TargetSizeX/2,TargetSizeY/2,TargetSizeZ/2);   //its size
  
  hollowBox = new G4SubtractionSolid("basket",basketSolid,targetSolid,0,G4ThreeVector(0,0.25*cm,0));

  G4LogicalVolume* logicBasket = new G4LogicalVolume(hollowBox,       //its solid
						     fBasketMaterial,	//its material
						     "basket");		//its name
  new G4PVPlacement(0,			                               //no rotation
		    G4ThreeVector(0,-30.0*cm,0),	                               //at (0,0,0)
		    "Target",		//its name
		    logicBasket,	//its logical volume
		    fPhysiWorld,		//its mother  volume
		    false,		//no boolean operation
		    100);			//copy number

  G4LogicalVolume* logicTarget = new G4LogicalVolume(targetSolid,       //its solid
						     fTargetMaterial,	//its material
						     "Target");		//its name
  
  new G4PVPlacement(0,			                               //no rotation
		    G4ThreeVector(0,-29.75*cm,0),	                               //at (0,0,0)
		    "Target",		//its name
		    logicTarget,	//its logical volume
		    fPhysiWorld,		//its mother  volume
		    false,		//no boolean operation
		    200);			//copy number

  // Visualization attributes
  G4VisAttributes* worldVisAtt= new G4VisAttributes(G4Colour(1.0,1.0,1.0)); //White
  worldVisAtt->SetVisibility(true);
  //worldVisAtt->SetForceSolid(true);
  //worldVisAtt->SetForceSolid(true);
  fLogicWorld->SetVisAttributes(worldVisAtt);

  G4VisAttributes* worldVisAtt1 = new G4VisAttributes(G4Colour(1.0,0.0,0.0,0.1)); //read
  worldVisAtt1->SetVisibility(true);
  //worldVisAtt->SetForceSolid(true);
  worldVisAtt1->SetForceWireframe(true);
  logicTarget->SetVisAttributes(worldVisAtt1);

  G4VisAttributes* worldVisAtt2 = new G4VisAttributes(G4Colour(0.0,1.0,1.0,1.0)); //Cyan
  worldVisAtt2->SetVisibility(true);
  worldVisAtt2->SetLineWidth(1.0);
  //worldVisAtt->SetForceSolid(true);
  worldVisAtt2->SetForceWireframe(true);
  logicBasket->SetVisAttributes(worldVisAtt2);

  // Create Target G4Region and add logical volume
  
  fRegion = new G4Region("Target");
  
  G4ProductionCuts* cuts = new G4ProductionCuts();
  
  G4double defCut = 1*nanometer;
  //G4double defCut = 1*micrometer;
  cuts->SetProductionCut(defCut,"gamma");
  cuts->SetProductionCut(defCut,"e-");
  cuts->SetProductionCut(defCut,"e+");
  cuts->SetProductionCut(defCut,"proton");
  
  fRegion->SetProductionCuts(cuts);
  fRegion->AddRootLogicalVolume(logicTarget); 

  return fPhysiWorld;
}