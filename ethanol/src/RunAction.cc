#include "RunAction.hh"
#include "Analysis.hh"

#include "G4Run.hh"
#include "G4RunManager.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"
#include "G4Threading.hh"
#include "G4ParticleDefinition.hh"



//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

RunAction::RunAction()
{
 //fFileName = "ethanol";
 fFileName = "water";
 fpTrackingAction = 0;
 fInitialized = 0;
 fDebug = false;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

RunAction::~RunAction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void RunAction::BeginOfRunAction(const G4Run* run)
{  
  // In this example, we considered that the same class was
  // used for both master and worker threads.
  // However, in case the run action is long,
  // for better code review, this practice is not recommanded.
  //
  
  if(isMaster) // WARNING : in sequential mode, isMaster == true    
    BeginMaster(run);    
  else 
    BeginWorker(run);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void RunAction::EndOfRunAction(const G4Run* run)
{
  if(isMaster)   
    EndMaster(run);
  else	
    EndWorker(run);
}


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void RunAction::BeginMaster(const G4Run* run)
{
  bool sequential = (G4RunManager::GetRunManager()->GetRunManagerType() == G4RunManager::sequentialRM);
  
  if(fDebug)
    {
      G4cout << "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°" << G4endl;
      if(!sequential)
	G4cout << "°°°°°°°°°°°°°°°° RunAction::BeginMaster" << G4endl;
      PrintRunInfo(run);
      G4cout << "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°" << G4endl;
    }
  
  if(sequential)
    {
      if(!fInitialized)	
	InitializeWorker(run);
      // Note: fpTrackingAction could be used as a flag for initialization instead      

      CreateHistogram();
    }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void RunAction::BeginWorker(const G4Run* run)
{
  if(fDebug)
    {
      G4cout << "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°" << G4endl;
      G4cout << "°°°°°°°°°°°°°°°° RunAction::BeginWorker" << G4endl;
      PrintRunInfo(run);
      G4cout << "°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°" << G4endl;
    }
  if(!fInitialized)	
    InitializeWorker(run);
  
  CreateHistogram();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void RunAction::EndMaster(const G4Run* run)
{
  bool sequential = (G4RunManager::GetRunManager()->GetRunManagerType() 
		     == G4RunManager::sequentialRM);
  if(sequential)    
    EndWorker(run);    
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void RunAction::EndWorker(const G4Run* run)
{
  if(fDebug)
    {
      PrintRunInfo(run);
    }
  
  G4int nofEvents = run->GetNumberOfEvent();
  if ( nofEvents == 0 )
    {
      if(fDebug)
	{
	  G4cout << "°°°°°°°°°°°°°°°° NO EVENTS TREATED IN THIS RUN ==> LEAVING RunAction::EndOfRunAction "<< G4endl;
	}
      return;
    }
  
  ///////////////
  // Write Histo
  //
  WriteHistogram();
  
  ///////////////
  // Complete cleanup
  //
  delete G4AnalysisManager::Instance();
  
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void RunAction::InitializeWorker(const G4Run*)
{
	fInitialized = true;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void RunAction::CreateHistogram()
{
	// Book histograms, ntuple

	// Create analysis manager
	// The choice of analysis technology is done via selection of a namespace
	// in Analysis.hh

	G4cout << "##### Create analysis manager " << "  " << this << G4endl;
	G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();

	G4cout << "Using " << analysisManager->GetType() << " analysis manager" << G4endl;

	// Create directories

	//analysisManager->SetHistoDirectoryName("histograms");
	//analysisManager->SetNtupleDirectoryName("ntuple");
	analysisManager->SetVerboseLevel(1);
	// Creating ntuple
  analysisManager -> OpenFile("ethanol_field_gps_electron");
  analysisManager -> CreateNtuple("ethanol", "hist");
  analysisManager -> CreateNtupleFColumn("Track_Deposit_E");
  //analysisManager -> CreateNtupleFColumn("Track_Length");
  analysisManager -> CreateNtupleFColumn("Track_Kinetic_E");
  //analysisManager -> CreateNtupleFColumn("x");
  analysisManager -> CreateNtupleFColumn("y");
  //analysisManager -> CreateNtupleFColumn("z");
  analysisManager -> CreateNtupleIColumn("Track_ID");
  //analysisManager -> CreateNtupleIColumn("Parent_ID");
  analysisManager -> FinishNtuple();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void RunAction::WriteHistogram()
{
	// print histogram statistics
	//
	G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();

	// save histograms
	//
	analysisManager->Write();
	analysisManager->CloseFile();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void RunAction::PrintRunInfo(const G4Run* run)
{
	G4cout << "°°°°°°°°°°°°°°°° Run is = " << run->GetRunID() << G4endl;
	G4cout << "°°°°°°°°°°°°°°°° Run type is = " << G4RunManager::GetRunManager()->GetRunManagerType() << G4endl;
	G4cout << "°°°°°°°°°°°°°°°° Event processed = " << run->GetNumberOfEventToBeProcessed() << G4endl;
	G4cout << "°°°°°°°°°°°°°°°° N° Event = " << run->GetNumberOfEvent() << G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
 

