import numpy as np
import matplotlib.pyplot as plt

"""
e_6mev_1 = np.genfromtxt("bragg_6mev_5cm_1.txt",dtype=np.float64,comments='#')
e_6mev_2 = np.genfromtxt("bragg_6mev_5cm_2.txt",dtype=np.float64,comments='#')
e_6mev_3 = np.genfromtxt("bragg_6mev_10cm_1.txt",dtype=np.float64,comments='#')
e_6mev_4 = np.genfromtxt("bragg_6mev_10cm_2.txt",dtype=np.float64,comments='#')
e_6mev_5 = np.genfromtxt("bragg_6mev_15cm_1.txt",dtype=np.float64,comments='#')
e_6mev_6 = np.genfromtxt("bragg_6mev_15cm_2.txt",dtype=np.float64,comments='#')
e_6mev_7 = np.genfromtxt("bragg_6mev_20cm_1.txt",dtype=np.float64,comments='#')
e_6mev_8 = np.genfromtxt("bragg_6mev_20cm_2.txt",dtype=np.float64,comments='#')
"""
"""
e_6mev_1 = np.genfromtxt("bragg_6mev_2cm_gps1.txt",dtype=np.float64,comments='#')
e_6mev_2 = np.genfromtxt("bragg_6mev_2cm_gps2.txt",dtype=np.float64,comments='#')
e_6mev_3 = np.genfromtxt("bragg_6mev_4cm_gps1.txt",dtype=np.float64,comments='#')
e_6mev_4 = np.genfromtxt("bragg_6mev_4cm_gps2.txt",dtype=np.float64,comments='#')
#e_6mev_5 = np.genfromtxt("bragg_6mev_6cm_gps1.txt",dtype=np.float64,comments='#')
#e_6mev_6 = np.genfromtxt("bragg_6mev_6cm_gps2.txt",dtype=np.float64,comments='#')
#e_6mev_7 = np.genfromtxt("bragg_6mev_8cm_gps1.txt",dtype=np.float64,comments='#')
#e_6mev_8 = np.genfromtxt("bragg_6mev_8cm_gps2.txt",dtype=np.float64,comments='#')
"""
"""
plt.plot(-e_6mev_1[:,0],e_6mev_1[:,1], label='6MeV electron ,Track_ID = 1, 5cm')
plt.plot(-e_6mev_2[:,0],e_6mev_2[:,1], label='6MeV electron ,Track_ID = 1, Kin_E = 0, 5 cm')
plt.plot(-e_6mev_3[:,0],e_6mev_3[:,1], label='6MeV electron ,Track_ID = 1, 10cm')
plt.plot(-e_6mev_4[:,0],e_6mev_4[:,1], label='6MeV electron ,Track_ID = 1, Kin_E = 0, 10 cm')
#plt.plot(-e_6mev_5[:,0],e_6mev_5[:,1], label='6MeV electron ,Track_ID = 1, 15cm')
#plt.plot(-e_6mev_6[:,0],e_6mev_6[:,1], label='6MeV electron ,Track_ID = 1, Kin_E = 0, 15 cm')
#plt.plot(-e_6mev_7[:,0],e_6mev_7[:,1], label='6MeV electron ,Track_ID = 1, 20cm')
#plt.plot(-e_6mev_8[:,0],e_6mev_8[:,1], label='6MeV electron ,Track_ID = 1, Kin_E = 0, 20 cm')
"""
e_6mev_1 = np.genfromtxt("e_6MeV_3cm_1.txt",dtype=np.float64,comments='#')
e_6mev_2 = np.genfromtxt("e_9MeV_3cm_1.txt",dtype=np.float64,comments='#')
e_6mev_3 = np.genfromtxt("e_12MeV_3cm_1.txt",dtype=np.float64,comments='#')
#e_6mev_4 = np.genfromtxt("e_6MeV_6cm_0.txt",dtype=np.float64,comments='#')
#e_6mev_5 = np.genfromtxt("e_9MeV_6cm_0.txt",dtype=np.float64,comments='#')
#e_6mev_6 = np.genfromtxt("e_12MeV_6cm_0.txt",dtype=np.float64,comments='#')

#plt.plot(e_6mev_1[:,0],e_6mev_1[:,1],label='6MeV electron beam 3cm',linewidth=4)
#plt.plot(e_6mev_2[:,0],e_6mev_2[:,1],label='9MeV electron beam 3cm',linewidth=4)
#plt.plot(e_6mev_3[:,0],e_6mev_3[:,1],label='12MeV electron beam 3cm',linewidth=4)
#plt.plot(e_6mev_4[:,0],e_6mev_4[:,1]/0.356044*100.0,label='6MeV electron beam 6cm',linewidth=4,linestyle='dashed')
#plt.plot(e_6mev_5[:,0],e_6mev_5[:,1]/0.246752*100.0,label='9MeV electron beam 6cm',linewidth=4,linestyle='dashed')
#plt.plot(e_6mev_6[:,0],e_6mev_6[:,1]/0.185369*100.0,label='12MeV electron beam 6cm',linewidth=4,linestyle='dashed')

#plt.plot(e_6mev_4[:,0],e_6mev_4[:,1]/0.356044*100.0,label='6MeV electron beam 6cm',color = 'red',linewidth=4,linestyle='-')
#plt.plot(e_6mev_5[:,0],e_6mev_5[:,1]/0.246752*100.0,label='9MeV electron beam 6cm',color = 'blue',linewidth=4,linestyle='-')
#plt.plot(e_6mev_6[:,0],e_6mev_6[:,1]/0.185369*100.0,label='12MeV electron beam 6cm',color = 'green',linewidth=4,linestyle='-')

#plt.plot(e_6mev_4[:,0],e_6mev_4[:,1]/102.546*100.0,label='6MeV electron beam 6cm',color = 'red',linewidth=4,linestyle='-')
#plt.plot(e_6mev_5[:,0],e_6mev_5[:,1]/96.2283*100.0,label='9MeV electron beam 6cm',color = 'blue',linewidth=4,linestyle='-')
#plt.plot(e_6mev_6[:,0],e_6mev_6[:,1]/95.5258*100.0,label='12MeV electron beam 6cm',color = 'green',linewidth=4,linestyle='-')

"""
plt.plot(e_6mev_1[:,0],e_6mev_1[:,1]/0.561535*100, color='red',label='6MeV electron beam',linewidth=4)
plt.plot(e_6mev_2[:,0],e_6mev_2[:,1]/0.393706*100,color='blue', label='9MeV electron beam',linewidth=4)
plt.plot(e_6mev_3[:,0],e_6mev_3[:,1]/0.30038*100, color='green',label='12MeV electron beam',linewidth=4)
"""
"""
plt.plot(e_6mev_1[:,0],e_6mev_1[:,1]/124.002*100, color='red',label='6MeV electron beam',linewidth=3)
plt.plot(e_6mev_2[:,0],e_6mev_2[:,1]/115.439*100,color='blue', label='9MeV electron beam',linewidth=3)
plt.plot(e_6mev_3[:,0],e_6mev_3[:,1]/108.583*100, color='green',label='12MeV electron beam',linewidth=3)
"""

plt.plot(e_6mev_1[:,0],e_6mev_1[:,1]/157.7*100, color='red',label='6MeV electron beam',linewidth=3)
plt.plot(e_6mev_2[:,0],e_6mev_2[:,1]/150.073*100,color='blue', label='9MeV electron beam',linewidth=3)
plt.plot(e_6mev_3[:,0],e_6mev_3[:,1]/144.223*100, color='green',label='12MeV electron beam',linewidth=3)

plt.xlim(0,100)
plt.ylim(0.01,120)

#plt.yscale("log")
plt.xlabel('Depth [mm]',fontsize=24)
plt.ylabel('Relative deposit energy to peak [%]',fontsize=24)
#plt.xscale("log")
plt.xticks(fontsize=18)
plt.yticks(fontsize=18)
"""
x = np.arange(0,100)
f = np.full(100,50.0, dtype=np.float64)
idx1 = np.argwhere(np.diff(np.sign(f - e_6mev_1[:,1]/157.7*100.0))).flatten()
plt.plot(x[idx1], f[idx1], 'o')
"""
plt.axhline(y=0.24, color='y', linestyle='--',label='Bremsstrahlung background line',linewidth=2)
plt.axhline(y=100, color='m', linestyle=':',label='100 percent line')
plt.axhline(y=50, color='m', linestyle='--',label='50 percent line')
plt.axvline(x=31.1755, color='r', linestyle='--')
plt.axvline(x=47.4769, color='b', linestyle='--')
plt.axvline(x=63.4488, color='g', linestyle='--')

xxi = np.linspace(0,100,100)
m1, b1 = np.polyfit(e_6mev_1[32:34,0],e_6mev_1[32:34,1]/157.7*100,1)
m2, b2 = np.polyfit(e_6mev_2[48:50,0],e_6mev_2[48:50,1]/150.073*100,1)
m3, b3 = np.polyfit(e_6mev_3[64:68,0],e_6mev_3[64:68,1]/144.223*100,1)
plt.plot(xxi,m1*xxi+b1,color='red',label='6MeV practical ragne extrapolate',linewidth=1,linestyle='-.')
plt.plot(xxi,m2*xxi+b2,color='blue',label='9MeV practical ragne extrapolate',linewidth=1,linestyle='-.')
plt.plot(xxi,m3*xxi+b3,color='green',label='12MeV practical ragne extrapolate',linewidth=1,linestyle='-.')

plt.legend(loc='best',fontsize=16)
"""
plt.text(10, 1.34, r'Bremsstrahlung bkg line', color= 'y',fontsize=20)
plt.text(80, 52, r'50% line', color= 'm',fontsize=20)
plt.text(10, 102, r'100% line', color= 'y',fontsize=20)
"""
"""
plt.text(32, 1.1, r'6 MeV', color= 'red',fontsize=25)
plt.text(52, 1.1, r'9 MeV', color= 'blue',fontsize=25)
plt.text(72, 1.1, r'12 MeV', color= 'green',fontsize=25)
"""
plt.show()