import numpy as np
import scipy as sp
from scipy import interpolate
from scipy.interpolate import Rbf, InterpolatedUnivariateSpline
from scipy.interpolate import UnivariateSpline
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.font_manager

matplotlib.font_manager.findSystemFonts(fontpaths=None, fontext='ttf')
#mpl.use("pgf")
pgf_with_pdflatex = {
    "pgf.texsystem": "pdflatex",
    "pgf.preamble": [
         r"\usepackage[utf8x]{inputenc}",
         r"\usepackage[T1]{fontenc}",
         r"\usepackage{cmbright}",
         ]
}
mpl.rcParams.update(pgf_with_pdflatex)
mpl.rcParams['font.family'] = 'serif'

file1 = np.genfromtxt("ethanol_6meV_electron_945.txt",dtype=np.float64,comments='#')
file2 = np.genfromtxt("ethanol_9meV_electron_945.txt",dtype=np.float64,comments='#')
file3 = np.genfromtxt("ethanol_12meV_electron_945.txt",dtype=np.float64,comments='#')

file4 = np.genfromtxt("ethanol_60MeV_proton_945_0.txt",dtype=np.float64,comments='#')
file5 = np.genfromtxt("ethanol_120MeV_proton_945_0.txt",dtype=np.float64,comments='#')
file6 = np.genfromtxt("ethanol_200MeV_proton_945_0.txt",dtype=np.float64,comments='#')

from matplotlib.ticker import AutoMinorLocator, FormatStrFormatter, FixedLocator, FixedFormatter ,MultipleLocator
#fig = plt.figure(figsize=(27.9, 15.7))
fig = plt.figure()
ax = fig.gca()
#ax = plt.subplot()
x_formatter = FixedFormatter([1.5])
x_locator = FixedLocator([1.5])
ax.xaxis.set_minor_formatter(x_formatter)
ax.xaxis.set_minor_locator(x_locator)
ax.xaxis.set_major_locator(MultipleLocator(10))
ax.yaxis.set_major_locator(MultipleLocator(10))
ax.xaxis.set_major_formatter(FormatStrFormatter("%.0f"))
ax.yaxis.set_major_formatter(FormatStrFormatter("%.0f"))
ax.tick_params(axis="x", direction="out", length=4, width=2, color="turquoise")
ax.tick_params(axis="y", direction="out", length=4, width=2, color="orange")

ax.set_xlim(1.5,150)
ax.set_ylim(0.0,120)
ax.set_xlabel('Depth [mm]',fontsize=24,horizontalalignment='right', x=1.0)
ax.set_ylabel('Percentage depth dose [%]',fontsize=24)
#ax.xaxis.set_label_coords(.95, -0.055)

ax.plot(file1[:,0],file1[:,1]/file1[:,1].max()*100, 'r-',label='6MeV electron',linewidth=3)
ax.plot(file2[:,0],file2[:,1]/file2[:,1].max()*100, 'b--', label='9MeV electron',linewidth=3)
ax.plot(file3[:,0],file3[:,1]/file3[:,1].max()*100, 'g-.',label='12MeV electron',linewidth=3)
ax.plot(file4[:,0],file4[:,1]/file4[:,1].max()*100, 'm:',label='60MeV proton',linewidth=3)
ax.plot(file5[:,0],file5[:,1]/file5[:,1].max()*100, 'k+', label='120MeV proton',linewidth=1,markersize=10)
ax.plot(file6[:,0],file6[:,1]/file6[:,1].max()*100, 'yx',drawstyle="default", label='200MeV proton',linewidth=1,markersize=10)

plt.xticks(fontsize=24)
plt.yticks(fontsize=24)
ax.legend(loc='best',fontsize=24,frameon=False)
plt.show()

#plt.savefig('Figure-4.pdf',dpi=300,transparent=True)
#plt.savefig('Figure-4.jpg', dpi=300, quality=95, optimize=True, progressive=True)  

"""
plt.xlim(0,100)
plt.ylim(0.0,120)
plt.xlabel('Depth [mm]',fontsize=24)
plt.ylabel('Relative deposit energy to peak [%]',fontsize=24)
plt.xticks(fontsize=18)
plt.yticks(fontsize=18)
plt.legend(loc='best',fontsize=24)
plt.show()
"""