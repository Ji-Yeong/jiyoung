{

#include <iostream>
#include <TROOT.h>
#include <TBrowser.h>
#include <TFile.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TH2D.h>
#include <TH1D.h>
#include <TPad.h>
#include <TArrow.h>
gROOT->Reset();
gROOT->SetStyle("Plain");
gStyle->SetPalette(1);
gStyle->SetOptStat(0);
gROOT->ForceStyle();
auto can2 = new TCanvas ("can2","can2",800,600);
can2->Divide(1,1);


TFile* ethanol_6MeV_3cm = new TFile("water_field_gps_6mev.root");
TFile* ethanol_9MeV_3cm = new TFile("water_field_gps_9mev.root");
TFile* ethanol_12MeV_3cm = new TFile("water_field_gps_12mev.root");

TTree* e_6MeV_3cm  = (TTree*)ethanol_6MeV_3cm ->Get("ethanol");
TTree* e_9MeV_3cm  = (TTree*)ethanol_9MeV_3cm ->Get("ethanol");
TTree* e_12MeV_3cm = (TTree*)ethanol_12MeV_3cm->Get("ethanol");

int bin_number = 600;
int particle_number = 1000000;

TH1D* scatter_6MeV_3cm = new TH1D("scatter_6MeV_3cm","",bin_number,0.0,600.0);
TH1D* scatter_9MeV_3cm = new TH1D("scatter_9MeV_3cm","",bin_number,0.0,600.0);
TH1D* scatter_12MeV_3cm = new TH1D("scatter_12MeV_3cm","",bin_number,0.0,600.0);
TH1D* scatter_6MeV_3cm1 = new TH1D("scatter_6MeV_3cm1","",bin_number,0.0,600.0);
TH1D* scatter_9MeV_3cm1 = new TH1D("scatter_9MeV_3cm1","",bin_number,0.0,600.0);
TH1D* scatter_12MeV_3cm1 = new TH1D("scatter_12MeV_3cm1","",bin_number,0.0,600.0);

TCut mycut = "Track_ID == 1 && Track_Kinetic_E == 0";
TCut mycut1 = "Track_ID == 1";
//TCut mycut = "Track_ID == 1 && Parent_ID == 0 && (Track_Kinetic_E > 0)  && (y <0 && y < -30 ) ";
//Track_ID == 1 && Track_Kinetic_E == 0

e_6MeV_3cm ->Draw("-y >> scatter_6MeV_3cm",mycut1,"goff");
e_9MeV_3cm ->Draw("-y >> scatter_9MeV_3cm",mycut1,"goff");
e_12MeV_3cm->Draw("-y >> scatter_12MeV_3cm",mycut1,"goff");

e_6MeV_3cm ->Draw("-y >> scatter_6MeV_3cm1",mycut,"goff");
e_9MeV_3cm ->Draw("-y >> scatter_9MeV_3cm1",mycut,"goff");
e_12MeV_3cm->Draw("-y >> scatter_12MeV_3cm1",mycut,"goff");

//e_6MeV_3cm ->Draw("Track_Kinetic_E>>scatter_6MeV_3cm",mycut,"goff");
//e_9MeV_3cm ->Draw("Track_Kinetic_E>>scatter_9MeV_3cm",mycut,"goff");
//Track_ID == 1 && Track_Kinetic_E == 0
//e_12MeV_3cm->Draw("Track_Kinetic_E>>scatter_12MeV_3cm",mycut,"goff");

cout << scatter_6MeV_3cm->GetXaxis()->GetBinCenter(scatter_6MeV_3cm->GetMaximumBin()) <<" :6MeV nocut max " 
<< scatter_9MeV_3cm->GetXaxis()->GetBinCenter( scatter_9MeV_3cm->GetMaximumBin()) <<" :9MeV nocut max "
<< scatter_6MeV_3cm->GetXaxis()->GetBinCenter(scatter_12MeV_3cm->GetMaximumBin()) <<" :12MeV nocut max " << endl;

cout << scatter_6MeV_3cm->GetRMS(1) << " :6MeV nocut rms " << scatter_9MeV_3cm->GetRMS(1) << " : 9MeV nocut rms "
<< scatter_12MeV_3cm->GetRMS(1) << ": 12 MeV nocut rms" << endl;
cout << scatter_6MeV_3cm->GetMean(1) << " :6MeV nocut mean" << scatter_9MeV_3cm->GetMean(1) << " :9MeV nocut mean"
<< scatter_12MeV_3cm->GetMean(1) << " :12MeV nocut mean" << endl;

cout << scatter_6MeV_3cm1->GetXaxis()->GetBinCenter(scatter_6MeV_3cm1->GetMaximumBin()) <<" :6MeV cut max " 
<< scatter_9MeV_3cm1->GetXaxis()->GetBinCenter( scatter_9MeV_3cm1->GetMaximumBin()) <<" :9MeV cut max "
<< scatter_6MeV_3cm1->GetXaxis()->GetBinCenter(scatter_12MeV_3cm1->GetMaximumBin()) <<" :12MeV cut max " << endl;

cout << scatter_6MeV_3cm1->GetRMS(1) << " :6MeV cut rms " << scatter_9MeV_3cm1->GetRMS(1) << " : 9MeV cut rms "
<< scatter_12MeV_3cm1->GetRMS(1) << ": 12 MeV cut rms" << endl;
cout << scatter_6MeV_3cm1->GetMean(1) << " :6MeV cut mean" << scatter_9MeV_3cm1->GetMean(1) << " :9MeV cut mean"
<< scatter_12MeV_3cm1->GetMean(1) << " :12MeV cut mean" << endl;

TLegend *leg = new TLegend(0.6,0.1,0.80,0.5);
//leg->SetHeader("Electron beam range with 6,9,12 MeV","C");
leg->SetBorderSize(0);
leg->SetFillStyle(0);
TLegendEntry* l1 = leg->AddEntry(scatter_6MeV_3cm,"6MeV","l");
l1->SetTextColor(kRed);
l1->SetLineColor(kRed);
l1->SetLineWidth(4);
TLegendEntry* l2 = leg->AddEntry(scatter_9MeV_3cm,"9MeV","l");
l2->SetTextColor(kBlue);
l2->SetLineColor(kBlue);
l2->SetLineWidth(4);
TLegendEntry* l3 = leg->AddEntry(scatter_12MeV_3cm,"12MeV","l");
l3->SetTextColor(kGreen);
l3->SetLineColor(kGreen);
l3->SetLineWidth(4);
TLegendEntry* l4 = leg->AddEntry(scatter_6MeV_3cm1,"6MeV_{track w/ E_{k}=0}","l");
l4->SetTextColor(kRed);
l4->SetLineColor(kRed);
l4->SetLineWidth(4);
l4->SetLineStyle(2);
TLegendEntry* l5 = leg->AddEntry(scatter_9MeV_3cm1,"9MeV_{track w/ E_{k}=0}","l");
l5->SetTextColor(kBlue);
l5->SetLineColor(kBlue);
l5->SetLineWidth(4);
l5->SetLineStyle(2);
TLegendEntry* l6 = leg->AddEntry(scatter_12MeV_3cm1,"12MeV_{track w/ E_{k}=0}","l");
l6->SetTextColor(kGreen);
l6->SetLineColor(kGreen);
l6->SetLineWidth(4);
l6->SetLineStyle(2);


//can2->cd(0);
can2->cd();
   //gPad->SetGrid(1);
   scatter_6MeV_3cm->SetXTitle("Depth[mm]");
   scatter_6MeV_3cm->SetYTitle("Accumulated number");
   scatter_6MeV_3cm->GetYaxis()->SetTitleOffset(0);
   scatter_6MeV_3cm->SetLineColor(kRed);
   scatter_6MeV_3cm->SetLineWidth(2);
   scatter_6MeV_3cm->Draw();
   leg->Draw();
   
   scatter_9MeV_3cm->SetLineColor(kBlue);
   scatter_9MeV_3cm->SetLineWidth(2);
   scatter_9MeV_3cm->Draw("SAME");
   
   scatter_12MeV_3cm->SetLineColor(kGreen);
   scatter_12MeV_3cm->SetLineWidth(2);
   scatter_12MeV_3cm->Draw("SAME");
   
   scatter_6MeV_3cm1->SetLineColor(kRed);
   scatter_6MeV_3cm1->SetLineWidth(2);
   scatter_6MeV_3cm1->SetLineStyle(2);
   scatter_6MeV_3cm1->Draw("SAME");
   
   scatter_9MeV_3cm1->SetLineColor(kBlue);
   scatter_9MeV_3cm1->SetLineWidth(2);
   scatter_9MeV_3cm1->SetLineStyle(2);
   scatter_9MeV_3cm1->Draw("SAME");
   
   scatter_12MeV_3cm1->SetLineColor(kGreen);
   scatter_12MeV_3cm1->SetLineWidth(2);
   scatter_12MeV_3cm1->SetLineStyle(2);
   scatter_12MeV_3cm1->Draw("SAME"); 
   gPad->Modified();
   TPad *p = new TPad("p","p", 0.58,0.5,0.9,0.87);
   p->Draw();
   p->cd();
   scatter_6MeV_3cm1->SetXTitle("Range[mm]");
   scatter_6MeV_3cm1->Draw();
   scatter_9MeV_3cm1->Draw("SAME");
   scatter_12MeV_3cm1->Draw("SAME"); 
   scatter_6MeV_3cm->Draw("SAME");
   scatter_9MeV_3cm->Draw("SAME");
   scatter_12MeV_3cm->Draw("SAME"); 

   gPad->Modified();

}