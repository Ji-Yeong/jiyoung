{

#include <iostream>
#include <TROOT.h>
#include <TBrowser.h>
#include <TFile.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TH2D.h>
#include <TH1D.h>
#include <TPad.h>
#include <TArrow.h>
gROOT->Reset();
gROOT->SetStyle("Plain");
gStyle->SetPalette(1);
gStyle->SetOptStat(0);
gROOT->ForceStyle();
//auto can2 = new TCanvas ("can2","can2",800,600);
//TView *view = TView::CreateView(1);

//TFile* ethanol_6MeV_3cm = new TFile("water_field_gps_gamma_6mev.root");
//TFile* ethanol_9MeV_3cm = new TFile("water_field_gps_gamma_9mev.root");
//TFile* ethanol_12MeV_3cm = new TFile("water_field_gps_gamma_12mev.root");

TFile* ethanol_6MeV_3cm = new TFile("ethanol_6mev_electron.root");
TFile* ethanol_9MeV_3cm = new TFile("ethanol_9mev_electron.root");
TFile* ethanol_12MeV_3cm = new TFile("ethanol_12mev_electron.root");

//TFile* ethanol_6MeV_3cm = new TFile("water_field_gps_neutron_6mev.root");
//TFile* ethanol_9MeV_3cm = new TFile("water_field_gps_neutron_9mev.root");
//TFile* ethanol_12MeV_3cm = new TFile("water_field_gps_neutron_12mev.root");

//TFile* ethanol_6MeV_3cm = new TFile("water_field_gps_6mev.root");
//TFile* ethanol_9MeV_3cm = new TFile("water_field_gps_9mev.root");
//TFile* ethanol_12MeV_3cm = new TFile("water_field_gps_12mev.root");

TTree* e_6MeV_3cm  = (TTree*)ethanol_6MeV_3cm ->Get("ethanol");
TTree* e_9MeV_3cm  = (TTree*)ethanol_9MeV_3cm ->Get("ethanol");
TTree* e_12MeV_3cm = (TTree*)ethanol_12MeV_3cm->Get("ethanol");
//TTree* e_6MeV_6cm  = (TTree*)ethanol_6MeV_6cm ->Get("ethanol");
//TTree* e_9MeV_6cm  = (TTree*)ethanol_9MeV_6cm ->Get("ethanol");
//TTree* e_12MeV_6cm = (TTree*)ethanol_12MeV_6cm->Get("ethanol");

int bin_number = 100;
int particle_number = 1000000;

TH2D* scatter_6MeV_3cm = new TH2D("scatter_6MeV_3cm","",bin_number,0.0,100.0,bin_number,0.0,450);
TH2D* scatter_9MeV_3cm = new TH2D("scatter_9MeV_3cm","",bin_number,0.0,100.0,bin_number,0.0,450);
TH2D* scatter_12MeV_3cm = new TH2D("scatter_12MeV_3cm","",bin_number,0.0,100.0,bin_number,0.0,450);
//TH2D* scatter_6MeV_6cm = new TH2D("scatter_6MeV_6cm","",bin_number,0.0,100.0,bin_number,0.0,450);
//TH2D* scatter_9MeV_6cm = new TH2D("scatter_9MeV_6cm","",bin_number,0.0,100.0,bin_number,0.0,450);
//TH2D* scatter_12MeV_6cm = new TH2D("scatter_12MeV_6cm","",bin_number,0.0,100.0,bin_number,0.0,450);

e_6MeV_3cm ->Draw("Track_Deposit_E:-y >> scatter_6MeV_3cm","","goff");
ofstream out1("ethanol_6meV_electron_945.txt");
int i=1,j=1;
double accumulated_deposit_energy =0.0 ,x1 = 0.0 ,x2 = 0.0 ,y1=0.0,y2=0.0;
for(i=0;i<bin_number;i++)
{ 
  x1 = scatter_6MeV_3cm->GetXaxis()->GetBinCenter(i);
  accumulated_deposit_energy =0.0;
  for(j=0;j<bin_number;j++)
  {
    y1 = 0;
    y2 = 0;
    y1 = scatter_6MeV_3cm->GetYaxis()->GetBinCenter(j);
    y2 = scatter_6MeV_3cm->GetBinContent(i,j);
    accumulated_deposit_energy += y1*y2;
    if (j==99)
    {
      //cout <<  "x1(mm)->: " << x1 <<", accumlated_deposti_energy(keV)->: " 
      //<< accumulated_deposit_energy/particle_number <<", y1(keV)->: " 
      //<< y1 << ", y2(weight_number)->: " << y2 << endl;  
      out1 << x1 << " " << accumulated_deposit_energy/particle_number << endl;
    }
  }
} 
out1.close();
e_9MeV_3cm ->Draw("Track_Deposit_E:-y >> scatter_9MeV_3cm","","goff");
ofstream out2("ethanol_9meV_electron_945.txt");
i=1,j=1 ,accumulated_deposit_energy =0.0 ,x1 = 0.0 ,x2 = 0.0 ,y1=0.0,y2=0.0;
for(i=0;i<bin_number;i++)
{ 
  x1 = scatter_9MeV_3cm->GetXaxis()->GetBinCenter(i);
  accumulated_deposit_energy =0.0;
  for(j=0;j<bin_number;j++)
  {
    y1 = 0;
    y2 = 0;
    y1 = scatter_9MeV_3cm->GetYaxis()->GetBinCenter(j);
    y2 = scatter_9MeV_3cm->GetBinContent(i,j);
    accumulated_deposit_energy += y1*y2;
    if (j==99)
    {
      //cout <<  "x1(mm)->: " << x1 <<", accumlated_deposti_energy(keV)->: " 
      //<< accumulated_deposit_energy/particle_number <<", y1(keV)->: " 
      //<< y1 << ", y2(weight_number)->: " << y2 << endl;  
      out2 << x1 << " " << accumulated_deposit_energy/particle_number << endl;
    }
  }
} 
out2.close();
//Track_ID == 1 && Track_Kinetic_E == 0
e_12MeV_3cm->Draw("Track_Deposit_E:-y >> scatter_12MeV_3cm","","goff");
ofstream out3("ethanol_12meV_electron_945.txt");
i=1,j=1,accumulated_deposit_energy =0.0 ,x1 = 0.0 ,x2 = 0.0 ,y1=0.0,y2=0.0;
for(i=0;i<bin_number;i++)
{ 
  x1 = scatter_12MeV_3cm->GetXaxis()->GetBinCenter(i);
  accumulated_deposit_energy =0.0;
  for(j=0;j<bin_number;j++)
  {
    y1 = 0;
    y2 = 0;
    y1 = scatter_12MeV_3cm->GetYaxis()->GetBinCenter(j);
    y2 = scatter_12MeV_3cm->GetBinContent(i,j);
    accumulated_deposit_energy += y1*y2;
    if (j==99)
    {
      //cout <<  "x1(mm)->: " << x1 <<", accumlated_deposti_energy(keV)->: " 
      //<< accumulated_deposit_energy/particle_number <<", y1(keV)->: " 
      //<< y1 << ", y2(weight_number)->: " << y2 << endl;  
      out3 << x1 << " " << accumulated_deposit_energy/particle_number << endl;
    }
  }
} 
out3.close();
}