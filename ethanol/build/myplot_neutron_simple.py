import numpy as np
import matplotlib.pyplot as plt

file1 = np.genfromtxt("water_6MeV_field_gps_neutron_1.txt",dtype=np.float64,comments='#')
file2 = np.genfromtxt("water_9MeV_field_gps_neutron_1.txt",dtype=np.float64,comments='#')
file3 = np.genfromtxt("water_12MeV_field_gps_neutron_1.txt",dtype=np.float64,comments='#')

#file1 = np.genfromtxt("water_100keV_field_gps_gamma_1.txt",dtype=np.float64,comments='#')
#file4 = np.genfromtxt("water_500keV_field_gps_gamma_1.txt",dtype=np.float64,comments='#')
#file5 = np.genfromtxt("water_1MeV_field_gps_gamma_1.txt",dtype=np.float64,comments='#')

plt.plot(file1[:,0],file1[:,1]/file1[2:,1].max()*100, color='red',label='6MeV neutron',linewidth=3)
plt.plot(file2[:,0],file2[:,1]/file2[2:,1].max()*100, color='blue', label='9MeV neutron',linewidth=3)
plt.plot(file3[:,0],file3[:,1]/file3[2:,1].max()*100, color='green',label='12MeV neutron',linewidth=3)
#plt.plot(file4[:,0],file4[:,1]/file4[2:,1].max()*100, color='purple',label='0.1MeV gamma',linewidth=3)
#plt.plot(file5[:,0],file5[:,1]/file5[2:,1].max()*100, color='orange', label='0.5MeV gamma',linewidth=3)



plt.xlim(0,600),plt.ylim(0.0,120)
plt.xlabel('Depth [mm]',fontsize=24),plt.ylabel('Relative deposit energy to peak [%]',fontsize=24)
plt.xticks(fontsize=18),plt.yticks(fontsize=18)
#plt.yscale("log"),#plt.xscale("log")

plt.axhline(y=100, color='m', linestyle=':',label='100 percent line')
plt.axhline(y=50, color='m', linestyle='--',label='50 percent line')

#plt.axvline(x=24.9752,color='r', linestyle='--')
#plt.axvline(x=38.403, color='b', linestyle='--')
#plt.axvline(x=51.8463, color='g', linestyle='--')

plt.legend(loc='best',fontsize=24)
plt.show()