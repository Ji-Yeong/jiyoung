{

#include <iostream>
#include <TROOT.h>
#include <TBrowser.h>
#include <TFile.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TH2D.h>
#include <TH1D.h>
#include <TPad.h>
#include <TArrow.h>
gROOT->Reset();
gROOT->SetStyle("Plain");
gStyle->SetPalette(1);
gStyle->SetOptStat(0);
gROOT->ForceStyle();
auto can2 = new TCanvas ("can2","can2",800,600);
//TView *view = TView::CreateView(1);


TFile* ethanol_6MeV_3cm = new TFile("water_field_gps_6mev.root");
TFile* ethanol_9MeV_3cm = new TFile("water_field_gps_9mev.root");
TFile* ethanol_12MeV_3cm = new TFile("water_field_gps_12mev.root");
//TFile* ethanol_6MeV_3cm = new TFile("water_field_6mev.root");
//TFile* ethanol_9MeV_3cm = new TFile("water_field_9mev.root");
//TFile* ethanol_12MeV_3cm = new TFile("water_field_12mev.root");
//TFile* ethanol_6MeV_3cm = new TFile("water_point_6mev.root");
//TFile* ethanol_9MeV_3cm = new TFile("water_point_9mev.root");
//TFile* ethanol_12MeV_3cm = new TFile("water_point_12mev.root");
//TFile* ethanol_6MeV_3cm = new TFile("ethanol_6MeV_3cm.root");
//TFile* ethanol_9MeV_3cm = new TFile("ethanol_9MeV_3cm.root");
//TFile* ethanol_12MeV_3cm = new TFile("ethanol_12MeV_3cm.root");
//TFile* ethanol_6MeV_6cm = new TFile("ethanol_6MeV_6cm.root");
//TFile* ethanol_9MeV_6cm = new TFile("ethanol_9MeV_6cm.root");
//TFile* ethanol_12MeV_6cm = new TFile("ethanol_12MeV_6cm.root");

TTree* e_6MeV_3cm  = (TTree*)ethanol_6MeV_3cm ->Get("ethanol");
TTree* e_9MeV_3cm  = (TTree*)ethanol_9MeV_3cm ->Get("ethanol");
TTree* e_12MeV_3cm = (TTree*)ethanol_12MeV_3cm->Get("ethanol");
//TTree* e_6MeV_6cm  = (TTree*)ethanol_6MeV_6cm ->Get("ethanol");
//TTree* e_9MeV_6cm  = (TTree*)ethanol_9MeV_6cm ->Get("ethanol");
//TTree* e_12MeV_6cm = (TTree*)ethanol_12MeV_6cm->Get("ethanol");

int bin_number = 100;
int particle_number = 1000000;

TH2D* scatter_6MeV_3cm = new TH2D("scatter_6MeV_3cm","",bin_number,0.0,100.0,bin_number,0.0,50);
TH2D* scatter_9MeV_3cm = new TH2D("scatter_9MeV_3cm","",bin_number,0.0,100.0,bin_number,0.0,50);
TH2D* scatter_12MeV_3cm = new TH2D("scatter_12MeV_3cm","",bin_number,0.0,100.0,bin_number,0.0,50);
//TH2D* scatter_6MeV_6cm = new TH2D("scatter_6MeV_6cm","",bin_number,0.0,100.0,bin_number,0.0,450);
//TH2D* scatter_9MeV_6cm = new TH2D("scatter_9MeV_6cm","",bin_number,0.0,100.0,bin_number,0.0,450);
//TH2D* scatter_12MeV_6cm = new TH2D("scatter_12MeV_6cm","",bin_number,0.0,100.0,bin_number,0.0,450);

e_6MeV_3cm ->Draw("Track_Deposit_E:-y >> scatter_6MeV_3cm","Track_ID == 1 ");

ofstream out1("water_6MeV_field_gps_0_fine.txt");
int i=1,j=1;
double accumulated_deposit_energy =0.0 ,x1 = 0.0 ,x2 = 0.0 ,y1=0.0,y2=0.0;
for(i=0;i<bin_number;i++)
{ 
  x1 = scatter_6MeV_3cm->GetXaxis()->GetBinCenter(i);
  accumulated_deposit_energy =0.0;
  for(j=0;j<bin_number;j++)
  {
    y1 = 0;
    y2 = 0;
    y1 = scatter_6MeV_3cm->GetYaxis()->GetBinCenter(j);
    y2 = scatter_6MeV_3cm->GetBinContent(i,j);
    accumulated_deposit_energy += y1*y2;
    if (j==99)
    {
      //cout <<  "x1(mm)->: " << x1 <<", accumlated_deposti_energy(keV)->: " 
      //<< accumulated_deposit_energy/particle_number <<", y1(keV)->: " 
      //<< y1 << ", y2(weight_number)->: " << y2 << endl;  
      out1 << x1 << " " << accumulated_deposit_energy/particle_number << endl;
    }
  }
} 
out1.close();

e_9MeV_3cm ->Draw("Track_Deposit_E:-y >> scatter_9MeV_3cm","Track_ID == 1 ");

ofstream out2("water_9MeV_field_gps_0_fine.txt");
i=1,j=1 ,accumulated_deposit_energy =0.0 ,x1 = 0.0 ,x2 = 0.0 ,y1=0.0,y2=0.0;
for(i=0;i<bin_number;i++)
{ 
  x1 = scatter_9MeV_3cm->GetXaxis()->GetBinCenter(i);
  accumulated_deposit_energy =0.0;
  for(j=0;j<bin_number;j++)
  {
    y1 = 0;
    y2 = 0;
    y1 = scatter_9MeV_3cm->GetYaxis()->GetBinCenter(j);
    y2 = scatter_9MeV_3cm->GetBinContent(i,j);
    accumulated_deposit_energy += y1*y2;
    if (j==99)
    {
      //cout <<  "x1(mm)->: " << x1 <<", accumlated_deposti_energy(keV)->: " 
      //<< accumulated_deposit_energy/particle_number <<", y1(keV)->: " 
      //<< y1 << ", y2(weight_number)->: " << y2 << endl;  
      out2 << x1 << " " << accumulated_deposit_energy/particle_number << endl;
    }
  }
} 
out2.close();
//Track_ID == 1 && Track_Kinetic_E == 0
e_12MeV_3cm->Draw("Track_Deposit_E:-y >> scatter_12MeV_3cm","Track_ID == 1 ");

ofstream out3("water_12MeV_field_gps_0_fine.txt");
i=1,j=1,accumulated_deposit_energy =0.0 ,x1 = 0.0 ,x2 = 0.0 ,y1=0.0,y2=0.0;
for(i=0;i<bin_number;i++)
{ 
  x1 = scatter_12MeV_3cm->GetXaxis()->GetBinCenter(i);
  accumulated_deposit_energy =0.0;
  for(j=0;j<bin_number;j++)
  {
    y1 = 0;
    y2 = 0;
    y1 = scatter_12MeV_3cm->GetYaxis()->GetBinCenter(j);
    y2 = scatter_12MeV_3cm->GetBinContent(i,j);
    accumulated_deposit_energy += y1*y2;
    if (j==99)
    {
      //cout <<  "x1(mm)->: " << x1 <<", accumlated_deposti_energy(keV)->: " 
      //<< accumulated_deposit_energy/particle_number <<", y1(keV)->: " 
      //<< y1 << ", y2(weight_number)->: " << y2 << endl;  
      out3 << x1 << " " << accumulated_deposit_energy/particle_number << endl;
    }
  }
} 
out3.close();

//6MeV_6cm ->Draw("Track_Deposit_E:-y >> scatter_6MeV_6cm","Track_ID == 1 && Track_Kinetic_E == 0");
//9MeV_6cm ->Draw("Track_Deposit_E:-y >> scatter_9MeV_6cm","Track_ID == 1 && Track_Kinetic_E == 0");
//12MeV_6cm->Draw("Track_Deposit_E:-y >> scatter_12MeV_6cm","Track_ID == 1 && Track_Kinetic_E == 0");
TH1D* e_6MeV_3cm_proj_X = scatter_6MeV_3cm->ProjectionX("e_6MeV_3cm_proj_X",1,101);
TH1D* e_9MeV_3cm_proj_X = scatter_9MeV_3cm->ProjectionX("e_9MeV_3cm_proj_X",1,101);
TH1D* e_12MeV_3cm_proj_X = scatter_12MeV_3cm->ProjectionX("e_12MeV_3cm_proj_X",1,101);

cout << e_6MeV_3cm_proj_X->GetXaxis()->GetBinCenter(e_6MeV_3cm_proj_X->GetMaximumBin()) <<" :6MeV max " <<e_9MeV_3cm_proj_X->GetXaxis()->GetBinCenter( e_9MeV_3cm_proj_X->GetMaximumBin()) <<" :9MeV rms "
<< e_6MeV_3cm_proj_X->GetXaxis()->GetBinCenter(e_12MeV_3cm_proj_X->GetMaximumBin()) <<" :12MeV rms " << endl;

cout << e_6MeV_3cm_proj_X->GetRMS(1) << " :6MeV rms " << e_9MeV_3cm_proj_X->GetRMS(1) << " : 9MeV rms "
<< e_12MeV_3cm_proj_X->GetRMS(1) << ": 12 MeV " << endl;
cout << e_6MeV_3cm_proj_X->GetMean(1) << " :6MeV mean" << e_9MeV_3cm_proj_X->GetMean(1) << " :9MeV mean"
<< e_12MeV_3cm_proj_X->GetMean(1) << " :12MeV mean" << endl;


can2->Divide(1,1);
TLegend *leg = new TLegend(0.1,0.7,0.48,0.9);
//leg->SetHeader("Electron beam range with 6,9,12 MeV","C");
leg->SetBorderSize(0);
leg->SetFillStyle(0);
TLegendEntry* l1 = leg->AddEntry(e_6MeV_3cm_proj_X,"6MeV","l");
l1->SetTextColor(kRed);
l1->SetLineColor(kRed);
l1->SetLineWidth(4);
TLegendEntry* l2 = leg->AddEntry(e_9MeV_3cm_proj_X,"9MeV","l");
l2->SetTextColor(kBlue);
l2->SetLineColor(kBlue);
l2->SetLineWidth(4);
TLegendEntry* l3 = leg->AddEntry(e_12MeV_3cm_proj_X,"12MeV","l");
l3->SetTextColor(kGreen);
l3->SetLineColor(kGreen);
l3->SetLineWidth(4);
//can2->cd(0);
can2->cd(1);
   //gPad->SetGrid(1);
   e_6MeV_3cm_proj_X->SetXTitle("Depth[mm]");
   e_6MeV_3cm_proj_X->SetYTitle("Accumulated number");
   e_6MeV_3cm_proj_X->SetLineColor(kRed);
   e_6MeV_3cm_proj_X->SetLineWidth(2);
   e_6MeV_3cm_proj_X->GetYaxis()->SetTitleOffset(0);
   e_6MeV_3cm_proj_X->Draw();
   //e_9MeV_3cm_proj_X->SetXTitle("range[mm]");
   //e_9MeV_3cm_proj_X->SetYTitle("number");
   e_9MeV_3cm_proj_X->SetLineColor(kBlue);
   e_9MeV_3cm_proj_X->SetLineWidth(2);
   e_9MeV_3cm_proj_X->Draw("SAME");
   //e_12MeV_3cm_proj_X->SetXTitle("range[mm]");
   //e_12MeV_3cm_proj_X->SetYTitle("number");
   e_12MeV_3cm_proj_X->SetLineColor(kGreen);
   e_12MeV_3cm_proj_X->SetLineWidth(2);
   e_12MeV_3cm_proj_X->Draw("SAME");
   leg->Draw();
   gPad->Modified();

//can2->cd(2);
//   gPad->SetGrid(1);
//   scatter_6MeV_3cm->Draw("CONT1");
//   scatter_9MeV_3cm->Draw("CONT1 SAME");
//   scatter_12MeV_3cm->Draw("CONT1 SAME");
//   gPad->Modified();


/*
can2->Divide(3,2);
//can2->cd(0);
can2->cd(1);
   gPad->SetGrid(1);
   e_6MeV_3cm_proj_X->SetXTile("range[mm]")
   e_6MeV_3cm_proj_X->SetXYile("number")
   e_6MeV_3cm_proj_X->Draw();
   gPad->Modified();
can2->cd(2);
   gPad->SetGrid(1);
   e_9MeV_3cm_proj_X->SetXTile("range[mm]")
   e_9MeV_3cm_proj_X->SetXYile("number")
   e_9MeV_3cm_proj_X->Draw();
   gPad->Modified();
can2->cd(3);
   gPad->SetGrid(1);
   e_12MeV_3cm_proj_X->SetXTile("range[mm]")
   e_12MeV_3cm_proj_X->SetXYile("number")
   e_12MeV_3cm_proj_X->Draw();
   gPad->Modified();
can2->cd(4);
   gPad->SetGrid(1);
   scatter_6MeV_3cm->SetXTile("range[mm]")
   scatter_6MeV_3cm->SetYTile("Deposited Energy[keV]")
   scatter_6MeV_3cm->Draw("CONTZ");
   gPad->Modified();
can2->cd(5);
   gPad->SetGrid(1);
   scatter_9MeV_3cm->SetXTile("range[mm]")
   scatter_9MeV_3cm->SetYTile("Deposited Energy[keV]")
   scatter_9MeV_3cm->Draw("CONTZ");
   gPad->Modified();
can2->cd(6);
   gPad->SetGrid(1);
   scatter_12MeV_3cm->SetXTile("range[mm]")
   scatter_12MeV_3cm->SetYTile("Deposited Energy[keV]")
   scatter_12MeV_3cm->Draw("CONTZ");
   gPad->Modified();
*/
}