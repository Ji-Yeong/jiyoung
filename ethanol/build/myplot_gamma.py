import numpy as np
import matplotlib.pyplot as plt

file1 = np.genfromtxt("water_6MeV_field_gps_gamma_1.txt",dtype=np.float64,comments='#')
file2 = np.genfromtxt("water_9MeV_field_gps_gamma_1.txt",dtype=np.float64,comments='#')
file3 = np.genfromtxt("water_12MeV_field_gps_gamma_1.txt",dtype=np.float64,comments='#')

"""왼쪽 캔버스 부분 """
grid = plt.GridSpec(3,3, wspace=0.2, hspace=0.1)
plt.subplot(grid[0:,:2])

plt.plot(file1[:,0],file1[:,1]/file1[:,1].max()*100, color='red',label='6MeV electron beam',linewidth=3)
plt.plot(file2[:,0],file2[:,1]/file2[:,1].max()*100, color='blue', label='9MeV electron beam',linewidth=3)
plt.plot(file3[:,0],file3[:,1]/file3[:,1].max()*100, color='green',label='12MeV electron beam',linewidth=3)

depth_dmax_1 = file1[:,0][np.argmax(file1[:,1])]
depth_dmax_2 = file2[:,0][np.argmax(file2[:,1])]
depth_dmax_3 = file3[:,0][np.argmax(file3[:,1])]
index_d_upper_1 = np.argwhere(file1[:,1]>file1[:,1].max()/5*4).max()
index_d_upper_2 = np.argwhere(file2[:,1]>file2[:,1].max()/5*4).max()
index_d_upper_3 = np.argwhere(file3[:,1]>file3[:,1].max()/5*4).max()
index_d_lower_1 = np.argwhere(file1[:,1]>file1[:,1].max()/5).max()
index_d_lower_2 = np.argwhere(file2[:,1]>file2[:,1].max()/5).max()
index_d_lower_3 = np.argwhere(file3[:,1]>file3[:,1].max()/5).max()
index_steep_decent_grad_1 = np.argmin(np.gradient(file1[:,1]))
index_steep_decent_grad_2 = np.argmin(np.gradient(file2[:,1]))
index_steep_decent_grad_3 = np.argmin(np.gradient(file3[:,1]))

""" 50 퍼센트 라인과 주어진 데이터를 보간한 곡선의 교차점 구할려고 하니 오차가 심함 비추천"""
"""
precesion = 1000001
f = np.full(precesion,50.0, dtype=np.float64)
xval1 = np.linspace(file1[index_d_upper_1,0],file1[index_d_lower_1,0],precesion)
yinterp1 = np.interp(xval1,file1[index_d_upper_1:index_d_lower_1,0],file1[index_d_upper_1:index_d_lower_1,1]/file1[3:,1].max()*100.0)
idx1 = np.argwhere(np.diff(np.sign(f - yinterp1))).flatten()
xval2 = np.linspace(file2[index_d_upper_2,0],file2[index_d_lower_2,0],precesion)
yinterp2 = np.interp(xval2,file2[index_d_upper_2:index_d_lower_2,0],file2[index_d_upper_2:index_d_lower_2,1]/file2[3:,1].max()*100.0)
idx2 = np.argwhere(np.diff(np.sign(f - yinterp2))).flatten()
xval3 = np.linspace(file3[index_d_upper_3,0],file3[index_d_lower_3,0],precesion)
yinterp3 = np.interp(xval3,file3[index_d_upper_3:index_d_lower_3,0],file3[index_d_upper_3:index_d_lower_3,1]/file3[3:,1].max()*100.0)
idx3 = np.argwhere(np.diff(np.sign(f - yinterp3))).flatten()
plt.plot(xval1[idx1], f[idx1], 'ro')
plt.plot(xval2[idx2], f[idx2], 'bo')
plt.plot(xval3[idx3], f[idx3], 'go')
"""
""" """

""" Practical Range 구하기 위한 접선 구하기 위함 """

xxi = np.linspace(0,100,100)
m1, b1 = np.polyfit(file1[index_steep_decent_grad_1-1:index_steep_decent_grad_1+1,0],\
    file1[index_steep_decent_grad_1-1:index_steep_decent_grad_1+1,1]/file1[:,1].max()*100,1)
m2, b2 = np.polyfit(file2[index_steep_decent_grad_2-1:index_steep_decent_grad_2+1,0],\
    file2[index_steep_decent_grad_2-1:index_steep_decent_grad_2+1,1]/file2[:,1].max()*100,1)
m3, b3 = np.polyfit(file3[index_steep_decent_grad_3-1:index_steep_decent_grad_3+1,0],\
    file3[index_steep_decent_grad_3-1:index_steep_decent_grad_3+1,1]/file3[:,1].max()*100,1)

plt.plot(xxi,m1*xxi+b1,color='red',label='6MeV practical ragne extrapolate',linewidth=1,linestyle='-.')
plt.plot(xxi,m2*xxi+b2,color='blue',label='9MeV practical ragne extrapolate',linewidth=1,linestyle='-.')
plt.plot(xxi,m3*xxi+b3,color='green',label='12MeV practical ragne extrapolate',linewidth=1,linestyle='-.')

print(depth_dmax_1,"<-Depth at Dmplt |",depth_dmax_2,"<-Depth at Dmax2 |",depth_dmax_3,"<-Depth at Dmax3")
print(index_d_upper_1,"<-index at upper range1 |",index_d_upper_2,"index at upper range2 |",index_d_upper_3,"<-index at upper range3")
print(index_d_lower_1,"<-index at lower range1 |",index_d_lower_2,"index at lower range2 |",index_d_lower_3,"<-index at lower range3")
print(index_steep_decent_grad_1,"<-index at steep decent_gradient1 |",index_steep_decent_grad_2,"<-index at steep decent_gradient2 |"\
    ,index_steep_decent_grad_3,"<-index at steep decent_gradient3")

#print(np.percentile(file1[:,1],75))
#print(file3[:,0][np.argwhere(file3[:,1]/file3[:,1].max()*100==50)])

plt.xlim(0,600),plt.ylim(0.0,120)
plt.xlabel('Depth [mm]',fontsize=24),plt.ylabel('Relative deposit energy to peak [%]',fontsize=24)
plt.xticks(fontsize=18),plt.yticks(fontsize=18)
#plt.yscale("log"),#plt.xscale("log")

plt.axhline(y=100, color='m', linestyle=':',label='100 percent line')
plt.axhline(y=50, color='m', linestyle='--',label='50 percent line')
plt.annotate('24.6661',xy=(24.6661,50),xytext=(26, 54),arrowprops=dict(arrowstyle="->",facecolor='black'))
plt.annotate('37.9871',xy=(37.9871,50),xytext=(40, 54),arrowprops=dict(arrowstyle="->",facecolor='black'))
plt.annotate('51.3341',xy=(51.3341,50),xytext=(53, 54),arrowprops=dict(arrowstyle="->",facecolor='black'))


""" 파이썬 시각화 기능으로 교점을 확대해서 육안으로 확인 하는수 밖에 
R50 과 Radiation background 는 자동화 할려면 복잡하게 해야함 """
plt.axhline(y=0.25, color='r', linestyle='--',label='6 MeV beam Bremsstrahlung background',linewidth=1)
plt.axhline(y=0.5, color='b', linestyle='--',label='9 MeV beam Bremsstrahlung background',linewidth=1)
plt.axhline(y=0.8, color='g', linestyle='--',label='12 MeV beam Bremsstrahlung background',linewidth=1)
plt.plot(24.6661, 50, 'ro') 
plt.plot(37.9871, 50, 'bo')
plt.plot(51.3341, 50, 'go')
""" """
#plt.axvline(x=24.9752,color='r', linestyle='--')
#plt.axvline(x=38.403, color='b', linestyle='--')
#plt.axvline(x=51.8463, color='g', linestyle='--')

plt.legend(loc='best',fontsize=16)
    #,fontsize=16)
"""
plt.text(10, 1.34, r'Bremsstrahlung bkg line', color= 'y',fontsize=20)
plt.text(80, 52, r'50% line', color= 'm',fontsize=20)
plt.text(10, 102, r'100% line', color= 'y',fontsize=20)
"""
""" 오른쪽 캔버스 부분  """
plt.subplot(grid[0,2])
plt.xlim(28,35),plt.ylim(-1.0,3)
plt.axhline(y=0.25, color='r', linestyle='--',label='6 MeV beam Bremsstrahlung background',linewidth=1)
plt.axhline(y=0.5, color='b', linestyle='--',linewidth=1)
plt.axhline(y=0.8, color='g', linestyle='--',linewidth=1)
plt.plot(xxi,m1*xxi+b1,color='red',label='6MeV practical ragne extrapolate',linewidth=1,linestyle='-.')
plt.plot(file1[:,0],file1[:,1]/file1[:,1].max()*100, color='red',label='6MeV electron beam',linewidth=3)
plt.xlabel('Depth [mm]',fontsize=12),plt.ylabel('Relative deposit energy to peak [%]',fontsize=12)
plt.annotate('29.6226',xy=(29.6226,0.25),xytext=(29.4218, -0.114),arrowprops=dict(arrowstyle="->",facecolor='black'))
plt.legend(loc='best')

plt.subplot(grid[1,2])
plt.xlim(42,52),plt.ylim(-1.0,3)
plt.plot(xxi,m2*xxi+b2,color='blue',label='9MeV practical ragne extrapolate',linewidth=1,linestyle='-.')
plt.plot(file2[:,0],file2[:,1]/file2[:,1].max()*100, color='blue', label='9MeV electron beam',linewidth=3)
plt.axhline(y=0.25, color='r', linestyle='--',linewidth=1)
plt.axhline(y=0.5, color='b', linestyle='--',label='9 MeV beam Bremsstrahlung background',linewidth=1)
plt.axhline(y=0.8, color='g', linestyle='--',linewidth=1)
plt.xlabel('Depth [mm]',fontsize=12),plt.ylabel('Relative deposit energy to peak [%]',fontsize=12)
plt.annotate('44.9718',xy=(44.9718,0.5),xytext=(44, -0.114),arrowprops=dict(arrowstyle="->",facecolor='black'))
plt.legend(loc='best')

plt.subplot(grid[2,2])
plt.xlim(58,70),plt.ylim(-1.0,3)
plt.axhline(y=0.25, color='r', linestyle='--',linewidth=1)
plt.axhline(y=0.5, color='b', linestyle='--',linewidth=1)
plt.axhline(y=0.8, color='g', linestyle='--',label='12 MeV beam Bremsstrahlung background',linewidth=1)
plt.plot(xxi,m3*xxi+b3,color='green',label='12MeV practical ragne extrapolate',linewidth=1,linestyle='-.')
plt.plot(file3[:,0],file3[:,1]/file3[:,1].max()*100, color='green',label='12MeV electron beam',linewidth=3)
plt.xlabel('Depth [mm]',fontsize=12),plt.ylabel('Relative deposit energy to peak [%]',fontsize=12)
plt.annotate('60.1553',xy=(60.1553,0.8),xytext=(61, -0.114),arrowprops=dict(arrowstyle="->",facecolor='black'))
plt.legend(loc='best')

plt.show()