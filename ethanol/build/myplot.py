import numpy as np
import matplotlib.pyplot as plt

file1 = np.genfromtxt("ethanol_6meV_electron_945.txt",dtype=np.float64,comments='#')
file2 = np.genfromtxt("ethanol_9meV_electron_945.txt",dtype=np.float64,comments='#')
file3 = np.genfromtxt("ethanol_12meV_electron_945.txt",dtype=np.float64,comments='#')
"""
file1 = np.genfromtxt("ethanol_6meV_electron_9442.txt",dtype=np.float64,comments='#')
file2 = np.genfromtxt("ethanol_9meV_electron_9442.txt",dtype=np.float64,comments='#')
file3 = np.genfromtxt("ethanol_12meV_electron_9442.txt",dtype=np.float64,comments='#')
"""


"""왼쪽 캔버스 부분 """
grid = plt.GridSpec(3,3, wspace=0.2, hspace=0.1)
plt.subplot(grid[0:,:2])

plt.plot(file1[:,0],file1[:,1]/file1[:,1].max()*100, color='red',label='6MeV electron beam',linewidth=3)
plt.plot(file2[:,0],file2[:,1]/file2[:,1].max()*100, color='blue', label='9MeV electron beam',linewidth=3)
plt.plot(file3[:,0],file3[:,1]/file3[:,1].max()*100, color='green',label='12MeV electron beam',linewidth=3)

depth_dmax_1 = file1[:,0][np.argmax(file1[:,1])]
depth_dmax_2 = file2[:,0][np.argmax(file2[:,1])]
depth_dmax_3 = file3[:,0][np.argmax(file3[:,1])]
index_d_upper_1 = np.argwhere(file1[:,1]>file1[:,1].max()/5*4).max()
index_d_upper_2 = np.argwhere(file2[:,1]>file2[:,1].max()/5*4).max()
index_d_upper_3 = np.argwhere(file3[:,1]>file3[:,1].max()/5*4).max()
index_d_lower_1 = np.argwhere(file1[:,1]>file1[:,1].max()/5).max()
index_d_lower_2 = np.argwhere(file2[:,1]>file2[:,1].max()/5).max()
index_d_lower_3 = np.argwhere(file3[:,1]>file3[:,1].max()/5).max()
index_steep_decent_grad_1 = np.argmin(np.gradient(file1[:,1]))
index_steep_decent_grad_2 = np.argmin(np.gradient(file2[:,1]))
index_steep_decent_grad_3 = np.argmin(np.gradient(file3[:,1]))

""" 50 퍼센트 라인과 주어진 데이터를 보간한 곡선의 교차점 구할려고 하니 오차가 심함 비추천"""
"""
precesion = 1000001
f = np.full(precesion,50.0, dtype=np.float64)
xval1 = np.linspace(file1[index_d_upper_1,0],file1[index_d_lower_1,0],precesion)
yinterp1 = np.interp(xval1,file1[index_d_upper_1:index_d_lower_1,0],file1[index_d_upper_1:index_d_lower_1,1]/file1[3:,1].max()*100.0)
idx1 = np.argwhere(np.diff(np.sign(f - yinterp1))).flatten()
xval2 = np.linspace(file2[index_d_upper_2,0],file2[index_d_lower_2,0],precesion)
yinterp2 = np.interp(xval2,file2[index_d_upper_2:index_d_lower_2,0],file2[index_d_upper_2:index_d_lower_2,1]/file2[3:,1].max()*100.0)
idx2 = np.argwhere(np.diff(np.sign(f - yinterp2))).flatten()
xval3 = np.linspace(file3[index_d_upper_3,0],file3[index_d_lower_3,0],precesion)
yinterp3 = np.interp(xval3,file3[index_d_upper_3:index_d_lower_3,0],file3[index_d_upper_3:index_d_lower_3,1]/file3[3:,1].max()*100.0)
idx3 = np.argwhere(np.diff(np.sign(f - yinterp3))).flatten()
plt.plot(xval1[idx1], f[idx1], 'ro')
plt.plot(xval2[idx2], f[idx2], 'bo')
plt.plot(xval3[idx3], f[idx3], 'go')
"""
""" """

""" Practical Range 구하기 위한 접선 구하기 위함 """

xxi = np.linspace(0,100,100)
m1, b1 = np.polyfit(file1[index_steep_decent_grad_1-1:index_steep_decent_grad_1+1,0],\
    file1[index_steep_decent_grad_1-1:index_steep_decent_grad_1+1,1]/file1[:,1].max()*100,1)
m2, b2 = np.polyfit(file2[index_steep_decent_grad_2-1:index_steep_decent_grad_2+1,0],\
    file2[index_steep_decent_grad_2-1:index_steep_decent_grad_2+1,1]/file2[:,1].max()*100,1)
m3, b3 = np.polyfit(file3[index_steep_decent_grad_3-1:index_steep_decent_grad_3+1,0],\
    file3[index_steep_decent_grad_3-1:index_steep_decent_grad_3+1,1]/file3[:,1].max()*100,1)

plt.plot(xxi,m1*xxi+b1,color='red',label='6MeV practical ragne extrapolate',linewidth=1,linestyle='-.')
plt.plot(xxi,m2*xxi+b2,color='blue',label='9MeV practical ragne extrapolate',linewidth=1,linestyle='-.')
plt.plot(xxi,m3*xxi+b3,color='green',label='12MeV practical ragne extrapolate',linewidth=1,linestyle='-.')

print(depth_dmax_1,"<-Depth at Dmplt |",depth_dmax_2,"<-Depth at Dmax2 |",depth_dmax_3,"<-Depth at Dmax3")
print(index_d_upper_1,"<-index at upper range1 |",index_d_upper_2,"index at upper range2 |",index_d_upper_3,"<-index at upper range3")
print(index_d_lower_1,"<-index at lower range1 |",index_d_lower_2,"index at lower range2 |",index_d_lower_3,"<-index at lower range3")
print(index_steep_decent_grad_1,"<-index at steep decent_gradient1 |",index_steep_decent_grad_2,"<-index at steep decent_gradient2 |"\
    ,index_steep_decent_grad_3,"<-index at steep decent_gradient3")

#print(np.percentile(file1[:,1],75))
#print(file3[:,0][np.argwhere(file3[:,1]/file3[:,1].max()*100==50)])

plt.xlim(0,100),plt.ylim(0.0,120)
plt.xlabel('Depth [mm]',fontsize=24),plt.ylabel('Relative deposit energy to peak [%]',fontsize=24)
plt.xticks(fontsize=18),plt.yticks(fontsize=18)
#plt.yscale("log"),#plt.xscale("log")

plt.axhline(y=100, color='m', linestyle=':',label='100 percent line')
plt.axhline(y=50, color='m', linestyle='--',label='50 percent line')
"""
plt.annotate('24.6661 water',xy=(24.6661,50),xytext=(22, 46),arrowprops=dict(arrowstyle="->",facecolor='black'))
plt.annotate('37.9871 water',xy=(37.9871,50),xytext=(38, 46),arrowprops=dict(arrowstyle="->",facecolor='black'))
plt.annotate('51.3341 water',xy=(51.3341,50),xytext=(49, 46),arrowprops=dict(arrowstyle="->",facecolor='black'))

plt.annotate('25.6242',xy=(25.6242,50),xytext=(28, 54),arrowprops=dict(arrowstyle="-",facecolor='y'))
plt.annotate('39.186',xy=(39.186,50),xytext=(42, 54),arrowprops=dict(arrowstyle="-",facecolor='y'))
plt.annotate('52.6466',xy=(52.6466,50),xytext=(55, 54),arrowprops=dict(arrowstyle="-",facecolor='y'))
"""

""" 파이썬 시각화 기능으로 교점을 확대해서 육안으로 확인 하는수 밖에 
R50 과 Radiation background 는 자동화 할려면 복잡하게 해야함 """
plt.axhline(y=0.15256, color='r', linestyle='--',label='6 MeV beam Bremsstrahlung background',linewidth=1)
plt.axhline(y=0.277248, color='b', linestyle='--',label='9 MeV beam Bremsstrahlung background',linewidth=1)
plt.axhline(y=0.437078, color='g', linestyle='--',label='12 MeV beam Bremsstrahlung background',linewidth=1)
#plt.plot(24.6661, 50, 'rx') 
#plt.plot(37.9871, 50, 'bx')
#plt.plot(51.3341, 50, 'gx')

#plt.plot(25.6242, 50, 'ro') 
#plt.plot(39.186, 50, 'bo')
#plt.plot(52.6466, 50, 'go')
""" """
#plt.axvline(x=24.9752,color='r', linestyle='--')
#plt.axvline(x=38.403, color='b', linestyle='--')
#plt.axvline(x=51.8463, color='g', linestyle='--')

plt.legend(loc='best',fontsize=16)
    #,fontsize=16)
"""
plt.text(53, 71.34, 'water 1 $g/cm^{3}$ : $R_{50}$ 24.661(6MeV) 37.9871(9MeV) 54.3341(12MeV) \n $R_{p}$: 29.6226(6MeV) 44.9718(9MeV) 60.1553(12MeV)', color= 'cyan',fontsize=11)
plt.text(53, 65.34, 'NE3(7:3) 0.961 $g/cm^{3}$ : $R_{50}$ 25.6242(6MeV) 39.186(9MeV) 52.6466(12MeV) \n $R_{p}$: 30.5979(6MeV) 46.4864(9MeV) 61.7702(12MeV)', color= 'cyan',fontsize=11)
plt.text(53, 59.34, 'Ref(9:1) 0.9442 $g/cm^{3}$ : $R_{50}$ 26.0742(6MeV) 39.8412(9MeV) 53.5001(12MeV) \n $R_{p}$: 31.1772(6MeV) 47.0708(9MeV) 62.7553(12MeV)', color= 'cyan',fontsize=11)
plt.text(53, 53.34, 'Ref(9:1) 0.9451 $g/cm^{3}$ : $R_{50}$ 26.0456(6MeV) 39.7987(9MeV) 53.4369(12MeV) \n $R_{p}$: 31.1593(6MeV) 47.3023(9MeV) 62.784(12MeV)', color= 'cyan',fontsize=11)
"""
""" 오른쪽 캔버스 부분  """
plt.subplot(grid[0,2])
plt.xlim(28,35),plt.ylim(-1.0,3)
plt.axhline(y=0.15256, color='r', linestyle='--',label='6 MeV beam Bremsstrahlung background',linewidth=1)
plt.axhline(y=0.277248, color='b', linestyle='--',linewidth=1)
plt.axhline(y=0.437078, color='g', linestyle='--',linewidth=1)
plt.plot(xxi,m1*xxi+b1,color='red',label='6MeV practical ragne extrapolate',linewidth=1,linestyle='-.')
plt.plot(file1[:,0],file1[:,1]/file1[:,1].max()*100, color='red',label='6MeV electron beam',linewidth=3)
plt.xlabel('Depth [mm]',fontsize=12),plt.ylabel('Relative deposit energy to peak [%]',fontsize=12)
#plt.annotate('29.6226 water',xy=(29.6226,0.25),xytext=(29.4218, -0.114),arrowprops=dict(arrowstyle="->",facecolor='black'))
#plt.annotate('30.5979',xy=(30.5979,0.25),xytext=(30.7218, 0.314),arrowprops=dict(arrowstyle="-",facecolor='y'))
plt.legend(loc='best')

plt.subplot(grid[1,2])
plt.xlim(42,52),plt.ylim(-1.0,3)
plt.axhline(y=0.15256, color='r', linestyle='--',linewidth=1)
plt.axhline(y=0.277248, color='b', linestyle='--',label='9 MeV beam Bremsstrahlung background',linewidth=1)
plt.axhline(y=0.437078, color='g', linestyle='--',linewidth=1)
plt.plot(xxi,m2*xxi+b2,color='blue',label='9MeV practical ragne extrapolate',linewidth=1,linestyle='-.')
plt.plot(file2[:,0],file2[:,1]/file2[:,1].max()*100, color='blue', label='9MeV electron beam',linewidth=3)
plt.xlabel('Depth [mm]',fontsize=12),plt.ylabel('Relative deposit energy to peak [%]',fontsize=12)
#plt.annotate('44.9718 water',xy=(44.9718,0.5),xytext=(44, -0.114),arrowprops=dict(arrowstyle="->",facecolor='black'))
#plt.annotate('46.4864',xy=(46.4864,0.27366),xytext=(47, 0.314),arrowprops=dict(arrowstyle="-",facecolor='y'))
plt.legend(loc='best')

plt.subplot(grid[2,2])
plt.xlim(58,70),plt.ylim(-1.0,3)
plt.axhline(y=0.15256, color='r', linestyle='--',linewidth=1)
plt.axhline(y=0.277248, color='b', linestyle='--',linewidth=1)
plt.axhline(y=0.437078, color='g', linestyle='--',label='12 MeV beam Bremsstrahlung background',linewidth=1)
plt.plot(xxi,m3*xxi+b3,color='green',label='12MeV practical ragne extrapolate',linewidth=1,linestyle='-.')
plt.plot(file3[:,0],file3[:,1]/file3[:,1].max()*100, color='green',label='12MeV electron beam',linewidth=3)
plt.xlabel('Depth [mm]',fontsize=12),plt.ylabel('Relative deposit energy to peak [%]',fontsize=12)
#plt.annotate('60.1553 water',xy=(60.1553,0.8),xytext=(61, -0.114),arrowprops=dict(arrowstyle="->",facecolor='black'))
#plt.annotate('61.7702',xy=(61.7702,0.58406),xytext=(63, 0.714),arrowprops=dict(arrowstyle="-",facecolor='y'))
plt.legend(loc='best')

plt.show()