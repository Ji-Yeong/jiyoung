{
	gROOT->SetStyle("Plain");
        gStyle->SetOptStat(0);

        char hname[255] = "12mev";

        TFile * aa = new TFile(Form("ethanol_%s_electron.root",hname));

        TH1D * h1 = new TH1D("h1","h1",100,0,100);

        //ethanol->Draw("z>>h1");
        ethanol->Draw("-y>>h1","Track_ID==1 && Track_Kinetic_E==0");


        h1->SetTitle("");
	h1->GetXaxis()->SetTitle("Depth [mm]");
	h1->GetXaxis()->SetTitleFont(132);
	h1->GetYaxis()->SetTitleFont(132);
	h1->GetXaxis()->SetLabelFont(132);
	h1->GetYaxis()->SetLabelFont(132);
	TFile * out = new TFile(Form("Result_ethanol_%s_electron.root",hname),"RECREATE");
	h1->Write();
	out->Close();
}
	                                                                          
