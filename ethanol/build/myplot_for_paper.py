import numpy as np
import scipy as sp
from scipy import interpolate
from scipy.interpolate import Rbf, InterpolatedUnivariateSpline
from scipy.interpolate import UnivariateSpline
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.font_manager

matplotlib.font_manager.findSystemFonts(fontpaths=None, fontext='ttf')
#mpl.use("pgf")
pgf_with_pdflatex = {
    "pgf.texsystem": "pdflatex",
    "pgf.preamble": [
         r"\usepackage[utf8x]{inputenc}",
         r"\usepackage[T1]{fontenc}",
         r"\usepackage{cmbright}",
         ]
}
mpl.rcParams.update(pgf_with_pdflatex)
#mpl.rcParams['font.family'] = 'Dejavu Sans'
mpl.rcParams['font.family'] = 'serif'

file1 = np.genfromtxt("ethanol_6meV_electron_945.txt",dtype=np.float64,comments='#')
file2 = np.genfromtxt("ethanol_9meV_electron_945.txt",dtype=np.float64,comments='#')
file3 = np.genfromtxt("ethanol_12meV_electron_945.txt",dtype=np.float64,comments='#')

"""
xnew1 = file1[:,0]  
ynew1 = file1[:,1]/file1[:,1].max()*100
ius1 = InterpolatedUnivariateSpline(xnew1, ynew1)
#ius1 = UnivariateSpline(xnew1, ynew1)
xnewa1 = np.linspace(file1[:,0].min(),file1[:,0].max(),300)
ynewa1 = ius1(xnewa1)
#tck1 = interpolate.splrep(xnew1, ynew1, s=0)
#spline1 = interpolate.splev(file1[:,0], tck1, der=0)
xnew2 = file2[:,0]
ynew2 = file2[:,1]/file2[:,1].max()*100
ius2 = InterpolatedUnivariateSpline(xnew2, ynew2)
#ius2 = UnivariateSpline(xnew2, ynew2)
xnewa2 = np.linspace(file2[:,0].min(),file2[:,0].max(),300)
ynewa2 = ius2(xnewa2)
#tck2 = interpolate.splrep(xnew2, ynew2, s=0)
#spline2 = interpolate.splev(file2[:,0], tck2, der=0)
xnew3 = file3[:,0]
ynew3 = file3[:,1]/file3[:,1].max()*100
ius3 = InterpolatedUnivariateSpline(xnew3, ynew3)
#ius3 = UnivariateSpline(xnew3, ynew3)
xnewa3 = np.linspace(file3[:,0].min(),file3[:,0].max(),300)
ynewa3 = ius3(xnewa3)
#tck3 = interpolate.splrep(xnew3, ynew3, s=0)
#spline3 = interpolate.splev(file3[:,0], tck3, der=0)
plt.plot(xnewa1,ynewa1, color='red',label='6MeV electron beam',linewidth=3,ls="-")
plt.plot(xnewa2,ynewa2, color='blue', label='9MeV electron beam',linewidth=3,ls="--")
plt.plot(xnewa3,ynewa3, color='green',label='12MeV electron beam',linewidth=3,ls="-.")
"""
from matplotlib.ticker import AutoMinorLocator, FormatStrFormatter, FixedLocator, FixedFormatter ,MultipleLocator
fig = plt.figure()
ax = fig.gca()
#ax = plt.subplot()
x_formatter = FixedFormatter([1.5])
x_locator = FixedLocator([1.5])
ax.xaxis.set_minor_formatter(x_formatter)
ax.xaxis.set_minor_locator(x_locator)
ax.xaxis.set_major_locator(MultipleLocator(10))
ax.yaxis.set_major_locator(MultipleLocator(10))
ax.xaxis.set_major_formatter(FormatStrFormatter("%.0f"))
ax.yaxis.set_major_formatter(FormatStrFormatter("%.0f"))
ax.tick_params(axis="x", direction="out", length=4, width=2, color="turquoise")
ax.tick_params(axis="y", direction="out", length=4, width=2, color="orange")

ax.set_xlim(1.5,100)
ax.set_ylim(0.0,120)
ax.set_xlabel('Depth [mm]',fontsize=24)
ax.set_ylabel('Relative deposit energy to peak [%]',fontsize=24)

ax.plot(file1[:,0],file1[:,1]/file1[:,1].max()*100, color='red',label='6MeV',linewidth=3,ls="-")
ax.plot(file2[:,0],file2[:,1]/file2[:,1].max()*100, color='blue', label='9MeV',linewidth=3,ls="--")
ax.plot(file3[:,0],file3[:,1]/file3[:,1].max()*100, color='green',label='12MeV',linewidth=3,ls="-.")
plt.xticks(fontsize=18)
plt.yticks(fontsize=18)
ax.legend(loc='best',fontsize=24)
plt.show()

"""
plt.xlim(0,100)
plt.ylim(0.0,120)
plt.xlabel('Depth [mm]',fontsize=24)
plt.ylabel('Relative deposit energy to peak [%]',fontsize=24)
plt.xticks(fontsize=18)
plt.yticks(fontsize=18)
plt.legend(loc='best',fontsize=24)
plt.show()
"""